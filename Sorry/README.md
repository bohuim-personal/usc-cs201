Sorry
=====

### Sorry Game
- Game can be run through the main method in `SorryClient`
- Hosting/starting a game will start a `SorryServer` on the loopback IP and specified port
- The host is always the first player to connect to the server

### Scoreboard page
- Run main method in `ScoreServer`
- Goto the localhost at port 8080 `localhost:8080` or `127.0.0.1:8080`
- `ScoreServer` runs until it is explicitly terminated


### Notes
- When running on Mac OSX 10.11 El Capitan, the AudioSystem outputs a deprecated warning,  
  but this is not my fault and is Apple's problem. The sounds play correctly.
