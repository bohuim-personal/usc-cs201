package extension;

import java.awt.Component;
import java.awt.Container;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Insets;
import java.awt.image.ImageObserver;

import javax.swing.ImageIcon;


/**
 * ScaleIcon is an ImageIcon that scales to fill the entire space of its container. <br>
 * Inspired by <a href="https://tips4java.wordpress.com/2012/03/31/stretch-icon/">this blog post</a>
 * 
 * @author Bohui Moon
 */
public class ScaleIcon extends ImageIcon
{
	private static final long serialVersionUID = 1L;
	
	
	/* IVARS */
	
	
	/* ======== CONSTRUCTORS ========= */
	/**
	 * Creates an ScaleIcon from an image object using scale to fill.
	 * 
	 * @param image - the image
	 */
	public ScaleIcon(Image image)
	{
		super(image);
	}
	
	/**
	 * Creates an ImageIcon from the specified file using scale to fill.
	 * 
	 * @param filename - a String specifying a filename or path
	 */
	public ScaleIcon(String filename)
	{
		super(filename);
	}
	
	
	/* ======== OVERRIDE ========= */
	/**
	 * Draw the icon scaled to fill the container, excluding insets.
	 */
	@Override
	public void paintIcon(Component c, Graphics g, int x, int y)
	{
		Image img = getImage();
		if (img == null)
			return;
		
		Insets insets = ((Container) c).getInsets();
		x = insets.left;
		y = insets.top;
		
		int w = c.getWidth() - insets.left - insets.right;
		int h = c.getHeight() - insets.top - insets.bottom;
		
		ImageObserver observer = getImageObserver();
		g.drawImage(img, x, y, w, h, observer == null ? c : observer);
	}
}
