package extension;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JButton;


/**
 * ImageButton is a JButton that draws an image to fit its size.
 * 
 * @author Bohui Moon
 */
public class ImageButton extends JButton
{
	private static final long serialVersionUID = 2919208019281028L;
	
	
	/* IVARS */
	private Image image;
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a ImageButton with the given image
	 * 
	 * @param image
	 */
	public ImageButton(Image image)
	{
		this.image = image;
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * Set the image as the given one, ane repaint.
	 * 
	 * @param newImage = new Image to draw
	 */
	public void setImage(Image newImage)
	{
		image = newImage;
		repaint();
	}
	
	/**
	 * @return the currently drawn Image
	 */
	public Image getImage()
	{
		return image;
	}
	
	
	
	/* ======== OVERRIDE ======== */
	/**
	 * Draw the currently set image
	 * 
	 * @param g - Graphics to draw on
	 */
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int w = this.getWidth();
		int h = this.getHeight();
		g.drawImage(image, 0, 0, w, h, null);
		
		if (this.getModel().isPressed())
		{
			g.setColor(new Color(0.f, 0.f, 0.f, 0.3f));
			g.fillRect(0, 0, w, h);
		}
	}
}
