package extension;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import client.Constants;

/**
 * GrayButton is a normal button with its icons, border, and font preset.
 * 
 * @author Bohui Moon
 */
public class GrayButton extends JButton
{
	private static final long serialVersionUID = 454948594584L;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a GrayButton with the given text as its label/title.
	 * 
	 * @param text - String text button should display
	 */
	public GrayButton(String text, int fontSize)
	{
		super(text);
		
		//looks
		setBorder(BorderFactory.createLineBorder(Color.GRAY));
		setIcon(new ImageIcon( Constants.BUTTON_GRAY_DEFAULT ));
		setPressedIcon(new ImageIcon( Constants.BUTTON_GRAY_PRESSED ));
		setDisabledIcon(new ImageIcon(Constants.BUTTON_GRAY_DEFAULT));
		
		//text & font
		setFont(new Font(Constants.FONT_THIN_NAME, Font.PLAIN, fontSize));
		setHorizontalTextPosition(SwingConstants.CENTER);
	}
}
