package network;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

/**
 * SocketWrapper is an accessibility class that contains a PrintWriter and BufferedReader.
 * When started on a new thread, constantly reads lines and relays messages to delegate.
 * 
 * @author Bohui Moon
 */
public class SocketWrapper implements Runnable
{
	/**
	 * SocketWrapper.Delegate should be implemented by any class
	 * that can parse lines coming into this Socket's input stream.
	 */
	public interface MessageListener
	{		
		/**
		 * Called when SocketWrapper receives a line through its input stream,
		 * and relays the message the to the current delegate.
		 * The given line is guaranteed to be not null and not empty.
		 * 
		 * @param line - String line received
		 */
		public void didReceiveMessage(SocketWrapper sw, Message message);
	}
	
	/**
	 * SocketWrapper.DisconnectListener should be implemented by any class
	 * that keeps track of currently valid connections.
	 */
	public interface DisconnectListener
	{
		/**
		 * Called when SocketWrapper is deemed disconnected,
		 * tested for null when reading from its input stream.
		 * 
		 * @param sw - SocketWrapper that disconnected
		 */
		public void didDisconnect(SocketWrapper sw);
	}
	
	
	
	/* IVARS */
	private Thread myThread;
	
	private Socket socket;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	
	private Set<MessageListener> messageListeners;
	private DisconnectListener disconnectListener;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create SocketWrapper with the given Socket, and no delegate
	 * 
	 * @param s - Socket to wrap
	 */
	public SocketWrapper(Socket s) throws IOException
	{
		socket = s;
		
		oos = new ObjectOutputStream(socket.getOutputStream());
		oos.flush();
		ois = new ObjectInputStream(socket.getInputStream());
		
		messageListeners = new HashSet<MessageListener>();
		
		myThread = new Thread(this, "SocketWrapper");
		myThread.start();
	}
	
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return internal Socket associated with this wrapper
	 */
	public Socket getSocket()
	{
		return socket;
	}
	
	/**
	 * Add a MessageListener to be notified when a message is received.
	 * 
	 * @param listener - MessageListener to relay message to
	 */
	public synchronized void addMessageListener(MessageListener listener)
	{
		messageListeners.add(listener);
	}
	
	/**
	 * Remove the specified MessageListener to be notified when a message is received.
	 * 
	 * @param listener
	 */
	public synchronized void removeMessageListener(MessageListener listener)
	{
		messageListeners.remove(listener);
	}
	
	/**
	 * Set the DisconnectListener for this instance of SocketWrapper.
	 * Listener is notified whenever connection is lost on the remote side.
	 * 
	 * @param listener - DisconnectListener to report disconnect to
	 */
	public void setDisconnectListener(DisconnectListener listener)
	{
		disconnectListener = listener;
	}
	
	
	/* ======== NETWORK I/O ======== */	
	/**
	 * Send the given message object over the network.
	 * 
	 * @param message - Message object to send
	 */
	public void sendMessage(Message message)
	{
		if (socket.isClosed())
			return;
		
		try
		{
			oos.reset();
			oos.writeObject(message);
			oos.flush();
		}
		catch (IOException e)
		{
			//System.out.println("Failed to send " + message);
		}
	}
	
	/**
	 * Create a message with the given head and body, and send it.
	 * 
	 * @param head - String head 
	 * @param body - Serializable body
	 */
	public void sendMessage(String head, Serializable body)
	{
		sendMessage(new Message(head, body));
	}
	
	/**
	 * Create a Message only with a head and send it.
	 * 
	 * @param head - String head to send
	 */
	public void sendMessage(String head)
	{
		sendMessage(head, null);
	}
	
	/**
	 * Append the given name to "SocketWrapper" and set it as the new name for thread.
	 * 
	 * @param name - String name to append for new thread name
	 */
	public void setName(String name)
	{
		myThread.setName("SocketWrapper: " + name);
	}
	
	
	/**
	 * 
	 */
	@Override
	public void run()
	{
		try
		{
			while (true)
			{
				Message message = null;
				
				try 
				{
					Object obj = ois.readObject();

					if (obj instanceof Message)
						message = (Message) obj;
					
					if (message != null && message.isValid())
					{
						synchronized (this)
						{
							for (MessageListener ml : messageListeners)
								ml.didReceiveMessage(this, message);
						}
					}
				}
				catch (ClassNotFoundException e)
				{
					System.out.println("Class not found while reading message");
					throw new IOException();
				}
			}
		}
		catch (IOException e)
		{
			if (disconnectListener != null)
				disconnectListener.didDisconnect(this);
		}
	}
}
