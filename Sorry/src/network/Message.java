package network;

import java.io.Serializable;

/**
 * Message is an object to be sent over the network, specifying a string command and list of object parameters.
 * 
 * @author Bohui Moon
 */
public class Message implements Serializable
{
	private static final long serialVersionUID = -5366496390573530236L;
	
	
	/* IVARS */
	private String head;
	private Object body;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a Message with the given head and body.
	 * 
	 * @param head
	 * @param body
	 */
	public Message(String head, Serializable body)
	{
		this.head = head;
		this.body = body;
	}
	
	/**
	 * @return true if Message is valid; false otherwise.
	 */
	public boolean isValid()
	{
		return (head != null) && (!head.isEmpty());
	}
	
	/**
	 * @return String head of the Message
	 */
	public String head()
	{
		return head;
	}
	
	/**
	 * @return Object body of the Message
	 */
	public Object body()
	{
		return body;
	}
	
	@Override
	public String toString()
	{
		return "[Message: " + head + ", " + body + "]";
	}
}
