package network;


/**
 * Protocol defines constants used to communicate between SorryClient and Server.
 * 
 * @author Bohui Moon
 */
public class Protocol
{
	public static final String SEPARATOR = ":";
	public static final String SPACER = " ";
	
	
	//connection
	public static final String CONNECTION_MADE = "connection_made";
	public static final String CONNECTION_LOST = "connection_lost";
	
	
	//Color
	public static final String COLOR_READY 	  = "color_ready";
	public static final String COLOR_SELECT   = "color_select";
	public static final String COLOR_DESELECT = "color_deselect";
	public static final String COLOR_CONFIRM  = "color_confirm";
	public static final String ENABLE_CONFIRM = "color_enable_confirm";
	public static final String COLOR_FINISHED = "color_finished";
	
	//Timer
	public static final String TIMER_START = "timer_start";
	public static final String TIMER_STOP = "timer_stop";
	public static final String TIMER_DONE = "timer_done";
	
	
	//Turn
	public static final String NEXT_TURN = "next_turn";
	public static final String PLAYER_TURN = "player_turn";
	
	//Action
	public static final String HOVER   	   = "hover";
	public static final String UNHOVER 	   = "unhover";
	public static final String CLICK_LEFT  = "click_left";
	public static final String CLICK_RIGHT = "click_right";
	
	
	//Board
	public static final String BOARD_READY = "board_ready";
	public static final String BOARD_PAWNS = "board_pawns";
	
	public static final String CARD_CLICK = "card_click";
	public static final String CARD_ALERT = "card_alert";
	
	public static final String HIGHLIGHT_LOC_SEMI = "highlight_loc_semi";
	public static final String HIGHLIGHT_LOC_FULL = "highlight_loc_full";
	public static final String HIGHLIGHT_PATH_SEMI = "highlight_path_full";
	public static final String HIGHLIGHT_PATH_FULL = "highlight_path_full";
	
	public static final String HIGHLIGHT_CLEAR 		= "highlight_clear";
	public static final String HIGHLIGHT_CLEAR_LOC 	= "highlight_clear_loc";
	public static final String HIGHLIGHT_CLEAR_PATH = "highlight_clear_path";
	
	//Chat
	public static final String CHAT 		= "chat";
	public static final String DISCONNECT 	= "disconnect";
	
	
	//Game over
	public static final String GAME_OVER = "game_over";
	public static final String NAME_REQUEST = "name_request";
	public static final String NAME_RESPONSE = "name_response";
}
