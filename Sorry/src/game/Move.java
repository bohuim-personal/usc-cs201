package game;

import java.util.LinkedList;
import java.util.List;

/**
 * Move represents one Move a Player can make.
 * 
 * @author Bohui Moon
 */
public class Move
{
	/* IVARS */
	private Card card;
	private Pawn pawn;
	private BoardLocation start;
	private BoardLocation selected;
	private BoardLocation destination;
	
	private List<Pawn> sorrydPawns;
	
	private int score;
	private int distanceUsed;
	
	private boolean swap;

	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create Move with parameters specifying details about the Move.
	 */
	public Move(Card card, Pawn pawn, BoardLocation selected, BoardLocation destination, List<Pawn> sorrydPawns, int distUsed, int distToHome, boolean swap)
	{
		this.card = card;
		this.pawn = pawn;
		this.start = pawn.getLocation();
		this.selected = selected;
		this.destination = destination;
		
		this.sorrydPawns = sorrydPawns;
		if (sorrydPawns == null)
			this.sorrydPawns = new LinkedList<Pawn>();
		
		distanceUsed = distUsed;
		
		this.swap = swap;
		
		//score
		score = 0;
		score += 100 - distToHome;
		if (start.isStart())
			score += 600;
		for (int i=0; i < this.sorrydPawns.size(); i++)
			score += 100;
		
		//System.out.println("Move to " + destination + ", dist to home: " + distToHome + ", score: " + score);
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return Card associated with this Move
	 */
	public Card getCard()
	{
		return card;
	}
	
	/**
	 * @return Pawn being moved
	 */
	public Pawn getPawn()
	{
		return pawn;
	}
	
	/**
	 * @return starting location of this Move
	 */
	public BoardLocation getStart()
	{
		return start;
	}
	
	/**
	 * @return selected ending location of this Move
	 */
	public BoardLocation getSelected()
	{
		return selected;
	}
	
	/**
	 * @return final ending location of this Move
	 */
	public BoardLocation getDestination()
	{
		return destination;
	}
	
	/**
	 * @return number of Pawns sorry'd by this Move
	 */
	public int getSorrydCount()
	{
		return sorrydPawns.size();
	}
	
	/**
	 * @return List of Pawns sorry'd by this Move
	 */
	public List<Pawn> getSorrydPawns()
	{
		return sorrydPawns;
	}
	
	/**
	 * @return distance between start and selected
	 */
	public int distanceUsed()
	{
		return distanceUsed;
	}
	
	/**
	 * @return whether move should swap any landing target, rather than sorry'ing
	 */
	public boolean shouldSwap()
	{
		return swap;
	}
	
	/**
	 * @return the score of this Move
	 */
	public int score()
	{
		return score;
	}
	
	/**
	 * Returns whether this Move is special, as defined by either Sorry or Card 11 with swap option.
	 * 
	 * @return true if Sorry or Card 11 with swap, false otherwise.
	 */
	public boolean isSpecial()
	{
		Card.Type type = card.getType();
		return (type == Card.Type.CARD_SORRY) || (type == Card.Type.CARD_11 && swap);
	}
	
	/**
	 * @return true if move is backwards, false otherwise.
	 */
	public boolean isBackwards()
	{
		Card.Type type = card.getType();
		return (type == Card.Type.CARD_4) || (type == Card.Type.CARD_10 && distanceUsed == 1);
	}
}
