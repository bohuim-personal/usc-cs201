package game;

/**
 * Represents the 2 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card2 extends Card
{
	private static final long serialVersionUID = -1313577163639759156L;
	

	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a 2 Card 
	 */
	public Card2()
	{
		super(Type.CARD_2);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "2";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn from Start or move a pawn two spaces forward. Drawing a two entitles the player to draw again at the end of their turn.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{2};
	}

	@Override
	public boolean valid()
	{
		return 2 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(2);
	}

	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return !pawn.getLocation().isHome() && (this.pawn == null || this.pawn == pawn);
	}
}
