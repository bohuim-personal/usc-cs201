package game;

/**
 * Represents the 10 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card10 extends Card
{
	private static final long serialVersionUID = -8316623431843307323L;

	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card10()
	{
		super(Type.CARD_10);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "10";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn ten spaces forward or one space backward.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{10, -1};
	}


	@Override
	public boolean valid()
	{
		return (10 - stepsUsed != 0) && (1 - stepsUsed != 0);
	}
	
	@Override
	public void invalidate()
	{
		used(10);
	}

	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return ( pawn.getLocation().isPath() || pawn.getLocation().isSafety() ) && 
			   ( this.pawn == null || this.pawn == pawn );
	}

}