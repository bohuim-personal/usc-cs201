package game;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Stack;

import game.Card.Type;


/**
 * Deck keeps a stack of Cards that can be drawn and reshuffled when runs out.
 * 
 * @author Bohui Moon
 */
public class Deck
{
	/* IVARS */
	private ArrayList<Card> cards;
	private Stack<Card> deck;
	
	
	/* ======= CONSTRUCTOR & SETUP ======= */
	/**
	 * Create a Deck by creating necesary Cards and populating into the stack.
	 */
	public Deck()
	{
		cards = new ArrayList<Card>();
		deck = new Stack<Card>();
		
		createCards();
		resetDeck();
	}
	
	/**
	 * Create four cards of each type and insert into list of all cards.
	 */
	private void createCards()
	{
		
		for (Type t : Type.values())
		{
			//if (t == Type.CARD_1 || t == Type.CARD_7)// || t == Type.CARD_5 || t == Type.CARD_7)
			for (int i=0; i < 4; i++)
				cards.add( Card.createCard(t) );
		}
	}
	
	/**
	 * Reset deck by adding Cards from the list to the deck and shuffling.
	 */
	private void resetDeck()
	{
		for (Card c : cards)
			deck.add(c);
		Collections.shuffle(deck);
	}
	
	
	/* ======== METHODS ======== */
	/**
	 * Return the topmost Card of the deck. Reset deck before drawing if empty.<br>
	 * NOTE: Given card is guaranteed to be reset
	 * 
	 * @return Card on the top
	 */
	public Card draw()
	{
		if ( deck.isEmpty() )
			resetDeck();
		
		deck.peek().reset();
		return deck.pop();
	}
}
