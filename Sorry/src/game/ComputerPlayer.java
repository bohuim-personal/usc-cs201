package game;

import game.Card.Type;

import java.awt.Color;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;

import client.BoardDisplay;

/**
 * ComputerPlayer represents a Player controlled by the program. <br>
 * When handed the turn by the Game, it will process all the possible moves, and pick one with the highest score.
 * 
 * @author Bohui Moon
 */
public class ComputerPlayer extends Player
{
	/* IVARS */
	private BoardDisplay display;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a ComputerPlayer with the given team color.
	 * 
	 * @param color - Color of this ComputerPlayer's team
	 */
	public ComputerPlayer(Color color, Board board, BoardDisplay display)
	{
		super(color, board);
		
		this.display = display;
	}

	
	/* ======== EXTEND: Player ======== */
	/**
	 * Called when Game hands this Player the turn. <br>
	 * Look at all possible moves, and pick the highest scoring move.
	 */
	@Override
	public void nextMove()
	{
		Card card = board.drawCard();
		display.alertDraw(getColor(), card);
		
		card.reset();
		this.hasDraw = false;
		if (card.getType() == Card.Type.CARD_2)
			this.hasDraw = true;
		
		//make moves until card is invalid (mostly for Card7)
		while (card.valid())
		{
		
			Map<Pawn, Set<BoardLocation>> map = board.getValidLocations(pawns, card);
			if (!hasValidMoves(map))
				return;
			
			PriorityQueue<Move> moves = new PriorityQueue<Move>(new Comparator<Move>(){
				public int compare(Move m1, Move m2)
				{
					return m2.score() - m1.score();
				}
			});
			
			
			//create all possible moves
			for (Pawn pawn : map.keySet())
			{
				for (BoardLocation loc : map.get(pawn))
				{
					Color color = pawn.getColor();
					
					BoardLocation finalDest =  board.afterSlide(pawn, loc);
					int used = board.dist(pawn.getLocation(), loc, color);
					int distToHome = board.dist(finalDest, board.getHomeTile(pawn.getColor()).getLocation(), color);
					
					boolean swap = false;
					if (card.getType() == Type.CARD_11 && used != 11)
						swap = true;
					
					List<Pawn> sorrydPawns = board.getSorrydPawns(pawn, loc, swap);
					
					moves.add(new Move(card, pawn, loc, finalDest, sorrydPawns, used, distToHome, swap));
				}
			}
			
			
			//select highest scoring move
			Move move = moves.peek();
			
			card.setPawn(move.getPawn());
			if (card.getType() == Type.CARD_7)
				card.used( move.distanceUsed() );
			else
				card.invalidate();
			
			board.execute(move);
			display.update(board.getPawns());
		}
		
		System.out.println();
	}
	
	
	/* ======== HELPER ========= */
	/**
	 * Returns whether the given Map of Pawns to their Set of valid BoardLocations
	 * has any valid moves for any pawn.
	 * 
	 * @param map
	 * @return
	 */
	private boolean hasValidMoves(Map<Pawn, Set<BoardLocation>> map)
	{
		for (Pawn p : map.keySet())
		{
			if (!map.get(p).isEmpty())
				return true;
		}
		return false;
	}
}
