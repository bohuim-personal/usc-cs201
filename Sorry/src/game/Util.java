package game;

import java.awt.Color;

/**
 * Collection of static constants and functions for Game
 * 
 * @author Bohui Moon
 */
public abstract class Util
{
	/**
	 * Returns an index according to given color
	 * 
	 * @param c - color to get index of
	 * @return index of given color, or -1 if invalid color
	 */
	static public int indexFromColor(Color c)
	{
		if ( c.equals(Color.RED) )
			return 0;
		
		if ( c.equals(Color.BLUE) )
			return 1;
		
		if ( c.equals(Color.YELLOW) )
			return 2;
		
		if ( c.equals(Color.GREEN) )
			return 3;
		
		return -1;
	}
	
	/**
	 * Returns a color according to given index
	 * 
	 * @param index - of color
	 * @return color of given index, or black if invalid color
	 */
	static public Color colorFromIndex(int index)
	{
		int r = index % Game.NUM_COLORS;
		switch (r)
		{
			case 0: return Color.RED;
			case 1: return Color.BLUE;
			case 2: return Color.YELLOW;
			case 3: return Color.GREEN;
			default: return Color.GRAY;
		}
	}
	
	/**
	 * Returns string name of given color
	 * 
	 * @param c - color to get name of
	 * @return string name of color, or black if invalid color
	 */
	static public String colorName(Color c)
	{
		if ( c.equals(Color.RED) )
			return "red";
		
		if ( c.equals(Color.BLUE) )
			return "blue";
		
		if ( c.equals(Color.YELLOW) )
			return "yellow";
		
		if ( c.equals(Color.GREEN) )
			return "green";
		
		return "gray";
	}
	
	/**
	 * Construct the same color of the given alpha.
	 * 
	 * @param color - Color to make transparent
	 * @param alpha - transparency value
	 * @return Color of given base and alpha value
	 */
	static public Color changeAlpha(Color color, float alpha)
	{
		return new Color(color.getRed()/255.f, color.getGreen()/255.f, color.getBlue()/255.f, alpha);
	}
}
