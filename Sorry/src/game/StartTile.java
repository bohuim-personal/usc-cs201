package game;

import java.util.LinkedList;
import java.util.Queue;

/**
 * StartTile is a Tile that only has a previous Tile and internally holds a queue of Pawns.
 * 
 * @author Bohui Moon
 */
public class StartTile extends Tile
{	
	private static final long serialVersionUID = -5906277242138008454L;
	
	
	/* IVARS */
	private Queue<Pawn> queue;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 * @param location
	 */
	public StartTile(BoardLocation location)
	{
		super(location);
		
		queue = new LinkedList<Pawn>();
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return number of Pawns on this start
	 */
	public int getCount()
	{
		return queue.size();
	}
	
	/**
	 * Adds the given pawn to the queue instead of replacing it like superclass Tile.
	 * 
	 * @param pawn - Pawn to add
	 */
	@Override
	public void setPawn(Pawn pawn)
	{
		if (pawn == null || queue.contains(pawn))
			return;
		
		queue.add(pawn);
		pawn.setTile(this);
	}
	
	/**
	 * Return the Pawn currently on this Tile without removing.
	 * 
	 * @return Pawn currently on this Tile.
	 */
	public Pawn getPawn()
	{
		return queue.peek();
	}
	
	/**
	 * Remove the Pawn currently on this Tile and return it.
	 * 
	 * @return Pawn that was just on this Tile before getting removed.
	 */
	public Pawn removePawn()
	{
		if (queue.isEmpty())
			return null;
		
		return queue.remove();
	}
}
