package game;

/**
 * Represents the 3 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card3 extends Card
{
	private static final long serialVersionUID = 8751238972596691898L;


	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card3()
	{
		super(Type.CARD_3);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "3";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn three spaces forward.";
	}
	
	@Override
	public int[] getSteps()
	{
		return new int[]{3};
	}


	@Override
	public boolean valid()
	{
		return 3 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(3);
	}


	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return ( pawn.getLocation().isPath() || pawn.getLocation().isSafety() ) && 
			   ( this.pawn == null || this.pawn == pawn );
	}
}