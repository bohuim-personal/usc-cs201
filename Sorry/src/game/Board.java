package game;

import game.Card.Type;
import game.Tile.TileType;

import java.awt.Color;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Board represents the Sorry board with Tiles containing Pawns.
 * 
 * @author Bohui Moon
 */
public class Board
{
	public static final int PATH_LENGTH 	= 15;
	public static final int SAFETY_LENGTH	= 5;
	
	public static final int SHORT_SLIDE_FRONT 	= 0;
	public static final int SHORT_SLIDE_END 	= 3;
	public static final int SHORT_SLIDE_LENGTH 	= 4;
	public static final int LONG_SLIDE_FRONT 	= 8;
	public static final int LONG_SLIDE_END 		= 12;
	public static final int LONG_SLIDE_LENGTH 	= 5;
	
	/* Interfaces */
	/**
	 * Implemented by any class wishing to receive updates when board state changes.
	 */
	public interface UpdateListener
	{
		public void updated();
	}
	
	
	/* IVARS */	
	private UpdateListener updateListener;
	
	private HomeTile[] homes;
	private StartTile[] starts;
	private Tile[][] path;
	private Tile[][] safety;
	
	private Deck deck;
	private LinkedList<Pawn> pawns;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a Board and setup internal tiles
	 */
	public Board()
	{
		path   = new Tile[Game.NUM_COLORS][PATH_LENGTH];
		safety = new Tile[Game.NUM_COLORS][SAFETY_LENGTH];
		homes  = new HomeTile[Game.NUM_COLORS];
		starts = new StartTile[Game.NUM_COLORS];
		
		deck = new Deck();
		pawns = new LinkedList<Pawn>();
		
		setup();
	}
	
	
	/* ======== SETUP ========= */
	/**
	 * Initialize all necessary Tiles and link them.
	 */
	private void setup()
	{		
		//first create fill board with tiles.
		for (int zone = 0; zone < Game.NUM_COLORS; zone++)
		{
			//path
			for (int i = 0; i < PATH_LENGTH; i++)
				path[zone][i] = new Tile( new BoardLocation(TileType.PATH, zone, i) );
			
			//safety
			for (int i = 0; i < SAFETY_LENGTH; i++)
				safety[zone][i] = new Tile( new BoardLocation(TileType.SAFETY, zone, i) );
			
			//start
			starts[zone] = new StartTile( new BoardLocation(TileType.START, zone, -1) );
			
			//home
			homes[zone] = new HomeTile( new BoardLocation(TileType.HOME, zone, -1) );
		}
		
		Tile prev = path[0][0];
		Tile curr = null;
		for (int i = 1; i < Game.NUM_COLORS * PATH_LENGTH; i++)
		{
			int zone = i / PATH_LENGTH;
			int index = i % PATH_LENGTH;
			
			curr = path[zone][index];
			
			curr.setPrev(prev);
			prev.setNext(curr);
			
			if (index == 1)
			{
				
				Tile sprev = safety[zone][0];
				Tile scurr = null;
				
				sprev.setPrev(curr);
				curr.setColorNext(sprev);
				
				for (int s = 1; s < SAFETY_LENGTH; s++)
				{
					scurr = safety[zone][s];
					
					scurr.setPrev(sprev);
					sprev.setNext(scurr);
					
					sprev = scurr;
				}
				
				HomeTile h = homes[zone];
				h.setPrev(scurr);
				scurr.setNext(h);
			}
			else if (index == 3)
				starts[zone].setNext(curr);
			
			prev = curr;
		}
		Tile first = path[0][0];
		first.setPrev(curr);
		curr.setNext(first);
	}
	
	/**
	 * Register the given Player as participant by adding his/her pawns to the board.
	 * 
	 * @param player - Player joining the game
	 */
	public void registerPlayer(Player player)
	{
		for (Pawn p : player.getPawns())
		{
			pawns.add(p);
			starts[Util.indexFromColor(player.getColor())].setPawn(p);
		}
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @param listener - UpdateListener to be notified when Board state changes
	 */
	public void addUpdateListener(UpdateListener listener)
	{
		updateListener = listener;
	}
	
	/**
	 * @return List of Pawns on the board
	 */
	public LinkedList<Pawn> getPawns()
	{
		return pawns;
	}
	
	/**
	 * @return reset Card drawn from Deck.
	 */
	public Card drawCard()
	{
		return deck.draw();
	}
	
	/**
	 * Returns the number of Pawns on the StartTile in the given zone.
	 * 
	 * @param zone - color index of the StartTile 
	 * @return number of Pawns on the specified StartTile
	 */
	public int getStartCount(int zone)
	{
		return starts[zone].getCount();
	}
	
	/**
	 * Returns the number of Pawns on the HomeTile in the given zone.
	 * 
	 * @param zone - color index of the HomeTile 
	 * @return number of Pawns on the specified HomeTile
	 */
	public int getHomeCount(int zone)
	{
		return homes[zone].getCount();
	}
	
	/**
	 * Return the Tile at the specified BoardLocation.
	 * 
	 * @param loc - BoardLocation of Tile
	 * @return Tile at given loc
	 */
	public Tile getTile(BoardLocation loc)
	{
		if (loc.isStart())
			return starts[loc.getZone()];
		
		if (loc.isPath())
			return path[ loc.getZone() ][ loc.getIndex() ];
			
		if (loc.isSafety())
			return safety[ loc.getZone() ][ loc.getIndex() ];
		
		if (loc.isHome())
			return homes[loc.getZone()];
		
		return null;
	}
	
	/**
	 * Return the StartTile of the given color
	 * 
	 * @param color - Color of the StartTile 
	 * @return StartTile of color
	 */
	public StartTile getStartTile(Color color)
	{
		return starts[Util.indexFromColor(color)];
	}
	
	/**
	 * Return the HomeTile of the given Color
	 * 
	 * @param color - Color of the HomeTile
	 * @return HomeTile of color
	 */
	public HomeTile getHomeTile(Color color)
	{
		return homes[Util.indexFromColor(color)];
	}
	
	/**
	 * Return the Pawn on the Tile at the given BoardLocation.
	 * 
	 * @param loc - BoardLocation of Tile Pawn is on
	 * @return Pawn at given loc
	 */
	public Pawn getPawn(BoardLocation loc)
	{
		return getTile(loc).getPawn();
	}
	
	
	
	/* ======== SLIDES ======== */
	/**
	 * Returns whether the given Pawn can slide on the given tile. <br>
	 * Requirements are that the Tile is slide not the same color as the Pawn,
	 * and there is not a friendly Pawn at the end blocking. 
	 * 
	 * @param pawn - Pawn to check for slide validity
	 * @param tile - Tile to check starting from
	 * @return true if slide is possible, false otherwise.
	 */
	private boolean canLandOn(Pawn pawn, Tile tile)
	{
		BoardLocation loc = tile.getLocation();
		if (!loc.isSlideStart()) //if not slide, can land on it
			return true;
		
		if ( loc.getColor().equals(pawn.getColor()) ) //pawns can't side on their same color slide
			return true;
		
		//if is a slide, then check the end
		int length = loc.isShortSlideStart() ? SHORT_SLIDE_LENGTH : LONG_SLIDE_LENGTH;
		for (int i = 0; i < length-1; i++)
			tile = tile.getNext(null);
		
		Pawn target = tile.getPawn();
		if (target != null && pawn.sameTeam(target)) //can't land on the same team at the end of the slide
			return false;
		
		return true;
	}
	
	/**
	 * Returns the BoardLocation after attempting a slide. <br>
	 * If the given Pawn cannot slide on the given BoardLocation, loc is just returned.
	 * Otherwise, the end of the slide is returned.
	 * 
	 * @param pawn - Pawn to attempt sliding
	 * @param loc  - BoardLocation to start on
	 * @return ending BoardLocation after slide attempt
	 */
	public BoardLocation afterSlide(Pawn pawn, BoardLocation loc)
	{
		Tile tile = getTile(loc);
		if (!loc.isSlideStart() || !canLandOn(pawn, tile))
			return loc;
		
		if ( loc.getColor().equals(pawn.getColor()) ) //pawns can't side on their same color slide
			return loc;
		
		int length = loc.isShortSlideStart() ? SHORT_SLIDE_LENGTH : LONG_SLIDE_LENGTH;
		for (int i = 0; i < length-1; i++)
			tile = tile.getNext(null);
		return tile.getLocation();
	}
	
	
	
	/* ======== SORRY'D ========= */
	/**
	 * Returns a List of Pawns that would be sorry'd if the given Pawn were to land on the given location. <br>
	 * Usually this List will be only 1 item, unless Pawn lands on a slid with multiple enemy Pawns.
	 * 
	 * @param pawn - Pawn to simulate move
	 * @param loc  - BoardLocation to simulate move to
	 * @param swap - flag indicating whether landing targets should be swapped, not sorry'd
	 * @return List of Pawns that would be sorry'd from the simulation
	 */
	public List<Pawn> getSorrydPawns(Pawn pawn, BoardLocation loc, boolean swap)
	{		
		Tile tile = getTile(loc);
		List<Pawn> list = new LinkedList<Pawn>();
		
		if (Util.indexFromColor(pawn.getColor()) == loc.getZone())
			return list;
		
		if (!loc.isSlideStart())
		{
			Pawn target = tile.getPawn();
			if (!swap && target != null && pawn != target && !target.sameTeam(pawn))
				list.add(target);
			return list;
		}
		
		//is a slide, but cant land on it
		if (!canLandOn(pawn, tile))
			return list;
		
		//is a slide, and can land on it
		int length = tile.getLocation().isShortSlideStart() ? SHORT_SLIDE_LENGTH : LONG_SLIDE_LENGTH;
		for (int i = 0; i < length-1; i++)
		{
			tile = tile.getNext(null);
			
			Pawn target = tile.getPawn();
			if (target != null && pawn != target && !target.sameTeam(pawn))
				list.add(target);
		}
		
		return list;
	}
	
	/**
	 * Sorry the given Pawn by removing it from its current Tile and adding it to its Start.
	 * 
	 * @param pawn - Pawn to sorry.
	 */
	public void sorry(Pawn pawn)
	{
		if (pawn == null || pawn.isAtStart() || pawn.isAtHome())
			return;
		
		pawn.removeSelf();
		
		StartTile start = getStartTile(pawn.getColor());
		start.setPawn(pawn);
	}
	
	/**
	 * Swap the given Pawns a & b.
	 * 
	 * @param a - first Pawn to swap
	 * @param b - second Pawn to swap
	 */
	public void swap(Pawn a, Pawn b)
	{
		Tile ta = a.getTile();
		Tile tb = b.getTile();
		
		a.removeSelf();
		b.removeSelf();
		
		ta.setPawn(b);
		tb.setPawn(a);
	}
	
	
	
	/* ======== MOVEMENT ========= */
	/**
	 * Return a Map of Pawns to their Set of valid BaordLocations, given an array of pawns and a card. <br>
	 * Uses card to determine valid Pawns and calculates BoardLocations some steps aways specified by the card. <br>
	 * <b>Note:</b> The returned Set will never be null nor contain a null value.
	 * 
	 * @return Map of Pawns to their Set of valid BoardLocations.
	 */
	public Map<Pawn, Set<BoardLocation>> getValidLocations(Pawn[] pawns, Card card)
	{
		Map<Pawn, Set<BoardLocation>> map = new HashMap<Pawn, Set<BoardLocation>>();
		for (Pawn pawn : pawns)
		{
			Set<BoardLocation> set = new HashSet<BoardLocation>();
			
			if (card.isValidPawn(pawn))
			{				
				for (int s : card.getSteps())
					set.add(step(pawn, s));
			
				//special moves
				Type type = card.getType();
				if (type == Type.CARD_11 || type == Type.CARD_SORRY)
				{
					for (Pawn p : getSwappablePawns(pawn))
						set.add(p.getLocation());
				}
				
				set.remove(null);
			}
			map.put(pawn, set);
		}
		
		return map;
	}
	
	/**
	 * Return the BoardLocation n steps away from the given Pawn's current BoardLocation. <br>
	 * Steps specified can be negative, in which case, the path is traversed backwards. <br>
	 * If the travel is not possible (ie. friendly Pawn block or slide), returns null.
	 * 
	 * @param pawn - Pawn that is traveling
	 * @param n - number of steps to traverse
	 * @return BoardLocation n steps away from pawn's current location
	 */
	public BoardLocation step(Pawn pawn, int n)
	{
		Tile tile = pawn.getTile();
		if (tile == null)
			return null;
		

		do
		{
			if (n > 0)
			{
				tile = tile.getNext(pawn.getColor());
				n--;
			}
			else if (n < 0)
			{
				tile = tile.getPrev();
				n++;
			}
			
			Pawn target = tile == null ? null : tile.getPawn();
			if (target != null && pawn.sameTeam(target))
				return null;
		}
		while(n != 0 && tile != null && !tile.getLocation().isHome());
		
		BoardLocation loc = tile.getLocation();
		if (loc.isSlideStart() && !canLandOn(pawn, tile))
			return null;
		
		return loc;
	}
	
	/**
	 * Execute the given move.
	 * 
	 * @param move
	 */
	public void execute(Move move)
	{
		Pawn pawn = move.getPawn();
		BoardLocation startLoc = move.getStart();
		BoardLocation destLoc = move.getDestination();
		
		Tile tile = getTile(startLoc);
		Tile dest = getTile(destLoc);
		
		//special move
		if (move.isSpecial())
		{
			Pawn target = getPawn(destLoc);
			if (move.shouldSwap())
				swap(pawn, target);
			else
				sorry(target);
			
			pawn.putSelf(dest);
			if (updateListener != null)
				updateListener.updated();
			
			return;
		}
		
		//otherwise animate
		//NOTE: THIS IS SHIT CODE, SHOULD NOT BE ANIMATING IN THE DATA MODEL BUT JUST WITH IT FOR NOW
		while (tile != dest)
		{
			tile = (move.isBackwards()) ? tile.getPrev() : tile.getNext(pawn.getColor());
			
			Pawn target = getPawn(tile.getLocation());
			if (target != null)
				sorry(target);
			
			pawn.putSelf(tile);
			
			if (updateListener != null)
				updateListener.updated();
			
			try { Thread.sleep(250); }
			catch (InterruptedException e)
			{
				System.out.println("[Error@Board.execute()] interrupted exception: " + e.getMessage());
			}
		}
	}
	
	/**
	 * Returns the score for the given color.
	 * 
	 * @param color - Color of team to get score of
	 * @return score of the color 
	 */
	public int getScore(Color color)
	{
		int index = Util.indexFromColor(color);
		
		int score = 0;
		score += getHomeCount(index) * 5;
		
		for (Pawn p : pawns)
		{
			if (!p.getColor().equals(color))
			{
				if (!p.isAtHome()) score += 3;
				if (p.isAtStart()) score += 1;
			}
		}
		
		return score;
	}
	
	
	/* ======== HELPERS ======== */
	/**
	 * Returns a list of Pawns that can be swapped with the given Pawn
	 * (ie. not on given Pawn's team and is on a path)
	 * 
	 * @param pawn - Pawn to check for swap
	 * @return List of Pawns able to swap with pawn
	 */
	private List<Pawn> getSwappablePawns(Pawn pawn)
	{
		List<Pawn> list = new LinkedList<Pawn>();
		
		for (Pawn p : pawns)
		{
			if (p.getLocation().isPath() && !pawn.sameTeam(p))
				list.add(p);
		}
		return list;
	}
	
	/**
	 * Returns the distance between two BoardLocations.
	 * 
	 * @param a - start location
	 * @param b - end location
	 * @return
	 */
	public int dist(BoardLocation a, BoardLocation b, Color color)
	{
		Tile start = getTile(a);
		Tile end = getTile(b);
		Tile tile;
		
		if (start == end || start == null || end == null)
			return 0;
		
		int forward = 0;
		tile = start;
		do
		{
			if (tile != null)
			{
				tile = tile.getNext(color);
				forward++;
			}
		}
		while(tile != null  && tile != end && tile != start && !tile.getLocation().isHome());
		
		if (a.isStart() || b.isHome())
			return forward;
		
		int backward = 0;
		tile = start;
		do
		{
			tile = tile.getPrev();
			backward++;
		}
		while (tile != end && tile != start);
		
		return Math.min(forward, backward);
	}
	
	/**
	 * Returns the path between two BoardLocations as List of BoardLocations,
	 * taking any travels into safety if the given color permits so.
	 * 
	 * @param a - start location
	 * @param b - end location
	 * @return List of BoardLocations betwen a and b 
	 */
	public LinkedList<BoardLocation> path(BoardLocation a, BoardLocation b, Color color)
	{
		//unreachable
		if ((b.isSafety() || b.isHome()) && !b.getColor().equals(color))
			return null;
		
		Tile start = getTile(a);
		Tile end = getTile(b);
		Tile tile;
		
		LinkedList<BoardLocation> forward = new LinkedList<BoardLocation>();
		tile = start;
		while (tile != null && tile != end && !tile.getLocation().isHome())
		{
			tile = tile.getNext(color);
			if (tile != null)
				forward.add(tile.getLocation());
		}
		
		if (b.isSafety() || b.isHome())
			return forward;
		
		LinkedList<BoardLocation> backward = new LinkedList<BoardLocation>();
		tile = start;
		while (tile != null && tile != end)
		{
			tile = tile.getPrev();
			if (tile != null)
				backward.add(tile.getLocation());
		}
		
		if (backward.isEmpty())
			return forward;
		
		if (a.isSafety() && tile == end)
			return backward;
		
		return forward.size() < backward.size() ? forward : backward;
	}
}
