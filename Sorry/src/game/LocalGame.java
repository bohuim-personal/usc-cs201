package game;

import java.awt.Color;
import java.util.LinkedList;

import client.BoardDisplay;


/**
 * Game manages one run through of the Sorry board game.
 * 
 * @author Bohui Moon
 */
public class LocalGame extends Game
{	
	/* IVARS */
	private BoardDisplay display;
	
	
	/* ========= CONSTRUCTOR & SETUP ======== */
	/**
	 * Create a new Game with the given number of players, user's color, and BoardDisplay to use.
	 * 
	 * @param numPlayers - the number of players 
	 * @param mainColor  - color of the human player
	 */
	public LocalGame(int numPlayers, Color mainColor, BoardDisplay display)
	{
		super();
		
		setupPlayers(numPlayers, mainColor);
		display.update(board.getPawns());
	}
	
	/**
	 * Setup the Players given the number and human player color
	 * 
	 * @param numPlayers - number of players including bots
	 * @param mainColor  - color of the human user
	 */
	private void setupPlayers(int numPlayers, Color mainColor)
	{
		order = new LinkedList<Player>();
		order.add(new HumanPlayer(mainColor, board, display));
		numPlayers--;
		
		int index = Util.indexFromColor(mainColor);
		if (numPlayers == 1)
			index++;
		
		while(numPlayers > 0)
		{
			index++;
			if (index >= Game.NUM_COLORS)
				index -= Game.NUM_COLORS;
			
			order.add( new ComputerPlayer(Util.colorFromIndex(index), board, display) );
			numPlayers--;
		}
	}
}
