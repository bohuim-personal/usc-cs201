package game;

import java.awt.Color;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Game represents one round of Sorry game. <br>
 * Instance holds the Board played on, and a Queue of Players participating. <br>
 * While there is no winner (Player doesn't have all Pawns at home),
 * Game continues to tell the next Player up to make a move. <br>
 * 
 * All implementing subclasses should retain the same gameplay functionalities. <br>
 * Only difference is what type of Players are playing, and how they're setup.
 * 
 * @author Bohui Moon
 */
public abstract class Game
{
	public static final int NUM_COLORS = 4;
	
	/* IVARS */
	protected Board board;
	protected Queue<Player> order;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a new Game by created Deque of Players and a new Board.
	 * Specific setup should be done separately on implementing subclasses.
	 */
	public Game()
	{
		order = new LinkedList<Player>();
		board = new Board();
	}
	
	
	
	/* ======== GAMEPLAY ======== */
	/**
	 * Play the game by carrying out next turns until a winner is declared. <br>
	 * Then all players are notified of the winner. 
	 */
	public void play()
	{
		Player winner = null;
		while( (winner = getWinner()) == null )
		{
			nextTurn();
			afterTurn();
		}
		
		for (Player p : order)
			p.notifyWinner(winner);
		
		afterGame();
	}
	
	/**
	 * Returns the color of the winning team, known when all four pawns of a color are in home.
	 * 
	 * @return Color of winning team, or null if none yet.
	 */
	public Player getWinner()
	{
		for (Player p : order)
		{
			if (p.allPawnsHome())
				return p;
		}
		return null;
	}
	
	/**
	 * Hand the next Player in queue the turn.
	 * Afterwards if the Player nas no draws left,
	 * put him into the back of the queue.
	 */
	private void nextTurn()
	{
		Player currentPlayer = order.peek();
		
		currentPlayer.nextMove();
		
		if (!currentPlayer.hasDraw())
			order.add( order.remove() );
	}
	
	/**
	 * Execute after each player's turn. <br>
	 * By default, this method does not do anything.
	 * Any subclass that wishes to perform an action after each turn should override this. 
	 */
	protected void afterTurn()
	{
		
	}
	
	/**
	 * Execute after game is over and Players have been notified of the winner. <br>
	 * By default, this method does not do anything. 
	 * Any subclass that wishes to perform an action after the game should override this.
	 */
	protected void afterGame()
	{
		
	}
	
	/**
	 * Ask the board for current score of the given color's team.
	 * 
	 * @param color - Team to get score of
	 * @return score of the given color's team
	 */
	public int getScore(Color color)
	{
		return board.getScore(color);
	}
}
