package game;

import java.io.Serializable;

/**
 * Represents the 1 card.
 * 
 * @author Bohui Moon
 */
public class Card1 extends Card implements Serializable
{
	private static final long serialVersionUID = 4325673729132938572L;
	

	/* ======== CONSTRUCTOR ======= */
	/**
	 * Create a Card1 using type CARD_1
	 */
	public Card1()
	{
		super(Type.CARD_1);
	}

	
	/* ======== ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "1";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn from Start or move a pawn one space forward.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{1};
	}


	@Override
	public boolean valid()
	{
		return 1 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(1);
	}
	
	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return !pawn.getLocation().isHome() && (this.pawn == null || this.pawn == pawn);
	}
}
