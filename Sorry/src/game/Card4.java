package game;

/**
 * Represents the 4 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card4 extends Card
{
	private static final long serialVersionUID = 4355142699469906445L;
	

	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card4()
	{
		super(Type.CARD_4);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "4";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn four spaces backwards.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{-4};
	}


	@Override
	public boolean valid()
	{
		return 4 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(4);
	}

	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return ( pawn.getLocation().isPath() || pawn.getLocation().isSafety() ) && 
			   ( this.pawn == null || this.pawn == pawn );
	}
}