package game;

/**
 * Represents the 12 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card12 extends Card
{
	private static final long serialVersionUID = -7428208433405788374L;

	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card12()
	{
		super(Type.CARD_12);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "12";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn twelve spaces forward.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{12};
	}

	@Override
	public boolean valid()
	{
		return 12 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(12);
	}

	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return ( pawn.getLocation().isPath() || pawn.getLocation().isSafety() ) && 
			   ( this.pawn == null || this.pawn == pawn );
	}

	
}