package game;

import game.Card.Type;

import java.awt.Color;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import client.BoardDisplay;

/**
 * HumanPlayer represents a Player controlled by the local user running the game. <br>
 * When HumanPlayer is handed the turn by the board, it also becomes the delegate for client.BoardDisplay.
 * 
 * @author Bohui Moon
 */
public class HumanPlayer extends Player implements BoardDisplay.Delegate
{
	
	/* IVARS */
	private BoardDisplay display;
	
	private Card selectedCard;
	private Pawn selectedPawn;
	private BoardLocation selectedDest;
	
	private boolean canceled;
	private Set<BoardLocation> validStarts;
	private BoardLocation highlightedStart;
	
	private Set<BoardLocation> validLocations;
	private List<BoardLocation> highlightedPath;
	
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a HumanPlayer on the given team color.
	 * 
	 * @param color - Color of this HumanPlayer's team
	 */
	public HumanPlayer(Color color, Board board, BoardDisplay display)
	{
		super(color, board);
		this.display = display;
	}

	
	
	/* ======== EXTEND: Player ======== */
	/**
	 * Called when Board hands this HumanPlayer the turn. <br>
	 * Wait to receive actions from the user through BoardDisplay, and make the move accordingly.
	 */
	@Override
	public void nextMove()
	{
		display.setDelegate(this);
		
		//Card
		selectedCard = null;
		while(selectedCard == null)
			waiting();
		
		selectedCard.reset();
		this.hasDraw = false;
		if (selectedCard.getType() == Card.Type.CARD_2)
			this.hasDraw = true;
		
		display.alertDraw(getColor(), selectedCard);
		
		while(selectedCard.valid())
		{
			reset();
			
			//get valid locations map
			Map<Pawn, Set<BoardLocation>> map = board.getValidLocations(pawns, selectedCard);
			
			validStarts = new HashSet<BoardLocation>();
			for (Pawn p : map.keySet())
			{
				//System.out.println(p);
				if (!map.get(p).isEmpty())
					validStarts.add(p.getLocation());
				
				//for (BoardLocation loc : map.get(p))
					//System.out.println("\t" + loc);
			}
			System.out.println();
			
			if (validStarts.isEmpty())
				return;
			
			//cancel cycle
			canceled = true;
			while(canceled)
			{	
				canceled = false;
				display.unhighlightAll();
				
				//Pawn
				while(selectedPawn == null || map.get(selectedPawn).isEmpty()) 
					waiting();
				display.highlight(selectedPawn.getLocation(), Util.changeAlpha(color, 0.5f));
				
				//Dest
				validLocations = map.get(selectedPawn);
				while(selectedDest == null && !canceled) 
					waiting();
				highlightedPath = null;
				display.unhighlightAll();
			}
			
			//make the move
			BoardLocation finalDest =  board.afterSlide(selectedPawn, selectedDest);
			int used = board.dist(selectedPawn.getLocation(), selectedDest, color);
			int distToHome = board.dist(finalDest, board.getHomeTile(selectedPawn.getColor()).getLocation(), color);
			
			boolean swap = false;
			if (selectedCard.getType() == Type.CARD_11 && used != 11)
				swap = true;
			
			List<Pawn> sorrydPawns = board.getSorrydPawns(selectedPawn, selectedDest, swap);
			
			Move move = new Move(selectedCard, selectedPawn, selectedDest, finalDest, sorrydPawns, used, distToHome, swap);
			
			selectedCard.setPawn(selectedPawn);
			if (selectedCard.getType() == Type.CARD_7)
				selectedCard.used( move.distanceUsed() );
			else
				selectedCard.invalidate();
			
			board.execute(move);
			display.update(board.getPawns());
		}
		
		System.out.println();
		
		display.setDelegate(null);
	}
	
	private void waiting()
	{
		try{ Thread.sleep(1); }
		catch (InterruptedException e)
		{
			
		}
	}


	/* ======== IMPLEMENT: BoardDisplayDelegate ========= */
	/**
	 * Called when user clicks card button. <br> 
	 */
	@Override
	public void cardClicked()
	{
		selectedCard = board.drawCard();
	}
	
	/**
	 * Called when user hover enters a tile
	 * 
	 * @param loc - BoardLocation of hover
	 */
	@Override
	public void hover(BoardLocation loc)
	{
		if (selectedPawn == null && validStarts != null && validStarts.contains(loc) && highlightedStart == null)
		{
			display.highlight(loc, new Color(color.getRed()/255.f, color.getGreen()/255.f, color.getBlue()/255.f, 0.3f));
			highlightedStart = loc;
		}
		else if (selectedPawn != null && validLocations != null && validLocations.contains(loc) && highlightedPath == null)
		{
			List<BoardLocation> path = board.path(selectedPawn.getLocation(), loc, color);
			display.highlight(path, new Color(color.getRed()/255.f, color.getGreen()/255.f, color.getBlue()/255.f, 0.3f));
			highlightedPath = path;
		}
	}

	/**
	 * Called when user hover exits a tile.
	 * 
	 * @param loc - BoardLocation of hover exit
	 */
	@Override
	public void unhover(BoardLocation loc)
	{
		if (selectedPawn == null && highlightedStart != null)
		{
			display.unhighlight(loc);
			highlightedStart = null;
		}
		if (selectedPawn != null && highlightedPath != null)
		{
			display.unhighlight(highlightedPath);
			highlightedPath = null;
		}
	}

	/**
	 * Called when user left clicks a tile.
	 * 
	 * @param loc - BoardLocation of left-click
	 */
	@Override
	public void leftClick(BoardLocation loc)
	{
		if (selectedPawn == null)
		{
			Pawn p = board.getPawn(loc);
			if (p != null && isMyPawn(p))
				selectedPawn = p;
		}
		else if (selectedDest == null)
		{
			if (validLocations != null && validLocations.contains(loc))
			{
				selectedDest = loc;
				canceled = false;
			}
		}
	}
	
	/**
	 * Called when user right clicks a tile.
	 * 
	 * @param loc - BoardLocation of right-click
	 */
	@Override
	public void rightClick(BoardLocation loc)
	{
		if (selectedPawn != null)
		{
			selectedPawn = null;
			selectedDest = null;
			canceled = true;
		}
	}
	
	
	
	/* ======== HELPERS ======== */
	/**
	 * Reset turn by setting all selected objects to null
	 */
	private void reset()
	{
		selectedPawn = null;
		selectedDest = null;
		
		validStarts = null;
		highlightedStart = null;
		
		validLocations = null;
		highlightedPath = null;
	}
}
