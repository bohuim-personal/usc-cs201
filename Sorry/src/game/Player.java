package game;

import java.awt.Color;


/**
 * Player represents one player participating in the game. <br>
 * Each player has a color/team and controls 4 pawns. 
 * 
 * @author Bohui Moon
 */
public abstract class Player
{
	public static final int NUM_PAWNS = 4;
	
	
	/* IVARS */
	protected Color color;
	protected Pawn[] pawns;
	
	protected boolean hasDraw;
	
	protected Board board;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a Player and four panws of the given team color. <br>
	 * Also tell Player to interact with the specified board and display. 
	 * 
	 * @param color
	 */
	public Player(Color color, Board board)
	{
		this.color = color;
		
		pawns = new Pawn[NUM_PAWNS];
		for (int i=0; i < pawns.length; i++)
			pawns[i] = new Pawn(color, i);
		
		this.hasDraw = false;
		
		this.board = board;
		board.registerPlayer(this);
	}
	
	
	/* ========== ACCESSORS ========= */
	/**
	 * @return team color of this player
	 */
	public Color getColor()
	{
		return color;
	}
	
	/**
	 * @return array of this Player's Pawns
	 */
	public Pawn[] getPawns()
	{
		return pawns;
	}
	
	/**
	 * Returns whether the given pawn is this player's
	 * 
	 * @param p - pawn to check
	 * @return true if color is same, false otherwise.
	 */
	public boolean isMyPawn(Pawn p)
	{
		return color.equals( p.getColor() );
	}
	
	/**
	 * Returns whether this Player has draws left. <br>
	 * Case happens when Player drew Card 2, which allows going again.
	 * 
	 * @return true if has a draw, false otherwise.
	 */
	public boolean hasDraw()
	{
		return hasDraw;
	}
	
	/**
	 * Returns whether all 4 of this Player's Pawns have made it to home.
	 *  
	 * @return true if all 4 Pawns home, false otherwise.
	 */
	public boolean allPawnsHome()
	{
		for (Pawn p : pawns)
		{
			if (!p.isAtHome())
				return false;
		}
		return true;
	}
	
	
	/* ======== METHODS ======== */
	/**
	 * Make the next move for the game to continue. <br>
	 * Human players wait until user draws a card and execute the card. <br>
	 * Computer players should calculate the next move automatically and execute it. 
	 */
	public abstract void nextMove();
	
	
	/**
	 * Notify this player of the Game's winner. <br>
	 * By default, this method does nothing. Any subclasses that wish to perform
	 * an action depending on a winner should override this method.
	 * 
	 * @param winner - Player that won
	 */
	public void notifyWinner(Player winner)
	{
		
	}
	
	
	
	/* ======== OVERRIDE ======== */
	/**
	 * @return String of form "Player <color>"
	 */
	public String toString()
	{
		return "Player " + Util.colorName(color);
	}
}
