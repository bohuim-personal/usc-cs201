package game;

/**
 * Represents the 11 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card11 extends Card
{
	private static final long serialVersionUID = -2532130570187138722L;


	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card11()
	{
		super(Type.CARD_11);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "11";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn eleven spaces forward, or switch spaces with an opposing pawn.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{11};
	}
	
	@Override
	public boolean valid()
	{
		return stepsUsed == 0;
	}
	
	@Override
	public void invalidate()
	{
		used(1);
	}


	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return ( pawn.getLocation().isPath() || pawn.getLocation().isSafety() ) && 
			   ( this.pawn == null || this.pawn == pawn );
	}

}