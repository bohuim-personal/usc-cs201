package game;

/**
 * Represents the Sorry card in Sorry.
 * 
 * @author Bohui Moon
 */
public class CardSorry extends Card
{
	private static final long serialVersionUID = -1018590762484173330L;
	

	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public CardSorry()
	{
		super(Type.CARD_SORRY);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "Sorry!";
	}

	@Override
	public String getDescription()
	{
		return "Move any one pawn from Start to a square occupied by any opponent, sending that pawn back to its own Start.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{};
	}

	@Override
	public boolean valid()
	{
		return stepsUsed == 0;
	}
	
	@Override
	public void invalidate()
	{
		used(1);
	}

	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return pawn.getLocation().isStart() && ( this.pawn == null || this.pawn == pawn );
	}

}