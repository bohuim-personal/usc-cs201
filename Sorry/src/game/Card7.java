package game;

/**
 * Represents the 7 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card7 extends Card
{
	private static final long serialVersionUID = 3649916859968729045L;
	
	
	/* IVARS */
	private Pawn pawn2;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card7()
	{
		super(Type.CARD_7);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "7";
	}

	@Override
	public String getDescription()
	{
		return "Move one pawn seven spaces forward or split the seven spaces between two pawns.";
	}

	@Override
	public int[] getSteps()
	{
		int[] steps = new int[7 - stepsUsed];
		for (int i = 1; i <= steps.length; i++)
			steps[i-1] = i;
		return steps;
	}

	@Override
	public boolean valid()
	{
		return 7 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(7);
	}
	
	@Override
	public boolean isValidPawn(Pawn p)
	{
		if (p == null)
			return false;
		
		boolean space = !(p.isAtHome() || p.isAtStart());  
		
		if (pawn2 == null)
			return space;
		else
			return space && (p == pawn || p == pawn2);
	}
	
	@Override
	public void setPawn(Pawn p)
	{
		if (pawn == null && pawn2 == null)
		{
			System.out.println("set first pawn");
			pawn = p;
		}
		if (pawn != null && pawn2 == null)
		{
			if ( !p.equals(pawn) )
			{
				System.out.println("set second pawn");
				pawn2 = p;
			}
		}
	}
	
	@Override
	public void reset()
	{
		super.reset();
		pawn2 = null;
	}
}
