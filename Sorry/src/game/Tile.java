package game;

import java.awt.Color;
import java.io.Serializable;


/**
 * Tile represents a single space on the Sorry board. <br>
 * Tiles know their BoardLocation, pawns them (if there is one), and front/back tiles. 
 * 
 * @author Bohui Moon
 */
public class Tile implements Serializable
{
	private static final long serialVersionUID = -4918613958393145887L;


	public static enum TileType {
		START,
		PATH,
		SAFETY,
		HOME
	}
	
	
	/* IVARS */
	private BoardLocation location;
	
	protected Tile prev;
	protected Tile next;
	protected Tile colorNext;
	
	private Pawn pawn;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a new Tile with nothing set
	 */
	public Tile(BoardLocation location)
	{
		this.location = location;
		
		pawn = null;
	}
	
	
	
	/* ======== ONE TIME SETTERS ======== */
	/**
	 * Set the given Tile as the previous one only if prev is not already set.
	 * 
	 * @param t - Tile behind this one
	 */
	public void setPrev(Tile t)
	{
		if (prev == null)
			prev = t;
	}
	
	/**
	 * Set the given Tile as the next one only if next is not already set.
	 * 
	 * @param t - Tile in front of this one
	 */
	public void setNext(Tile t)
	{
		if (next == null)
			next = t;
	}
	
	/**
	 * Set the give Tile as the next color one (ex. safety zone) if not already set.
	 * 
	 * @param t - next Tile of the same color
	 */
	public void setColorNext(Tile t)
	{
		if (colorNext == null)
			colorNext = t;
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return BoardLocation of this Tile
	 */
	public BoardLocation getLocation()
	{
		return location;
	}
	
	/**
	 * Set the given Pawn as the one currently on this Tile.
	 * 
	 * @param pawn - Pawn to place on this Tile
	 */
	public void setPawn(Pawn pawn)
	{
		if (pawn != null)
			pawn.setTile(null);
		
		this.pawn = pawn;
		pawn.setTile(this);
	}
	
	/**
	 * Return the Pawn currently on this Tile without removing.
	 * 
	 * @return Pawn currently on this Tile.
	 */
	public Pawn getPawn()
	{
		return pawn;
	}
	
	/**
	 * Remove the Pawn currently on this Tile and return it.
	 * 
	 * @return Pawn that was just on this Tile before getting removed.
	 */
	public Pawn removePawn()
	{
		Pawn temp = getPawn();
		if (pawn != null)
			pawn.setTile(null);
		pawn = null;
		return temp;
	}
	
	/**
	 * @return Tile behind this one
	 */
	public Tile getPrev()
	{
		return prev;
	}
	
	/**
	 * Return the Tile in front of this one, going into safety zone if color matches this Tile's color.
	 * 
	 * @return
	 */
	public Tile getNext(Color color)
	{
		if (color != null && color.equals(location.getColor()) && colorNext != null)
			return colorNext;
		return next;
	}
	
	
	/* ======== Override ======== */
	/**
	 * @return String representation of this Tile.
	 */
	@Override
	public String toString()
	{
		return location.toString();
	}
}
