package game;

import game.Tile.TileType;

import java.awt.Color;
import java.io.Serializable;


/**
 * BoardLocation represents an abstract location on the Sorry board described by a type, zone/color, and index. 
 * 
 * @author Bohui Moon
 */
public class BoardLocation implements Serializable
{
	private static final long serialVersionUID = 100001L;
	
	
	/* IVARS */
	private TileType type;
	private int zone;
	private int index;
	private Color color;
	
	
	/* ======== CONSTRUCTOR ========= */
	/**
	 * Create a BoardLocation specified by the given type, zone, and index. <br>
	 * Color of the location is calculated from the zone. <br>
	 * Index is set to -1 for Start and Home locations.
	 * 
	 * @param type
	 * @param zone
	 * @param index
	 */
	public BoardLocation(TileType type, int zone, int index)
	{
		this.type = type;
		this.zone = zone;
		this.index = index;
		
		color = Util.colorFromIndex(zone);
		if (type == TileType.START || type == TileType.HOME)
			index = -1;
	}
	
	
	/* ========= GETTERS ========= */
	/**
	 * @return type of location
	 */
	public TileType getType()
	{
		return type;
	}
	
	/**
	 * @return zone index corresponding to zone color
	 */
	public int getZone()
	{
		return zone;
	}
	
	/**
	 * @return color corresponding to tile zone location
	 */
	public Color getColor()
	{
		return color;
	}
	
	/**
	 * @return index within zone and type 
	 */
	public int getIndex()
	{
		return index;
	}
	
	
	/* ========= CHECKERS ========= */
	/**
	 * @return true if Start location, false otherwise.
	 */
	public boolean isStart()
	{
		return type == TileType.START;
	}
	
	/**
	 * @return true if Path location, false otherwise.
	 */
	public boolean isPath()
	{
		return type == TileType.PATH;
	}
	
	/**
	 * @return true if Safety location, false otherwise.
	 */
	public boolean isSafety()
	{
		return type == TileType.SAFETY;
	}
	
	/**
	 * @return true if Home location, false otherwise.
	 */
	public boolean isHome()
	{
		return type == TileType.HOME;
	}
	
	/**
	 * @return true if is part of a long slide, false otherwise.
	 */
	public boolean isLongSlide()
	{
		return isPath() && (Board.LONG_SLIDE_FRONT <= index && index <= Board.LONG_SLIDE_END); 
	}
	
	/**
	 * @return true if is part of a short slide, false otherwise.
	 */
	public boolean isShortSlide()
	{
		return isPath() && (Board.SHORT_SLIDE_FRONT <= index && index <= Board.SHORT_SLIDE_END);
	}
	
	/**
	 * @return true if is part of a any slide, false otherwise.
	 */
	public boolean isSlide()
	{
		return isShortSlide() || isLongSlide();
	}
	
	/**
	 * @return true if start of a long slide, false otherwise.
	 */
	public boolean isLongSlideStart()
	{
		return isPath() && index == Board.LONG_SLIDE_FRONT;
	}
	
	/**
	 * @return true if start of a short slide, false otherwise.
	 */
	public boolean isShortSlideStart()
	{
		return isPath() && index == Board.SHORT_SLIDE_FRONT;
	}
	
	/**
	 * @return true if start of any slide, false otherwise.
	 */
	public boolean isSlideStart()
	{
		return isShortSlideStart() || isLongSlideStart();
	}
	
	
	/* ======== Override ======== */
	/**
	 * @return String representation of this BoardLocation as [type, zone, index]
	 */
	@Override
	public String toString()
	{
		return "[BoardLocation: type " + type + ", zone " + zone + ", index " + index + "]";  
	}
	
	/**
	 * Checks if two BoardLocations are equal by comparing their ivars.
	 * 
	 * @param obj - the reference object with which to compare.
	 * @return true if this object is the same as the obj argument; false otherwise.
	 */
	@Override
	public boolean equals(Object obj)
	{
		BoardLocation loc = (BoardLocation)obj;
		return (type == loc.getType() && zone == loc.getZone() && index == loc.getIndex());
	}
	
	/**
	 * @return hash code of this BoardLocation
	 */
	@Override
	public int hashCode()
	{
		return type.ordinal() + ( (zone+1) * 17 ) + ( (index+1) * 23 );
	}
}
