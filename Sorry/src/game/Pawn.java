package game;

import java.awt.Color;
import java.io.Serializable;

/**
 * Pawn represents a single piece controlled by a Player. <br>
 * Pawns know of their team color, and know their tile/loctions.
 * 
 * @author Bohui Moon
 */
public class Pawn implements Serializable
{
	private static final long serialVersionUID = 6657830514185190280L;
	
	
	/* IVARS */
	private int index;
	private Color color;
	
	private Tile tile;
	
	
	/* ========== CONSTRUCTOR ========= */
	/**
	 * Create a Pawn in the given team color, and index. <br>
	 * Index is only used for differentiating Pawns when hashing.
	 * 
	 * @param color
	 * @param index
	 */
	public Pawn(Color color, int index)
	{
		this.index = index;
		this.color = color;
		
		tile = null;
	}
	
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return color of this Pawn
	 */
	public Color getColor()
	{
		return color;
	}
	
	/**
	 * Set the Tile this Pawn is currently on
	 * 
	 * @param t - Tile currently on
	 */
	public void setTile(Tile t)
	{
		tile = t;
	}
	
	/**
	 * @return Tile Pawn is currently on
	 */
	public Tile getTile()
	{
		return tile;
	}
	
	/**
	 * @return BoardLocation of the Tile currently on
	 */
	public BoardLocation getLocation()
	{
		if (tile == null)
			return null;
		return tile.getLocation();
	}
	
	/**
	 * @param other - pawn to check if on this Pawn's team
	 * @return true if both are same color, false otherwise.
	 */
	public boolean sameTeam(Pawn other)
	{
		return other.getColor().equals(color);
	}
	
	/**
	 * @return true if in Home tile, false otherwise.
	 */
	public boolean isAtHome()
	{
		if (tile == null)
			return false;
		return tile.getLocation().isHome();
	}
	
	/**
	 * @return true if on Start tile, false otherwise.
	 */
	public boolean isAtStart()
	{
		if (tile == null)
			return false;
		return tile.getLocation().isStart();
	}
	
	/**
	 * Removes self from the Tile its on by calling {@code removePawn()}
	 */
	public void removeSelf()
	{
		if (tile != null)
			tile.removePawn();
	}
	
	/**
	 * Put itself on the given Tile after removing self from current one.
	 * 
	 * @param t - Tile to put this Pawn on
	 */
	public void putSelf(Tile t)
	{
		if (t == null)
			return;
		
		removeSelf();
		t.setPawn(this);
	}
	
	
	
	/* ======= OVERRIDE ======== */
	/**
	 * @return true if given obj is the same Pawn
	 */
	@Override
	public boolean equals(Object obj)
	{
		Pawn target = (Pawn)obj;
		return (index == target.index) && color.equals(target.color);
	}
	
	/**
	 * @return hash code of this Pawn as index + color hash code
	 */
	@Override
	public int hashCode()
	{
		return index + color.hashCode();
	}
	
	/**
	 * @return String representation of Pawn
	 */
	@Override
	public String toString()
	{
		return "[Pawn: " + Util.colorName(color) + ", " + getLocation().toString() + "]";
	}
}
