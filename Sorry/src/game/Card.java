package game;

import java.io.Serializable;


/**
 * Card represents the description players make their move based off in Sorry! <br>
 * Each type of card, implemented as subclasses, knows what pawns & moves are possible.
 * 
 * @author Bohui Moon
 */
public abstract class Card implements Serializable
{
	private static final long serialVersionUID = -3963665490598107278L;


	public static enum Type
	{
		CARD_1,
		CARD_2,
		CARD_3,
		CARD_4,
		CARD_5,
		CARD_7,
		CARD_8,
		CARD_10,
		CARD_11,
		CARD_12,
		CARD_SORRY
	}
	
	
	/* IVARS */
	private Type type;
	
	protected int stepsUsed;
	protected Pawn pawn;
	
	
	/* ======== INIT ======== */
	/**
	 * Return an instance of the correct Card subclass given a type.
	 * 
	 * @param type - type to use
	 * @return Card instance of that type
	 */
	public static Card createCard(Type type)
	{
		switch (type)
		{
			case CARD_1 	: return new Card1();
			case CARD_2 	: return new Card2();
			case CARD_3 	: return new Card3();
			case CARD_4 	: return new Card4();
			case CARD_5 	: return new Card5();
			case CARD_7 	: return new Card7();
			case CARD_8 	: return new Card8();
			case CARD_10 	: return new Card10();
			case CARD_11 	: return new Card11();
			case CARD_12 	: return new Card12();
			case CARD_SORRY : return new CardSorry();
			default			: return null;
		}
	}
	
	/**
	 * Initialize Card with given type
	 * 
	 * @param type - of this card
	 */
	public Card(Type t)
	{
		type = t;
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return type of this card
	 */
	public Type getType()
	{
		return type;
	}
	
	/**
	 * @return string representation of card type
	 */
	public abstract String getTypeString();
	
	/**
	 * @return description for this card
	 */
	public abstract String getDescription();
	
	/**
	 * @return array of steps possible for this card.
	 */
	public abstract int[] getSteps();
	
	/**
	 * @return whether this card is still valid to use.
	 */
	public abstract boolean valid();
	
	/**
	 * set this card as invalid no matter what
	 */
	public abstract void invalidate();
	
	/**
	 * Returns whether the given pawn is valid to use
	 * 
	 * @param pawn - 
	 * @return
	 */
	public abstract boolean isValidPawn(Pawn pawn);
	
	/**
	 * Set the given Pawn as the one used
	 * 
	 * @param pawn
	 */
	public void setPawn(Pawn pawn)
	{
		this.pawn = pawn;
	}
	
	/**
	 * Increment the number of steps used
	 * 
	 * @param steps - num steps used.
	 */
	public void used(int steps)
	{
		stepsUsed += steps;
	}
	
	/**
	 * Reset fields.
	 */
	public void reset()
	{
		stepsUsed = 0;
		pawn = null;
	}
	
	
	
	/* ======== OVERRIDE ======== */
	/**
	 * @return text of card according to type
	 */
	@Override
	public String toString()
	{
		return getTypeString() + " - " + getDescription();
	}
}
