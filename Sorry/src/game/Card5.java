package game;

/**
 * Represents the 5 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card5 extends Card
{
	private static final long serialVersionUID = -5838745163972401600L;
	

	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card5()
	{
		super(Type.CARD_5);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "5";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn five spaces forward.";
	}

	@Override
	public int[] getSteps()
	{
		return new int[]{5};
	}

	@Override
	public boolean valid()
	{
		return 5 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(5);
	}

	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return ( pawn.getLocation().isPath() || pawn.getLocation().isSafety() ) && 
			   ( this.pawn == null || this.pawn == pawn );
	}
}