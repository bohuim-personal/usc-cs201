package game;

/**
 * Represents the 8 card in Sorry.
 * 
 * @author Bohui Moon
 */
public class Card8 extends Card
{
	private static final long serialVersionUID = -5392311556308684184L;
	

	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public Card8()
	{
		super(Type.CARD_8);
	}

	
	/* ========= ACCESSORS ========= */
	@Override
	public String getTypeString()
	{
		return "8";
	}

	@Override
	public String getDescription()
	{
		return "Move a pawn eight spaces forward.";
	}


	@Override
	public int[] getSteps()
	{
		return new int[]{8};
	}

	@Override
	public boolean valid()
	{
		return 8 - stepsUsed > 0;
	}
	
	@Override
	public void invalidate()
	{
		used(8);
	}

	@Override
	public boolean isValidPawn(Pawn pawn)
	{
		if (pawn == null)
			return false;
		return ( pawn.getLocation().isPath() || pawn.getLocation().isSafety() ) && 
			   ( this.pawn == null || this.pawn == pawn );
	}

}