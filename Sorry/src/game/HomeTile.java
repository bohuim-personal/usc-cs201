package game;

import java.awt.Color;
import java.util.LinkedList;
import java.util.Queue;


/**
 * HomeTile is a Tile that only has a next Tile and internally holds a queue of Pawns.
 * 
 * @author Bohui Moon
 */
public class HomeTile extends Tile
{
	private static final long serialVersionUID = 7888392803602238632L;
	
	
	/* IVARS */
	private Queue<Pawn> queue;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 * @param location
	 */
	public HomeTile(BoardLocation location)
	{
		super(location);
		
		queue = new LinkedList<Pawn>();
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return number of Pawns on this start
	 */
	public int getCount()
	{
		return queue.size();
	}
	
	/**
	 * Adds the given pawn to the queue instead of replacing it like superclass Tile.
	 * 
	 * @param pawn - Pawn to add
	 */
	@Override
	public void setPawn(Pawn pawn)
	{
		if (queue.contains(pawn))
			return;
		
		queue.add(pawn);
		pawn.setTile(this);
	}
	
	/**
	 * There should be no reason to get a Pawn from Home. <br>
	 * Always returns null.
	 * 
	 * @return null
	 */
	@Override
	public Pawn getPawn()
	{
		return null;
	}
	
	/**
	 * Cannot remove a Pawn that has already reached Home. <br>
	 * Always returns null.
	 * 
	 * @return null
	 */
	@Override
	public Pawn removePawn()
	{
		return null;
	}
	
	/**
	 * HomeTile dont have a next, and just returns itself.
	 * 
	 * @param color
	 */
	@Override
	public Tile getNext(Color color)
	{
		return this;
	}
}
