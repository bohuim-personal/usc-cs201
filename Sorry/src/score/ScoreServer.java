package score;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.util.PriorityQueue;
import java.util.Scanner;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

/**
 * ScoreServer launches a web server to the loopback IP port 8080. <br>
 * Every time a request comes in, it injects the data from the specified data path to the specified html path,
 * and serves the complete html document.
 * 
 * @author Bohui Moon
 */
public class ScoreServer
{
	private static final String INDEX_PATH = "page/index.html";
	private static final String DATA_PATH = "data/scores.txt";
	
	
	/* ======== CONSTRUCTOR =========*/
	/**
	 * Create a new ScoreServer
	 */
	public ScoreServer() throws IOException
	{
		HttpServer server = HttpServer.create(new InetSocketAddress(8080),0);
		server.createContext("/", new IndexHandler());
        server.setExecutor(null);
        server.start();
	}
	
	/**
	 * 
	 */
	static class IndexHandler implements HttpHandler
	{
        @Override
        public void handle(HttpExchange t) throws IOException
        {        	
        	Scanner scanner;
        	String index;
        	
        	//read index template
        	scanner = new Scanner(new File(INDEX_PATH));
        	index = scanner.useDelimiter("//A").next();
        	scanner.close();
        	
        	
        	//replace data
        	scanner = new Scanner(new File(DATA_PATH));
        	
        	PriorityQueue<Score> queue = new PriorityQueue<Score>();
        	while(scanner.hasNextLine())
        	{
        		String line = scanner.nextLine();        		
        		String[] params = line.split(" ");
        		queue.add( new Score(params[0], Integer.parseInt(params[1])) );
        	}
        	scanner.close();
        	
        	String data = "";
        	while (!queue.isEmpty())
        	{
        		Score s = queue.poll();
        		data += "<li class='score-item'>\n";
        		data += "\t<span class='score-name'>" + s.name + "</span>\n";
        		data += "\t<span class='score-val'>" + s.score + "</span>\n";
        		data += "</li>\n";
        	}
        	if (!data.isEmpty())
        		index = index.replace("no scores!", data);
        	
        	
        	//write to client
        	t.sendResponseHeaders(200, index.length());
        	OutputStream os = t.getResponseBody();
            os.write(index.getBytes());
            os.close();
        }
    }
	
	
	private static class Score implements Comparable<Score>
	{
		public String name;
		public int score;
		
		public Score(String n, int s)
		{
			name = n;
			score = s;
		}
		
		@Override
		public int compareTo(Score other)
		{
			if (score > other.score)
				return -1;
			if (score < other.score)
				return 1;
			return 0;
		}
	}
	
	
	/* ======== MAIN ======== */
	public static void main(String[] args)
	{
		try
		{
			new ScoreServer();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
