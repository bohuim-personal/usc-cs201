package score;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import java.util.Vector;


/**
 * ScoreManager manages the scores in the specified file, which needs to adhere to the ScoreManager format. <br>
 * <pre>
 * 	username1 score1
 * 	username2 score2
 * </pre>
 * 
 * When initialized, ScoreManager reads all the scores current in the file into an ordered set. <br>
 * Adding a new Score will should keep the set ordered in descending top scores. <br>
 * When saving, the file should always be in order.
 * 
 * @author Bohui Moon
 */
public class ScoreManager
{
	
	/* IVARS */
	private File file;
	private Vector<Score> scores;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a new ScoreManager managing the file at the given path. <br>
	 * Immediately read the scores data.
	 */
	public ScoreManager(String filepath) throws IOException
	{
		file = new File(filepath);
		file.createNewFile();
		
		scores = new Vector<Score>();
		readScores();
	}
	
	/**
	 * Read the scores from the already set File.
	 * 
	 * @throws IOException
	 */
	private void readScores() throws IOException
	{
		Scanner scanner = new Scanner(file);
		while(scanner.hasNextLine())
		{
			String[] params = scanner.nextLine().split(" ");
			scores.add(new Score(params[0], Integer.parseInt(params[1])));
		}
		scanner.close();
	}
	
	/**
	 * Add the given (name, score) tuple to the list of scores.
	 * 
	 * @param name - String name of user
	 * @param score - int score achieved in the game
	 */
	public void addScore(String name, int score)
	{
		scores.add(new Score(name, score));
	}
	
	/**
	 * Return the current list of scores
	 * 
	 * @return
	 */
	public List<Score> getScores()
	{
		scores.sort(null);
		return scores;
	}
	
	/**
	 * Save the current Score list to the managed file path.
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException
	{
		scores.sort(null);
		
		PrintWriter pw = new PrintWriter(file);
		for (Score s : scores)
			pw.println(s.name + " " + s.score);
		pw.flush();
		pw.close();
	}
	
	/**
	 * 
	 */
	public static class Score implements Comparable<Score>
	{
		public String name;
		public int score;
		
		public Score(String n, int s)
		{
			name = n;
			score = s;
		}
		
		public int compareTo(Score other)
		{
			if (score > other.score)
				return -1;
			if (score < other.score)
				return 1;
			return 0;
		}
		
		public boolean equals(Object obj)
		{
			Score other = (Score) obj;
			return name.equals(other.name);
		}
	}
}
