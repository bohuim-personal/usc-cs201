package server;

import game.Card;
import game.Game;
import game.Player;
import game.Util;
import game.Board;

import java.util.Vector;

import network.Message;
import network.Protocol;
import network.SocketWrapper;

/**
 * NetworkGame is a Game that runs on a SorryServer and takes NetworkPlayers
 * 
 * @author Bohui Moon
 */
public class NetworkGame extends Game implements SocketWrapper.MessageListener, Board.UpdateListener
{	
	public static final int TURN_LIMIT = 15;
	
	
	/* IVARS */
	private Vector<SocketWrapper> users;
	
	private int ready;
	private int remaining;
	private int responded;
	private boolean finished = false;
	
	
	/* ========= CONSTRUCTOR ======== */
	/**
	 * Create a NetworkGame with the given user color index mapping.
	 * 
	 * @param users - array of SocketWrapper 
	 */
	public NetworkGame(SocketWrapper[] colorMapping)
	{
		users = new Vector<SocketWrapper>(4);
		
		for (int i=0; i < colorMapping.length; i++)
		{
			SocketWrapper user = colorMapping[i];
			if (user != null)
			{
				users.add(user);
				
				user.addMessageListener(this);
				order.add(new NetworkPlayer(Util.colorFromIndex(i), board, this, user));
			}
		}
		
		ready = 0;
		remaining = users.size();
		responded = 0;
		finished = false;
		
		board.addUpdateListener(this);
	}
	
	/**
	 * Waits until all players have sent back a ready
	 */
	public void ready() throws InterruptedException
	{
		while (ready != remaining)
			Thread.sleep(1);
	}
	
	/**
	 * Called after the game is over
	 */
	@Override
	protected void afterGame()
	{
		try
		{
			while (!finished)
				Thread.sleep(1);
		}
		catch (InterruptedException e)
		{
			
		}
	}
	
	/**
	 * Notify all connected clients that the next turn has started.
	 */
	public void alertTurn()
	{
		for (SocketWrapper user : users)
			user.sendMessage(Protocol.NEXT_TURN, TURN_LIMIT);
	}
	
	/**
	 * Notify all connected clients the Card that specified Player just drew.
	 * 
	 * @param player - Player that drew the card
	 * @param card	 - Card drawn
	 */
	public void alertDraw(Player player, Card card)
	{
		for (SocketWrapper user : users)
			user.sendMessage(Protocol.CARD_ALERT, new Object[]{player.getColor(), card.getType()});
	}
	
	/**
	 * Alert all users of the current board state.
	 */
	public void alertUpdate()
	{
		for (SocketWrapper user : users)
			user.sendMessage(Protocol.BOARD_PAWNS, board.getPawns());
	}
	
	/**
	 * Notify all connected clients that the specified Player has disconnected.
	 * 
	 * @param player - Player that disconnected
	 */
	public void alertDisconnect(Player player)
	{
		remaining--;
		for (SocketWrapper user : users)
			user.sendMessage(Protocol.DISCONNECT, player.getColor());
	}
	
	
	/* ======== UPDATE LISTENER ======== */
	/**
	 * Called when board state is updated.
	 */
	public void updated()
	{
		alertUpdate();
	}
	

	/* ======== MESSAGE LISTENER ======== */
	/**
	 * 
	 * 
	 * @param sw
	 * @param message
	 */
	@Override
	public void didReceiveMessage(SocketWrapper sw, Message message)
	{
		String head = message.head();
		//Object body = message.body();
		
		if (head.equals(Protocol.BOARD_READY))
		{
			sw.sendMessage(Protocol.BOARD_PAWNS, board.getPawns());
			if ( sw == ((NetworkPlayer) order.peek()).getSocketWrapper() )
				sw.sendMessage(Protocol.PLAYER_TURN);
			
			ready++;
			if (ready == remaining)
				alertTurn();
		}
		else if (head.equals(Protocol.CHAT))
		{
			for (SocketWrapper user : users)
				user.sendMessage(message);
		}
		else if (head.equals(Protocol.NAME_RESPONSE))
		{
			for (SocketWrapper user : users)
				user.sendMessage(message);
			
			responded++;
			if (responded == remaining)
			{
				finished = true;
				for (SocketWrapper user : users)
					user.sendMessage(Protocol.GAME_OVER);
			}
		}
	}
}
