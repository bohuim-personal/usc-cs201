package server;

import java.io.IOException;
import java.net.ServerSocket;


/**
 * ClientAcceptor is a Runnable class that handles incoming connection requests.
 * When a valid socket connection is made, it's wrapped into a Client object,
 * and sent back to the server instance.
 * 
 * @author Bohui Moon
 */
public class SocketAcceptor implements Runnable
{
	/* IVARS */
	private SorryServer server;
	private ServerSocket ss;
	
	private boolean paused;
	
	
	/* ======== CONSTUCTOR ======== */
	/**
	 * Create a ClientAccept that communicates with the specified server.
	 * 
	 * @param server - SorryServer to communicate with
	 */
	public SocketAcceptor(SorryServer server)
	{
		this.server = server;
		ss = server.getServerSocket();
		
		paused = false;
	}
	
	
	/* ======= ACCESSORS ======== */
	/**
	 * @return true if thread is paused; false otherwise.
	 */
	public boolean paused()
	{
		return paused;
	}
	
	/**
	 * Pause this thread by raising the paused thread.
	 */
	public void pause()
	{
		paused = true;
	}
	
	/**
	 * Resume this thread by setting paused to false.
	 */
	public void resume()
	{
		paused = false;
	}
	
	/**
	 * 
	 */
	@Override
	public void run()
	{
		try
		{
			while (true)
			{
				while(paused)
					Thread.sleep(1);
				
				try
				{
					server.addSocket(ss.accept());
				}
				catch (IOException e)
				{
					System.out.println("[Error] attempting to accept socket connection");
					e.printStackTrace();
				}
			}
		}
		catch (InterruptedException e)
		{
			//System.out.println("ClientAcceptor thread interrupted.");
		}
	}
}
