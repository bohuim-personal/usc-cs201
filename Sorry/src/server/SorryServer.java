package server;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

import network.SocketWrapper;

/**
 * SorryServer is the umbrella server class that mainly has two phases.
 * After the server is 
 * 
 * @author Bohui Moon
 */
public class SorryServer implements Runnable, SocketWrapper.DisconnectListener
{
	/* IVARS */
	private Thread myThread;
	private ServerSocket ss;
	
	private int spotsLeft;
	private SocketWrapper host;
	private List<SocketWrapper> users;
	
	private Thread acceptorThread;
	private SocketAcceptor acceptor;
	
	private ColorPhase colorPhase;

	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a SorryServer listening on the specified port that allows for the specified number of players.
	 * 
	 * @param port
	 * @param numPlayers
	 */
	public SorryServer(int port, int players) throws IOException
	{
		ss = new ServerSocket();
		ss.setReuseAddress(true);
		ss.bind(new InetSocketAddress(port));
		
		spotsLeft = players;
		
		host = null;
		users = new LinkedList<SocketWrapper>();
		
		//socket acceptor
		acceptor = new SocketAcceptor(this);
		acceptorThread = new Thread(acceptor);
		
		colorPhase = new ColorPhase(players);
		
		//start server
		myThread = new Thread(this, "SorryServer");
		myThread.start();
		System.out.println("Sorry server launched with " + ss.getInetAddress().getHostName() + ":" + port);
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * @return main ServerSocket associated with this server instance
	 */
	public ServerSocket getServerSocket()
	{
		return ss;
	}
	
	/**
	 * Add the specified Client as one currently connected.
	 * Once the number of spots left reach 0, 
	 * acceptor thread waits until another client is disconnected.
	 * 
	 * @param client - newly connected Client
	 */
	public void addSocket(Socket socket) throws IOException
	{
		if (socket == null || spotsLeft <= 0)
			return;
		
		SocketWrapper newUser = new SocketWrapper(socket);
		newUser.setDisconnectListener(this);
		
		if (host == null)
			host = newUser;
		users.add(newUser);
		colorPhase.addUser(newUser);
		
		spotsLeft--;
		if (spotsLeft == 0)
			acceptor.pause();
	}
	
	/**
	 * Called by DisconnectChecker when an associated socket has disconnected
	 * 
	 * @param sw
	 */
	public void didDisconnect(SocketWrapper sw)
	{
		if (sw == host)
			myThread.interrupt();
		
		//remove from colorPhase
		users.remove(sw);
		colorPhase.removeUser(sw);
		
		spotsLeft++;
		if (spotsLeft > 0)
			acceptor.resume();
	}
	
	
	/**
	 * Main method of server.
	 */
	@Override
	public void run()
	{
		try
		{
			acceptorThread.start();
		
			colorPhase.process();
			acceptorThread.interrupt();
			
			//some way to extract SW->color match from ColorPhase instance
			NetworkGame game = new NetworkGame(colorPhase.getColors());
			game.ready();
			System.out.println("game start");
			game.play();
			
			ss.close();
			System.out.println("Server closed");
		}
		catch (InterruptedException e)
		{
			System.out.println("[Error SorryServer.run()] thread interrupted: " + e.getMessage());
			acceptorThread.interrupt();
		}
		catch (IOException e)
		{
			System.out.println("[Error SorryServer.run()] failed to close server socket: " + e.getMessage());
		}
	}
}
