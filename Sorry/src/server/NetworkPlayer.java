package server;

import game.Board;
import game.BoardLocation;
import game.Card;
import game.Card.Type;
import game.Move;
import game.Pawn;
import game.Player;
import game.Util;

import java.awt.Color;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import network.Message;
import network.Protocol;
import network.SocketWrapper;

/**
 * NetworkPlayer is Player that has a SocketWrapper and takes input over the network.
 * 
 * @author Bohui Moon
 */
public class NetworkPlayer extends Player implements SocketWrapper.MessageListener, SocketWrapper.DisconnectListener
{
	/* IVARS */
	private NetworkGame game;
	private SocketWrapper user;	
	
	private Timer timer;
	
	private Thread currentThread;
	private boolean disconnected;
	
	private boolean myTurn;
	private Card selectedCard;
	private Pawn selectedPawn;
	private BoardLocation selectedDest;
	
	private boolean canceled;
	private Set<BoardLocation> validStarts;
	private BoardLocation highlightedStart;
	
	private Set<BoardLocation> validLocations;
	private LinkedList<BoardLocation> highlightedPath;
	
	
	
	/* ======== CONSTRUCTOR ======== */	
	/**
	 * Create a NetworkPlayer of team color c that communicates with the given SocketWrapper.
	 * 
	 * @param sw - SocketWrapper to communicate through
	 * @param c  - team Color of this player
	 */
	public NetworkPlayer(Color color, Board board, NetworkGame ng, SocketWrapper sw)
	{
		super(color, board);
		
		user = sw;
		user.addMessageListener(this);
		user.setDisconnectListener(this);
		user.setName(Util.colorName(color) + " server");
		
		timer = new Timer();
		
		game = ng;
		myTurn = false;
		currentThread = null;
		disconnected = false;
	}

	public SocketWrapper getSocketWrapper()
	{
		return user;
	}
	
	
	/* ======== GAMEPLAY ======== */
	/**
	 * Reset move variables.
	 */
	public void reset()
	{
		selectedPawn = null;
		selectedDest = null;
		
		validStarts = null;
		highlightedStart = null;
		
		validLocations = null;
		highlightedPath = null;
	}
	
	/**
	 * Called every time this Player should make a move. <br>
	 * While the user is not disconnected call {@code humanMove()},
	 * which listens to the input messages coming from client socket.
	 * <br><br>
	 * When the user disconnects, call {@code computerMove()} to finish turn if current turn.
	 * Every subsequent turn after disconnect, should just be played by a computer.
	 */
	@Override
	public void nextMove()
	{
		game.alertTurn();
		
		if (disconnected)
		{
			computerMove();
			return;
		}
		
		//not dc'ed
		TimerTask task = null;
		try
		{
			user.sendMessage(Protocol.PLAYER_TURN);
			
			currentThread = Thread.currentThread();
			task = new TimerTask(){
				public void run()
				{
					currentThread.interrupt();
				}
			};
			timer.schedule(task, NetworkGame.TURN_LIMIT * 1000);
			
			humanMove(task);
		}
		catch (InterruptedException e)
		{
			System.out.println("turn interrupted");
			if (task != null)
				task.cancel();
			
			if (disconnected && myTurn)
			{
				System.out.println("Disconnected during turn");
				computerMove();
			}
		}
	}
	
	/**
	 * Move is made based on the action input the controlling user sends. <br>
	 * At each step of the way, the client is sent back a reply in response to its actions. <br>
	 * If user disconnects during move is happening, the method should immediately end
	 * by throwing an {@code InterruptedException}.
	 */
	private void humanMove(TimerTask task) throws InterruptedException
	{
		//reset turn variables
		myTurn = true;
		hasDraw = false;
		
		selectedCard = null;	
		while (selectedCard == null)
			Thread.sleep(1);
		
		game.alertDraw(this, selectedCard);
		if (selectedCard.getType() == Card.Type.CARD_2)
			hasDraw = true;
		
		//make move
		while (selectedCard.valid())
		{
			reset();
			 
			//get map of pawns to valid dests
			Map<Pawn, Set<BoardLocation>> map = board.getValidLocations(pawns, selectedCard);
			
			validStarts = new HashSet<BoardLocation>();
			for (Pawn p : map.keySet())
			{
				if (!map.get(p).isEmpty())
					validStarts.add(p.getLocation());
			}
			
			//no moves to make
			if (validStarts.isEmpty())
			{
				myTurn = false;
				return;
			}
			
			//select pawn
			canceled = true;
			while(canceled)
			{
				canceled = false;
				user.sendMessage(Protocol.HIGHLIGHT_CLEAR);
				
				//Pawn
				while(selectedPawn == null || map.get(selectedPawn).isEmpty()) 
					Thread.sleep(1);
				user.sendMessage(Protocol.HIGHLIGHT_LOC_FULL, selectedPawn.getLocation());
				
				
				//Dest
				validLocations = map.get(selectedPawn);
				while(selectedDest == null && !canceled) 
					Thread.sleep(1);
				highlightedPath = null;
				user.sendMessage(Protocol.HIGHLIGHT_CLEAR);
			}
			
			//make the move
			BoardLocation finalDest =  board.afterSlide(selectedPawn, selectedDest);
			int used = board.dist(selectedPawn.getLocation(), selectedDest, color);
			int distToHome = board.dist(finalDest, board.getHomeTile(selectedPawn.getColor()).getLocation(), color);
			
			boolean swap = false;
			if (selectedCard.getType() == Type.CARD_11 && used != 11)
				swap = true;
			
			List<Pawn> sorrydPawns = board.getSorrydPawns(selectedPawn, selectedDest, swap);
			
			Move move = new Move(selectedCard, selectedPawn, selectedDest, finalDest, sorrydPawns, used, distToHome, swap);
			
			selectedCard.setPawn(selectedPawn);
			if (selectedCard.getType() == Type.CARD_7)
				selectedCard.used( move.distanceUsed() );
			else
				selectedCard.invalidate();
			
			task.cancel();
			board.execute(move);
			//game.alertUpdate();
		}
		
		myTurn = false;
	}
	
	/**
	 * Computer Move makes a move without user input. <br>
	 * This method is only called after the user that was controlling this NetworkPlayer disconnects. <br>
	 * If the user disconnects in the middle of a turn, this method will use the card that was drawn (if applicable), <br>
	 * but will not use the selected pawn (if applicable). <br>
	 * In the case of a card that allows multiple moves (eg. Card 7), this method will use any remaining spaces left.
	 */
	private void computerMove()
	{
		myTurn = true;
		
		if (selectedCard == null)
		{
			selectedCard = board.drawCard();
			game.alertDraw(this, selectedCard);
		
			this.hasDraw = false;
			if (selectedCard.getType() == Card.Type.CARD_2)
				this.hasDraw = true;
		}
		
		//make moves until card is invalid (mostly for Card7)
		while (selectedCard.valid())
		{
			Map<Pawn, Set<BoardLocation>> map = board.getValidLocations(pawns, selectedCard);
			if (!hasValidMoves(map))
			{
				reset();
				selectedCard = null;
				return;
			}
			
			PriorityQueue<Move> moves = new PriorityQueue<Move>(new Comparator<Move>(){
				public int compare(Move m1, Move m2)
				{
					return m2.score() - m1.score();
				}
			});
			
			
			//create all possible moves
			for (Pawn pawn : map.keySet())
			{
				for (BoardLocation loc : map.get(pawn))
				{
					Color color = pawn.getColor();
					
					BoardLocation finalDest =  board.afterSlide(pawn, loc);
					int used = board.dist(pawn.getLocation(), loc, color);
					int distToHome = board.dist(finalDest, board.getHomeTile(pawn.getColor()).getLocation(), color);
					
					boolean swap = false;
					if (selectedCard.getType() == Type.CARD_11 && used != 11)
						swap = true;
					
					List<Pawn> sorrydPawns = board.getSorrydPawns(pawn, loc, swap);
					
					moves.add(new Move(selectedCard, pawn, loc, finalDest, sorrydPawns, used, distToHome, swap));
				}
			}
			
			
			//select highest scoring move
			Move move = moves.peek();
			
			selectedCard.setPawn(move.getPawn());
			if (selectedCard.getType() == Type.CARD_7)
				selectedCard.used( move.distanceUsed() );
			else
				selectedCard.invalidate();
			
			board.execute(move);
			//game.alertUpdate();
		}
		
		
		myTurn = false;
		
		reset();
		selectedCard = null;
	}
	
	
	/**
	 * Called when game ends with the Player that won. <br>
	 * 
	 * 
	 * @param winner - Player that won
	 */
	@Override
	public void notifyWinner(Player winner)
	{
		String title = "";
		String message = "";
		
		boolean won = false;
		String winColor = Util.colorName(winner.getColor());
		
		int score = board.getScore(color);
		if (winner == this)
		{
			won = true;
			title = "Congratulations!";
			message = "You won with " + score + " points";
		}
		else
		{
			title = "Maybe next time!";
			message = "Player " + winColor + " won, but you got " + score + " points";
		}
		
		user.sendMessage(Protocol.NAME_REQUEST, new Object[]{won, title, message, score});
		user.removeMessageListener(this);
	}
	
	
	
	/* ======== MESSAGE LISTENER ========= */
	/**
	 * Called when the associated client sends a message. <br>
	 * Parse all protocols related player move, but don't take action on chat (parsed by NetworkGame).
	 * 
	 * @param sw
	 * @param message
	 */
	@Override
	public void didReceiveMessage(SocketWrapper sw, Message message)
	{
		if (!myTurn)
			return;
		
		String head = message.head();
		Object body = message.body();
		
		if (head.equals(Protocol.CHAT))
			return;
		
		if (head.equals(Protocol.CARD_CLICK))
		{
			selectedCard = board.drawCard();
			return;
		}
		
		BoardLocation loc = (BoardLocation) body;
		if (head.equals(Protocol.HOVER))
		{
			if (selectedPawn == null && validStarts != null && validStarts.contains(loc) && highlightedStart == null)
			{
				user.sendMessage(Protocol.HIGHLIGHT_LOC_SEMI, loc);
				highlightedStart = loc;
			}
			else if (selectedPawn != null && validLocations != null && validLocations.contains(loc) && highlightedPath == null)
			{
				LinkedList<BoardLocation> path = board.path(selectedPawn.getLocation(), loc, color);
				user.sendMessage(Protocol.HIGHLIGHT_PATH_SEMI, path);
				highlightedPath = path;
			}
		}
		else if (head.equals(Protocol.UNHOVER))
		{
			if (selectedPawn == null && highlightedStart != null)
			{
				user.sendMessage(Protocol.HIGHLIGHT_CLEAR_LOC, loc);
				highlightedStart = null;
			}
			if (selectedPawn != null && highlightedPath != null)
			{
				user.sendMessage(Protocol.HIGHLIGHT_CLEAR_PATH, highlightedPath);
				highlightedPath = null;
			}
		}
		else if (head.equals(Protocol.CLICK_LEFT))
		{
			if (selectedPawn == null)
			{
				Pawn p = board.getPawn(loc);
				if (p != null && isMyPawn(p))
					selectedPawn = p;
			}
			else if (selectedDest == null)
			{
				if (validLocations != null && validLocations.contains(loc))
				{
					selectedDest = loc;
					canceled = false;
				}
			}
		}
		else if (head.equals(Protocol.CLICK_RIGHT))
		{
			if (selectedPawn != null)
			{
				selectedPawn = null;
				selectedDest = null;
				canceled = true;
			}
		}
	}



	/* ======== DISCONNECT LISTENER ========= */
	/**
	 * 
	 * 
	 * @param sw
	 */
	@Override
	public void didDisconnect(SocketWrapper sw)
	{
		game.alertDisconnect(this);
		
		disconnected = true;
		if (currentThread != null)
			currentThread.interrupt();
		
		try
		{
			user.getSocket().close();
		}
		catch (IOException e)
		{
			//error handling?
		}
	}
	
	
	/* ======== HELPER ========= */
	/**
	 * Returns whether the given Map of Pawns to their Set of valid BoardLocations
	 * has any valid moves for any pawn.
	 * 
	 * @param map
	 * @return
	 */
	private boolean hasValidMoves(Map<Pawn, Set<BoardLocation>> map)
	{
		for (Pawn p : map.keySet())
		{
			if (!map.get(p).isEmpty())
				return true;
		}
		return false;
	}
}
