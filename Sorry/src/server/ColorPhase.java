package server;

import game.Game;

import java.io.IOException;
import java.io.Serializable;
import java.util.Hashtable;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import network.Message;
import network.Protocol;
import network.SocketWrapper;

/**
 * ColorPhase is the starting phase of a SorryServer.
 * It stores the currently connected users, and is the delegate to their inputs.
 * When it reads that a new color has been selected, relays that message to all other users.
 * 
 * @author Bohui Moon
 */
public class ColorPhase implements SocketWrapper.MessageListener
{
	/**
	 * Selection acts like a struct and stores a color and confirmed flag. 
	 */
	class Selection
	{
		public int color = -1;
		public boolean confirmed = false;
	}
	
	
	/* Constants */
	private final int TIME_LIMIT = 30;
	
	
	/* IVARS */	
	private SocketWrapper host;
	
	private int total;
	private int selected;
	private int confirmed;
	private SocketWrapper[] colors;
	private Map<SocketWrapper, Selection> selections;
	
	private Timer timer;
	private Map<SocketWrapper, TimerTask> kickTimers;
	
	private boolean finished;
	
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a ColorPhase 
	 */
	public ColorPhase(int players)
	{
		host = null;
				
		total = players;
		selected = 0;
		confirmed = 0;
		
		colors = new SocketWrapper[Game.NUM_COLORS];
		selections = new Hashtable<SocketWrapper, Selection>();
		
		timer = new Timer();
		kickTimers = new Hashtable<SocketWrapper, TimerTask>();
		
		finished = false;
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * Add the given SocketWrapper to the list of monitored.
	 * 
	 * @param sw - SocketWrapper to add
	 */
	public void addUser(SocketWrapper sw)
	{
		sw.addMessageListener(this);
		selections.put(sw, new Selection());
		
		//for host
		if (host == null)
			host = sw;
	}
	
	/**
	 * Kick the current player 
	 * 
	 * @param sw
	 */
	private void kick(SocketWrapper sw)
	{
		sw.sendMessage(Protocol.TIMER_DONE);
		
		try
		{
			sw.getSocket().close();
		}
		catch (IOException e)
		{
			System.out.println("[Error@ColorPhase.kick()] failed to force kick through Socket.close(): " + e.getMessage());
		}
	}
	
	/**
	 * Remove the given SocketWrapper from the list of monitored.
	 * 
	 * @param sw - SocketWrapper to remove
	 */
	public void removeUser(SocketWrapper sw)
	{
		Selection choice = selections.get(sw);
		if (choice == null)
			return; //not registered user
		
		//decrement confirmed if user had confirmed
		if (choice.confirmed)
		{
			confirmed--;
			host.sendMessage(Protocol.ENABLE_CONFIRM, false);
		}
		
		//decrement selected and notify of deselection
		int color = choice.color;
		if (color >= 0)
		{
			selected--;
			colors[color] = null;
			sendAll(new Message(Protocol.COLOR_DESELECT, color));
		}
		
		selections.remove(sw);
	}
	
	/**
	 * Process the color selection phase until host confirms,
	 * and the finished flag is raised.
	 * 
	 * @throws InterruptedException
	 */
	public void process() throws InterruptedException
	{
		finished = false;
		while (!finished)
			Thread.sleep(1);
		
		for (SocketWrapper sw : selections.keySet())
			sw.removeMessageListener(this);
	}
	
	/**
	 * Returns an array of SocketWrappers where each index corresponds to a color.
	 * Null values indicate that color was not selected.
	 * 
	 * @return array mapping each color index to a SocketWrapper
	 */
	public SocketWrapper[] getColors()
	{
		return colors;
	}
	
	
	/* ======== MESSAGE LISTENER ======== */
	/**
	 * Called by any of the currently connect SocketWrappers when user clicks a button.
	 * Depending on the action message sent, notify all other clients of the action.
	 */
	@Override
	public void didReceiveMessage(SocketWrapper sw, Message message)
	{
		Selection choice = selections.get(sw);
		if (choice == null)
			return; //not a registered socket
		
		String head = message.head();
		Object body = message.body();
		
		if (head.equals(Protocol.COLOR_READY))
		{
			//when client says it's ready, send already selected colors
			for (Selection s : selections.values())
			{
				if (s.color >= 0)
					sw.sendMessage(Protocol.COLOR_SELECT, s.color);
			}
			
			//timers
			if (host == sw)
			{
				sw.sendMessage(Protocol.TIMER_STOP);
				return;
			}
			
			//non-hosts have a timer
			sw.sendMessage(Protocol.TIMER_START, TIME_LIMIT);
			
			TimerTask kickTimer = new TimerTask(){
				public void run()
				{
					kick(sw);
				}
			};
			kickTimers.put(sw, kickTimer);
			timer.schedule(kickTimer, TIME_LIMIT * 1000);
			
			return;
		}
		
		//parse index
		int index = (int)body;
		if (head.equals(Protocol.COLOR_SELECT))
		{
			if (colors[index] != null)
				return; //color already taken
				
			int prev = choice.color;
			if (prev >= 0)
			{
				selected--;
				colors[prev] = null;
				sendAll(Protocol.COLOR_DESELECT, prev);
			}
			
			selected++;
			colors[index] = sw;
			choice.color = index;
			sendAll(Protocol.COLOR_SELECT, index);
			
			if (sw != host)
				sw.sendMessage(Protocol.ENABLE_CONFIRM, true);
			
			//if host selects and everyone else already selected
			if (confirmed == total - 1 && selected == total)
				host.sendMessage(Protocol.ENABLE_CONFIRM, true);
		}
		else if (head.equals(Protocol.COLOR_DESELECT))
		{
			if (colors[index] == null)
				return;
			
			selected--;
			choice.color = -1;
			colors[index] = null;
			sendAll(message);
		}
		else if (head.equals(Protocol.COLOR_CONFIRM))
		{
			if (colors[index] == null || colors[index] != sw)
				return; //given color not set or not user's select
			
			confirmed++;
			choice.confirmed = true;
			
			if (sw == host)
			{
				sendAll(Protocol.COLOR_FINISHED, null);
				finished = true; //move on to game
			}
			else
			{
				//cancel kick timer
				sw.sendMessage(Protocol.TIMER_STOP);
				TimerTask task = kickTimers.get(sw);
				if (task != null)
					task.cancel();
				
				//last user confirms and host already has selection
				if (confirmed == total - 1 && selected == total)
					host.sendMessage(Protocol.ENABLE_CONFIRM, true);
			}
		}
	}
	
	
	/* ======== HELPERS ======= */
	/**
	 * Send the given head and body as a Message to all connected clients.
	 * 
	 * @param head - String head to send
	 * @param body - Serializable body to send
	 */
	private void sendAll(String head, Serializable body)
	{
		for (SocketWrapper user : selections.keySet())
			user.sendMessage(head, body);
	}
	
	/**
	 * Send the given Message to all connected clients.
	 * 
	 * @param message - Message to send
	 */
	private void sendAll(Message message)
	{
		for (SocketWrapper user : selections.keySet())
			user.sendMessage(message);
	}
}
