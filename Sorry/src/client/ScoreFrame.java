package client;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import score.ScoreManager;
import score.ScoreManager.Score;

/**
 * 
 * 
 * @author Bohui Moon
 */
public class ScoreFrame extends JFrame
{
	private static final long serialVersionUID = 1982392874234L;
	
	
	/* IVARS */
	private JTable table;
	private ScoreManager manager;
	
	
	/* ========= CONSTRUCTOR ======== */
	/**
	 * Create a new ScoreFrame with a table and scroll pane
	 */
	public ScoreFrame(ScoreManager sm)
	{	
		table = new JTable();
		table.setAutoCreateRowSorter(true);
		
		JScrollPane pane = new JScrollPane(table);
		this.add(pane);
		this.setSize(250, 400);
		
		manager = sm;
	}
	
	
	/**
	 * Read
	 * 
	 * @param filename
	 */
	public void update()
	{
		DefaultTableModel model = new DefaultTableModel(new String[]{"Names", "Scores"}, 0);
		table.setModel(model);
		
		for (Score s : manager.getScores())
			model.addRow(new Object[]{s.name, s.score});
	}
}
