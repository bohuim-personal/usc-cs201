package client;

import extension.ImageButton;
import game.Board;
import game.BoardLocation;
import game.Card;
import game.Game;
import game.Pawn;
import game.Tile.TileType;
import game.Util;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.Timer;


/**
 * 
 * 
 * @author Bohui Moon
 */
public class BoardDisplay extends JPanel implements ActionListener
{
	private static final long serialVersionUID = 5089438509384L;	
	
	/**
	 * 
	 */
	public static interface Delegate
	{
		public void cardClicked();
		public void hover(BoardLocation loc);
		public void unhover(BoardLocation loc);
		public void leftClick(BoardLocation loc);
		public void rightClick(BoardLocation loc);
	}
	
	
	/* IVARS */
	private Delegate delegate;
	
	private JButton cardButton;
	
	private JLabel[] homeLabels;
	private JLabel[] startLabels;
	private Map<BoardLocation, TileDisplay> tiles;
	private Set<BoardLocation> highlighted;
	
	private int limit;
	private Timer timer;
	
	
	/* ======== CONSTRUCTOR & SETUP ======== */
	/**
	 * Create a BoardDisplay under GridBagLayout and call setup methods.
	 */
	public BoardDisplay()
	{
		delegate = null;
		
		homeLabels = new JLabel[Game.NUM_COLORS];
		startLabels = new JLabel[Game.NUM_COLORS];
		tiles = new HashMap<BoardLocation, TileDisplay>();
		highlighted = new HashSet<BoardLocation>();
		
		limit = 0;
		timer = new Timer(1000, new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				if (limit <= 0)
				{
					timer.stop();
					return;
				}
				
				--limit;
				BoardDisplay.this.repaint();
			}
		});
		
		this.setLayout(new GridBagLayout());
		addCard();
		createBoard();
	}
	
	/**
	 * Add card label and deck button.
	 * 
	 * @param gbc
	 */
	private void addCard()
	{
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = gbc.weighty = 1;
		
		JLabel deckLabel = new JLabel("Cards:");
		deckLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		cardButton = new ImageButton(new ImageIcon(Constants.CARDBACK_RED).getImage());
		cardButton.setOpaque(false);
		cardButton.setBorderPainted(false);
		cardButton.setContentAreaFilled(false);
		cardButton.addActionListener(this);
		
		gbc.gridy = 8;
		gbc.gridx = 7;
		this.add(deckLabel, gbc);
		gbc.gridx = 8;
		gbc.anchor = GridBagConstraints.CENTER;
		this.add(cardButton, gbc);
	}
	
	/**
	 * 
	 * @param gbc
	 */
	private void createBoard()
	{
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = gbc.weighty = 1;
		
		Point[] starts = {new Point(11,14), new Point(1,11), new Point(4,1), new Point(14,4)};
		Point[] safety = {new Point(13,14), new Point(1,13), new Point(2,1), new Point(14,2)};
		Point[] homes  = {new Point(13, 9), new Point(6,13), new Point(2,6), new Point(9, 2)};
		Point[] slabels = {new Point(11,13), new Point(2,11), new Point(4,2), new Point(13,4)};
		Point[] hlabels = {new Point(13,8), new Point(7,13), new Point(2,7), new Point(8,2)};
		
		int row = 15;
		int col = 15;
		
		for (int zone = 0; zone < Game.NUM_COLORS; zone++)
		{
			//starts
			{
				BoardLocation loc = new BoardLocation(TileType.START, zone, -1);
				TileDisplay td = new TileDisplay(this, loc);
				tiles.put(loc, td);
				
				gbc.gridy = starts[zone].y;
				gbc.gridx = starts[zone].x;
				this.add(td, gbc);
			}
			
			//start labels 
			{
				JLabel label = new JLabel("0", SwingConstants.CENTER);
				label.setFont(new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 16));
				startLabels[zone] = label;
				
				gbc.gridy = slabels[zone].y;
				gbc.gridx = slabels[zone].x;
				this.add(label, gbc);
			}
			
			//path
			for (int i = 0; i < Board.PATH_LENGTH; i++)
			{
				BoardLocation loc = new BoardLocation(TileType.PATH, zone, i);
				TileDisplay td = new TileDisplay(this, loc);
				tiles.put(loc, td);
				
				if (zone == 0) col--;
				else if (zone == 1) row--;
				else if (zone == 2) col++;
				else if (zone == 3) row++;
				
				gbc.gridy = row;
				gbc.gridx = col;
				this.add(td, gbc);
			}
			
			//safety
			for (int i = 0; i < Board.SAFETY_LENGTH; i++)
			{
				BoardLocation loc = new BoardLocation(TileType.SAFETY, zone, i);
				TileDisplay td = new TileDisplay(this, loc);
				tiles.put(loc, td);
				
				gbc.gridy = safety[zone].y;
				gbc.gridx = safety[zone].x;
				
				if (zone == 0) 		gbc.gridy -= i;
				else if (zone == 1) gbc.gridx += i;
				else if (zone == 2) gbc.gridy += i;
				else if (zone == 3) gbc.gridx -= i;
				this.add(td, gbc);
			}
			
			//home
			{
				BoardLocation loc = new BoardLocation(TileType.HOME, zone, -1);
				TileDisplay td = new TileDisplay(this, loc);
				tiles.put(loc, td);
				
				gbc.gridy = homes[zone].y;
				gbc.gridx = homes[zone].x;
				this.add(td, gbc);
			}
			
			//home labels
			{
				JLabel label = new JLabel("0", SwingConstants.CENTER);
				label.setFont(new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 16));
				homeLabels[zone] = label;
				
				gbc.gridy = hlabels[zone].y;
				gbc.gridx = hlabels[zone].x;
				this.add(label, gbc);
			}
		}
	}
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * Set the delegate to the given one.
	 * 
	 * @param delegate - new Delegate to relay events to
	 */
	public void setDelegate(Delegate delegate)
	{
		this.delegate = delegate;
	}
	
	
	
	/* ======== UI ========= */
	/**
	 * 
	 * @param start
	 */
	public void startTimer(int start)
	{
		limit = start;
		
		if (!timer.isRunning())
			timer.start();
	}
	
	/**
	 * Ask the user for a name using a JOptionPane InputDialog using the given parameters.
	 * 
	 * @param title - String title to display
	 * @param after - String message to display
	 * @return String name user input
	 */
	public String askName(String title, String after)
	{
		String message = "";
		message += "<html>";
		message += "<div>";
		message += title;
		message += "</div>";
		message += "<div style='margin-top: 10px;'>";
		message += after;
		message += "</div>";
		message += "<div style='margin-top: 10px;'>";
		message += "Enter your name:";
		message += "</div>";
		message += "</html>";
		return JOptionPane.showInputDialog(this, message, "Sorry!", JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Alert the given Card using CardDialog.
	 * 
	 * @param card - Card to show
	 */
	public void alertDraw(Color color, Card card)
	{
		new CardDialog(SwingUtilities.getWindowAncestor(this), color, card);
	}
	
	/**
	 * Update the board display.
	 */
	public void update(List<Pawn> pawns)
	{
		//clear
		int start[] = new int[Game.NUM_COLORS];
		int home[]  = new int[Game.NUM_COLORS];
		for (TileDisplay td : tiles.values())
			td.setIcon(null);
		
		
		//update
		for (Pawn p : pawns)
		{
			Color c = p.getColor();
			int index = Util.indexFromColor(c);
			BoardLocation loc = p.getLocation();
			
			if (loc == null){}
			else if (loc.isStart())
				++start[index];
			else if (loc.isHome())
				++home[index];
			else
			{
				Image icon = Constants.PAWN_IMAGES[index];
				tiles.get(loc).setIcon(icon);
			}
		}
		
		for (int i = 0; i < Game.NUM_COLORS; i++)
		{
			startLabels[i].setText("" + start[i]);
			homeLabels[i].setText("" + home[i]);
		}
	}
	
	/**
	 * Go through set of highlighted tiles and unhighlight them.
	 */
	public void unhighlightAll()
	{	
		for (BoardLocation loc : highlighted)
			tiles.get(loc).setHighlight(null);
		
		highlighted.clear();
	}
	
	/**
	 * Un-highlight the BoardLocations given in the Iterable.
	 * 
	 * @param locs
	 */
	public void unhighlight(Iterable<BoardLocation> locs)
	{
		for (BoardLocation loc : locs)
		{
			tiles.get(loc).setHighlight(null);
			highlighted.remove(loc);
		}
	}
	
	/**
	 * Un-highlight the BoardLocation given
	 * 
	 * @param loc - BoardLocation to un-higlight
	 */
	public void unhighlight(BoardLocation loc)
	{
		tiles.get(loc).setHighlight(null);
		highlighted.remove(loc);
	}
	
	/**
	 * Highlight the BoardLocations given in the Iterable.
	 * 
	 * @param locs
	 * @param color
	 */
	public void highlight(Iterable<BoardLocation> locs, Color color)
	{
		if (locs == null)
			return;
		
		for (BoardLocation loc : locs)
		{
			tiles.get(loc).setHighlight(color);
			highlighted.add(loc);
		}
	}
	
	/**
	 * Highlight the BoardLocation given with the given color
	 * 
	 * @param loc - BoardLocaiton to tint
	 * @param color - RGB values to use for tint
	 */
	public void highlight(BoardLocation loc, Color color)
	{
		tiles.get(loc).setHighlight(color);
		highlighted.add(loc);
	}
	
	
	
	/* ======== EVENTS ======== */
	/**
	 * Called when user clicks the card button
	 * 
	 * @param e - ActionEvent fired
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		if (delegate != null)
			delegate.cardClicked();
	}
	
	/**
	 * Called by TileDisplay when user hover over it.
	 * 
	 * @param location - BoardLocation of TileDisplay hovered
	 */
	public void hover(BoardLocation location)
	{
		if (delegate != null)
			delegate.hover(location);
	}
	
	/**
	 * Called by TileDisplay when user hover stops.
	 * 
	 * @param location - BoardLocation of TileDisplay un-hovered
	 */
	public void unhover(BoardLocation location)
	{
		if (delegate != null)
			delegate.unhover(location);
	}
	
	/**
	 * Called by BoardTile when user clicks it. <br>
	 * Relay the message to the current delegate.
	 * 
	 * @param location - BoardLocation of TileDisplay left-clicked
	 */
	public void leftClick(BoardLocation location)
	{
		if (delegate != null)
			delegate.leftClick(location);
	}
	
	/**
	 * Called by TileDisplay when user right-clicks it. <br>
	 * 
	 * @param location - BoardLocation of TileDisplay right-clicked
	 */
	public void rightClickAt(BoardLocation location)
	{
		if (delegate != null)
			delegate.rightClick(location);
	}
	
	
	/* ======== OVERRIDE ======== */
	/**
	 * Draw the Sorry logo in the center of the board
	 */
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		//dimensions
		int w = this.getWidth();
		int h = this.getHeight();
		
		//sorry logo
		int imgW = (int)(w * (2.8/7));
		int imgH = (int)(h * (1.0/6));
		g.drawImage(Constants.LOGO, w/2 - imgW/2 + 20, h/2 - imgH - 20, imgW, imgH, null);
		
		//timer label
		FontMetrics fm = g.getFontMetrics();
		
		String time = "TIME 0:" + Constants.secondsToString(limit);
		int timeW = fm.stringWidth(time);
		
		g.setFont(Constants.messageFont(Font.PLAIN, 20));
		g.drawString(time, w/2 - timeW/2, h/2 + 70);
	}
}
