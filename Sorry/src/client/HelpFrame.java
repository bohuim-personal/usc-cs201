package client;

import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * HelpFrame is a JFrame windows showing some Help text
 * 
 * @author Bohui Moon
 */
public class HelpFrame extends JFrame
{
	private static final long serialVersionUID = -84723928498312039L;
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a new help frame
	 */
	public HelpFrame()
	{
		super("Sorry! - Help");
		
		this.setCursor(Constants.CURSOR_HAND_GRAY);
		
		String text = "";
		text += "Help\n\n";
		text += "Starting a round: \n";
		text += "    Press 'Start' \n";
		text += "    Select number of players (including yourself) \n";
		text += "    Select your team color \n\n";
		text += "Playing the game: \n";
		text += "    Draw a card by pressing the card button in the middle \n";
		text += "    Select a pawn by clicking it\n";
		text += "    Select a location you want to move it to\n";
		text += "    Hovering over any square highlights it if the selection is valid\n";
		text += "    Land all four of your pawn at Home first to win\n\n";
		text += "Scores: \n";
		text += "    Enter the name of the winning player after the game\n";
		
		JTextArea area = new JTextArea();
		area.setText(text);
		area.setFont(new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 18));
		area.setEditable(false);
		
		JScrollPane pane = new JScrollPane(area);
		this.add(pane);
		
		this.setSize(300, 450);
	}
}
