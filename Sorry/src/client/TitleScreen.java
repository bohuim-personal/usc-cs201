package client;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import extension.GrayButton;
import extension.ImagePanel;

/**
 * TitleScreen panel shows the Sorry logo and a start button.
 * 
 * @author Bohui Moon
 */
public class TitleScreen extends ImagePanel
{
	private static final long serialVersionUID = 2837419318129L;
	
	public static final int SELECTION_HOST = 0;
	public static final int SELECTION_JOIN = 1;
	
	
	/* IVARS */
	private int choice;
	private boolean waiting;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Initialize 
	 */
	public TitleScreen()
	{
		super( new ImageIcon(Constants.PANEL_GRAY).getImage() );
		
		//logo	
		JLabel logoLabel = new JLabel(new ImageIcon(Constants.LOGO));
		logoLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		//buttons
		JPanel buttonRow = new JPanel();
		buttonRow.setBackground(new Color(0,0,0,0));
		buttonRow.setLayout(new BoxLayout(buttonRow, BoxLayout.X_AXIS));
			Dimension buttonSize = new Dimension(120, 40);
			GrayButton hostButton = new GrayButton("Host", 18);
			hostButton.setPreferredSize(buttonSize);
			hostButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					choice = SELECTION_HOST;
					waiting = false;
				}
			});
			
			GrayButton joinButton = new GrayButton("Join", 18);
			joinButton.setPreferredSize(buttonSize);
			joinButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					choice = SELECTION_JOIN;
					waiting = false;
				}
			});
		buttonRow.add(Box.createHorizontalGlue());
		buttonRow.add(hostButton);
		buttonRow.add(Box.createRigidArea(new Dimension(120, 0)));
		buttonRow.add(joinButton);
		buttonRow.add(Box.createHorizontalGlue());
		
		//layout
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(Box.createVerticalGlue());
		add(logoLabel);
		add(Box.createVerticalGlue());
		add(buttonRow);
		add(Box.createVerticalGlue());
	}
	
	/**
	 * 
	 */
	public int start() throws InterruptedException
	{
		choice = -1;
		waiting = true;
		
		while(waiting)
			Thread.sleep(1);
		
		return choice;
	}
}
