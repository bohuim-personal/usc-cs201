package client;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import extension.GrayButton;
import extension.ImagePanel;

/**
 * JoinScreen allows the user to start by joining a created game.
 * User types an IP and port of the server to connect
 * 
 * @author Bohui Moon
 */
public class JoinScreen extends ImagePanel implements ActionListener
{
	private static final long serialVersionUID = 40239409109L;

	/* IVARS */
	private boolean waiting;
	private String ip;
	private int port;
	
	private JTextField ipField;
	private JTextField portField;
	private JButton connectButton;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a JoinScreen with labels, fields, and connect button.
	 * Specify a negative port to leave field blank on initialization.
	 * 
	 * @param defaultIP - IP to be already entered.
	 * @param defaultPort - port to be already entered.
	 */
	public JoinScreen(String defaultIP, int defaultPort)
	{
		super( new ImageIcon(Constants.PANEL_GRAY).getImage() );
		
		String startPort = (defaultPort < 0) ? "" : Integer.toString(defaultPort);		
		Font font = new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 30);
		
		//ip bar
		JPanel ipBar = new JPanel();
		ipBar.setLayout(new BoxLayout(ipBar, BoxLayout.X_AXIS));
		ipBar.setMaximumSize(new Dimension(400, 50));
			JLabel ipLabel = new JLabel("IP:");
			ipLabel.setFont(font);
			
			ipField = new JTextField(defaultIP); 
			ipField.setFont(font);
			ipField.setMaximumSize(new Dimension(250, 50));
			ipField.setPreferredSize(new Dimension(250, 50));
			ipField.addActionListener(this);
		ipBar.add(Box.createHorizontalGlue());
		ipBar.add(ipLabel);
		ipBar.add(ipField);
		ipBar.add(Box.createHorizontalGlue());
		
		//port bar
		JPanel portBar = new JPanel();
		portBar.setLayout(new BoxLayout(portBar, BoxLayout.X_AXIS));
		portBar.setMaximumSize(new Dimension(400, 50));
			JLabel portLabel = new JLabel("Port:");
			portLabel.setFont(font);
			
			portField = new JTextField(startPort);
			portField.setFont(font);
			portField.setMaximumSize(new Dimension(120, 50));
			portField.setPreferredSize(new Dimension(120, 50));
			portField.addActionListener(this);
		portBar.add(Box.createHorizontalGlue());
		portBar.add(portLabel);
		portBar.add(portField);
		portBar.add(Box.createHorizontalGlue());
		
		//button
		Dimension buttonSize = new Dimension(120, 40);
		connectButton = new GrayButton("Connect", 18);
		connectButton.setMaximumSize(buttonSize);
		connectButton.setPreferredSize(buttonSize);
		connectButton.addActionListener(this);
		connectButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		
		//layout
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(Box.createVerticalGlue());
		add(ipBar);
		add(portBar);
		add(Box.createVerticalGlue());
		add(connectButton);
		add(Box.createVerticalGlue());
	}
	
	/**
	 * This methods blocks until valid IP and port values are enetered.
	 * These values don't necessary need to exist, 
	 * they just need to be actual IP and port numbers.
	 */
	public void setConnection() throws InterruptedException
	{
		waiting = true;
		while (waiting)
			Thread.sleep(1);
	}
	
	/**
	 * @return currently set ip
	 */
	public String getIP()
	{
		return ip;
	}
	
	/**
	 * @return currently set port
	 */
	public int getPort()
	{
		return port;
	}
	
	
	/**
	 * Called when user pressed enter on either field, or clicks connect button.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		Object source = e.getSource();
		if (source == ipField)
		{
			portField.requestFocusInWindow();
		}
		else if (source == portField)
		{
			ip = ipField.getText();
			port = Integer.parseInt(portField.getText());
			waiting = false;
		}
		else if (source == connectButton)
		{
			ip = ipField.getText();
			port = Integer.parseInt(portField.getText());
			waiting = false;
		}
	}
}
