package client;

import javax.sound.sampled.Clip;

/**
 * Singleton class for plyaing sounds.
 * 
 * @author Bohui Moon
 */
public class SoundManager
{
	private static final SoundManager manager = new SoundManager();
	
	
	
	
	
	/* ======= CONSTRUCTOR ========= */
	/**
	 * Create a new SoundManager, this sound only be called once by the static constant manager
	 */
	private SoundManager()
	{
		
	}
	
	/**
	 * @return SoundManager Singleton instance 
	 */
	public static SoundManager sharedManager()
	{
		return manager;
	}
	
	
	/* ======== METHODS ======== */
	/**
	 * Play the given clip from the beginning
	 * 
	 * @param clip - sound Clip to play
	 */
	public void play(Clip clip)
	{
		clip.stop();
		clip.setFramePosition(0);
		clip.start();
	}
}
