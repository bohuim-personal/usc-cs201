package client;

import extension.ImagePanel;
import game.Card;
import game.Util;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Window;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;

/**
 * CardDialog presents 
 * 
 * @author Bohui Moon
 */
public class CardDialog extends JDialog
{
	private static final long serialVersionUID = -39849238491239L;
	
	private static final Dimension MIN_SIZE = new Dimension(270,400);
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a CardDialog showing information about the given card.
	 * 
	 * @param card - Card to display
	 */
	public CardDialog(Window window, Color color, Card card)
	{
		//super(window, Dialog.ModalityType.APPLICATION_MODAL);
		super(window, Dialog.ModalityType.MODELESS);
		
		this.setPreferredSize(MIN_SIZE);
		this.setMinimumSize(MIN_SIZE);
		this.setLocationRelativeTo(window);
		this.setCursor(Constants.CURSOR_HAND_GRAY);
		
		JPanel panel = new ImagePanel( new ImageIcon(Constants.CARD_BROWN).getImage() );
		panel.addMouseListener(new MouseAdapter(){
			public void mouseClicked(MouseEvent e)
			{
				CardDialog.this.dispose();
			}
		});
		
		this.setContentPane(panel);
		
		//card label
		JLabel colorLabel = new JLabel(Util.colorName(color));
		colorLabel.setFont(new Font(Constants.FONT_THIN_NAME, Font.BOLD, 20));
		
		JLabel cardLabel = new JLabel(card.getTypeString());
		cardLabel.setFont(new Font(Constants.FONT_THIN_NAME, Font.BOLD, 36));
		
		JLabel descLabel = new JLabel("<html><p align='center'>" + card.getDescription() + "</p></html>");
		descLabel.setFont(new Font(Constants.FONT_THIN_NAME, Font.BOLD, 16));
		descLabel.setHorizontalAlignment(SwingConstants.CENTER);
			
		//layout
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);	
			
		this.add(colorLabel);
		layout.putConstraint(SpringLayout.SOUTH, colorLabel, -20, SpringLayout.NORTH, cardLabel);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, colorLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel);
		
		this.add(cardLabel);
		layout.putConstraint(SpringLayout.SOUTH, cardLabel, -60, SpringLayout.VERTICAL_CENTER, panel);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, cardLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel);
		
		this.add(descLabel);
		layout.putConstraint(SpringLayout.WEST, descLabel, 20, SpringLayout.WEST, panel);
		layout.putConstraint(SpringLayout.EAST, descLabel, -20, SpringLayout.EAST, panel);
		layout.putConstraint(SpringLayout.NORTH, descLabel, 0, SpringLayout.VERTICAL_CENTER, panel);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, descLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel);
		
		this.setVisible(true);
	}
}
