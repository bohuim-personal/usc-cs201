package client;

import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SpringLayout;

import extension.ImagePanel;


/**
 * CountSelect allows user to select the numbers of players.
 * 
 * @author Bohui Moon
 */
public class CountScreen extends ImagePanel
{
	private static final long serialVersionUID = 548540930L;
	
	
	/* IVARS */
	private ButtonGroup group;
	private JButton confirmButton;
	
	private int choice;
	private boolean waiting;
	
	
	/* ========= CONSTRUCTOR ========= */
	/**
	 * Create a new CountSelect panel and setup GUI components.
	 */
	public CountScreen()
	{
		super( new ImageIcon(Constants.PANEL_GRAY).getImage() );
		choice = -1;
		
		
		//message
		JLabel message = new JLabel("SELECT THE NUMBER OF PLAYERS");
		message.setFont(Constants.MESSAGE_FONT);
	
		//confirm button
		confirmButton = Constants.getConfirmButton();
		confirmButton.setEnabled(false);
		confirmButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				waiting = false;
			}
		});
		
		//selection row
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Font rbFont = new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 25);
		ImageIcon iconDefault = new ImageIcon( toolkit.getImage(Constants.CHECK_CIRCLE_DEFAULT).getScaledInstance(22, 22, 0) );
		ImageIcon iconSelected = new ImageIcon( toolkit.getImage(Constants.CHECK_CIRCLE_SELECTED).getScaledInstance(22, 22, 0) );
		
		JRadioButton b2 = new JRadioButton("2");
			b2.setFont(rbFont);
			b2.setIcon(iconDefault);
			b2.setSelectedIcon(iconSelected);
			b2.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					confirmButton.setEnabled(true);
					choice = 2;
				}
			});
		JRadioButton b3 = new JRadioButton("3");
			b3.setFont(rbFont);
			b3.setIcon(iconDefault);
			b3.setSelectedIcon(iconSelected);
			b3.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					confirmButton.setEnabled(true);
					choice = 3;
				}
			});
		JRadioButton b4 = new JRadioButton("4");
			b4.setFont(rbFont);
			b4.setIcon(iconDefault);
			b4.setSelectedIcon(iconSelected);
			b4.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent e)
				{
					confirmButton.setEnabled(true);
					choice = 4;
				}
			});
		group = new ButtonGroup();
		group.add(b2);
		group.add(b3);
		group.add(b4);
			
		
		//main layout
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		
		this.add(b3);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, b3, 0, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, b3, 0, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(b2);
		layout.putConstraint(SpringLayout.EAST, b2, -180, SpringLayout.WEST, b3);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, b2, 0, SpringLayout.VERTICAL_CENTER, this);
		this.add(b4);
		layout.putConstraint(SpringLayout.WEST, b4, 180, SpringLayout.EAST, b3);
		layout.putConstraint(SpringLayout.VERTICAL_CENTER, b4, 0, SpringLayout.VERTICAL_CENTER, this);
		this.add(message);
		layout.putConstraint(SpringLayout.NORTH, message, 80, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, message, 0, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(confirmButton);
		layout.putConstraint(SpringLayout.EAST, confirmButton, -60, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, confirmButton, -50, SpringLayout.SOUTH, this);
	}
	
	
	/**
	 * Synchronously return the selected number of players chosen by user.
	 * 
	 * @return number of players chosen by user
	 * @throws InterruptedException
	 */
	public int getCount() throws InterruptedException
	{
		reset();
		
		waiting = true;
		while(waiting)
			Thread.sleep(1);
		
		return choice;
	}
	
	/**
	 * Clear radio selection, and disable confirm button.
	 */
	private void reset()
	{
		group.clearSelection();
		confirmButton.setEnabled(false);
	}
}
