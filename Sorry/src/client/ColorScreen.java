package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SpringLayout;
import javax.swing.SwingConstants;
import javax.swing.Timer;

import network.Message;
import network.Protocol;
import network.SocketWrapper;
import extension.ImagePanel;
import extension.ScaleIcon;
import game.Game;
import game.Util;


/**
 * ColorScreen is a JPanel that allows the user to select a team color for the game. <br>
 * 
 * <br>
 * Change log: <br>
 * 2.0 - takes in a SocketWrapper and sets itself as the delegate to the socket. <br>
 * 1.7 - switched to SpringLayout for smoother scaling <br>
 * 1.6 - buttons now have icons for different states <br>
 * 1.5 - extend ImagePanel and set a default border image as its background image. <br>
 * 1.2 - color "button" changed to actual JToggleButtons not panels <br>
 * 1.0 - added color buttons (panels) and confirm button <br>
 * 
 * @author Bohui Moon
 * @version 2.0
 */
public class ColorScreen extends ImagePanel implements ActionListener, SocketWrapper.MessageListener
{
	private static final long serialVersionUID = 1579328042834L;
	
	
	/* IVARS */
	private JButton[] colorButtons = new JButton[Game.NUM_COLORS];
	private JButton confirmButton;
	
	private int choice = -1;
	private boolean confirmed = false;
	
	private SocketWrapper sw;
	private boolean finished;
	
	private int limit;
	private Timer timer;
	private JLabel timerLabel;
	
	
	/* ======== CONSTRUTOR ======= */
	/**
	 * Create a ColorScreen
	 */
	public ColorScreen()
	{
		super( new ImageIcon(Constants.PANEL_GRAY).getImage() );
		
		
		//message
		JLabel message = new JLabel("SELECT YOUR COLOR");
		message.setFont(Constants.MESSAGE_FONT);
		
		//timer
		timerLabel = new JLabel("TIME 0:00");
		timerLabel.setFont(Constants.messageFont(Font.PLAIN, 24));
		timer = new Timer(1000, null);
		timer.setInitialDelay(0);
		timer.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				timerLabel.setText("TIME 0:" + Constants.secondsToString(limit));
				if (limit <= 0)
				{
					timer.stop();
					return;
				}
				
				--limit;
			}
		});
		
		//confirm button
		confirmButton = Constants.getConfirmButton();
		confirmButton.setEnabled(false);
		confirmButton.addActionListener(this);
		
		//toggle buttons
		Font tgFont = new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 24);
		
		String[] titles = {"red", "blue", "yellow", "green"};
		for (int i=0; i < colorButtons.length; i++)
		{
			Image[] icons = Constants.buttonsForIndex(i);
			
			JButton newButton = new JButton(titles[i]);
			newButton.setFont(tgFont);
			newButton.setFocusPainted(false);
			newButton.setBorderPainted(false);
			newButton.setHorizontalTextPosition(SwingConstants.CENTER);
			newButton.setIcon(new ScaleIcon(icons[0]));
			newButton.setPressedIcon(new ScaleIcon(icons[1]));
			newButton.setSelectedIcon(new ScaleIcon(icons[1]));
			newButton.addActionListener(this);
			
			colorButtons[i] = newButton;
		}
		
		
		//main layout
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		
		boolean verticalResize = true;
		
		this.add(message);
		layout.putConstraint(SpringLayout.NORTH, message, 40, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, message, 0, SpringLayout.HORIZONTAL_CENTER, this);
		
		this.add(timerLabel);
		layout.putConstraint(SpringLayout.NORTH, timerLabel, 0, SpringLayout.SOUTH, message);
		layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, timerLabel, 0, SpringLayout.HORIZONTAL_CENTER, this);
		
		this.add(confirmButton);
		layout.putConstraint(SpringLayout.EAST, confirmButton, -60, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, confirmButton, -50, SpringLayout.SOUTH, this);
		
		int sideMargin = 40;
		int heightMargin = 120;
		int buttonMargin = -3;
		
		this.add(colorButtons[0]);
		layout.putConstraint(SpringLayout.WEST, colorButtons[0], sideMargin, SpringLayout.WEST, this);
		if (verticalResize) layout.putConstraint(SpringLayout.NORTH, colorButtons[0], heightMargin, SpringLayout.NORTH, this);
		else 				layout.putConstraint(SpringLayout.NORTH, colorButtons[0], -heightMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.SOUTH, colorButtons[0], -buttonMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.EAST, colorButtons[0], -buttonMargin, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(colorButtons[1]);
		layout.putConstraint(SpringLayout.EAST, colorButtons[1], -sideMargin, SpringLayout.EAST, this);
		if (verticalResize) layout.putConstraint(SpringLayout.NORTH, colorButtons[1], heightMargin, SpringLayout.NORTH, this);
		else 				layout.putConstraint(SpringLayout.NORTH, colorButtons[1], -heightMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.SOUTH, colorButtons[1], -buttonMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.WEST, colorButtons[1], buttonMargin, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(colorButtons[3]);
		layout.putConstraint(SpringLayout.WEST, colorButtons[3], sideMargin, SpringLayout.WEST, this);
		if (verticalResize) layout.putConstraint(SpringLayout.SOUTH, colorButtons[3], -heightMargin, SpringLayout.SOUTH, this);
		else 				layout.putConstraint(SpringLayout.SOUTH, colorButtons[3], heightMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.NORTH, colorButtons[3], buttonMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.EAST, colorButtons[3], -buttonMargin, SpringLayout.HORIZONTAL_CENTER, this);
		this.add(colorButtons[2]);
		layout.putConstraint(SpringLayout.EAST, colorButtons[2], -sideMargin, SpringLayout.EAST, this);
		if (verticalResize) layout.putConstraint(SpringLayout.SOUTH, colorButtons[2], -heightMargin, SpringLayout.SOUTH, this);
		else 				layout.putConstraint(SpringLayout.SOUTH, colorButtons[2], heightMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.NORTH, colorButtons[2], buttonMargin, SpringLayout.VERTICAL_CENTER, this);
		layout.putConstraint(SpringLayout.WEST, colorButtons[2], buttonMargin, SpringLayout.HORIZONTAL_CENTER, this);
	}
	
	/**
	 * Set the given SocketWrapper as the one to communicate through.
	 * Revert the previous socket's delegate as null, and set this one as the new one's delegate.
	 * 
	 * @param newSW - new SocketWrapper to communicate through
	 */
	public void setSocketWrapper(SocketWrapper newSW)
	{
		if (sw != null)
			sw.addMessageListener(null);
		
		sw = newSW;
		sw.addMessageListener(this);
		sw.sendMessage(Protocol.COLOR_READY);
	}

	/**
	 * Run the color selection process and block calling Thread until finished flag is raised by server.
	 */
	public void process() throws InterruptedException
	{
		finished = false;
		while (!finished)
			Thread.sleep(1);
		
		sw.removeMessageListener(this);
	}
	
	/**
	 * @return chosen Color
	 */
	public Color getColor()
	{
		return Util.colorFromIndex(choice);
	}
	
	/**
	 * Reset all buttons and fields
	 */
	public void reset()
	{
		timerLabel.setVisible(true);
		
		confirmButton.setEnabled(false);
		for (int i=0; i < colorButtons.length; i++)
		{
			JButton button = colorButtons[i];
			button.setSelected(false);
			button.setForeground(Color.BLACK);
		}
		
		choice = -1;
		confirmed = false;
		finished = false;
		
		if (sw != null)
			sw.removeMessageListener(this);
		sw = null;
	}
	
	/**
	 * Start the timer with the given start upper limit.
	 * 
	 * @param start - int value of starting time limit in seconds
	 */
	private void startTimer(int start)
	{
		limit = start;
		timer.start();
	}
	
	/**
	 * Stop the timer and set the timer label to invisible
	 */
	private void stopTimer()
	{
		timer.stop();
		timerLabel.setVisible(false);
	}
	
	
	/* ======== Listener ========== */
	/**
	 * Called when user presses any of the buttons.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{		
		//choice already confirmed
		if (confirmed)
			return;
		
		Object source = e.getSource();
		if (source == confirmButton)
		{
			confirmed = true;
			confirmButton.setEnabled(false);
			sw.sendMessage(Protocol.COLOR_CONFIRM, choice);
			return;
		}
		
		for (int i=0; i < colorButtons.length; i++)
		{
			JButton button = colorButtons[i];
			if (source == button)
			{
				//can't select already selected
				if (button.isSelected())
					return;
				
				choice = i;
			}
		}
		
		sw.sendMessage(Protocol.COLOR_SELECT, choice);
	}
	
	/**
	 * Called when the associated SocketWrapper receives a message from connected socket. <br>
	 * Depending to the attached message head, parse the body accordingly.
	 */
	@Override
	public void didReceiveMessage(SocketWrapper sw, Message message)
	{
		String head = message.head();
		Object body = message.body();
		
		if (head.equals(Protocol.TIMER_START))
		{
			startTimer((int) body);
			return;
		}
		else if (head.equals(Protocol.TIMER_STOP))
		{
			stopTimer();
			return;
		}
		else if (head.equals(Protocol.TIMER_DONE))
		{
			finished = true;
			return;
		}
		else if (head.equals(Protocol.ENABLE_CONFIRM))
		{
			if (!confirmed)
				confirmButton.setEnabled((boolean) body);
			return;
		}
		else if (head.equals(Protocol.COLOR_FINISHED))
		{
			finished = true;
			return;
		}
		
		//parse index
		int index = (int) body;
		if (head.equals(Protocol.COLOR_SELECT))
		{
			Color fg = Color.GRAY;
			if (index == choice)
				fg = Color.WHITE;
			
			colorButtons[index].setForeground(fg);
			colorButtons[index].setSelected(true);
		}
		else if (head.equals(Protocol.COLOR_DESELECT))
		{
			colorButtons[index].setSelected(false);
			colorButtons[index].setForeground(Color.BLACK);;
		}
	}
}
