package client;

import game.BoardLocation;
import game.Card;
import game.Pawn;
import game.Util;

import java.awt.Color;
import java.io.IOException;
import java.util.List;

import network.Message;
import network.Protocol;
import network.SocketWrapper;
import score.ScoreManager;

/**
 * BoardDisplayManager manages the interaction between a BoardDisplay and Socket connection in a network game. <br>
 * When display notifies a user action happened, sends appropriate message to server. <br>
 * When server sends data back, updates the board accordingly.
 * 
 * @author Bohui Moon
 */
public class ClientCommunicator implements BoardDisplay.Delegate, ChatWidget.Delegate, SocketWrapper.MessageListener
{
	
	/* IVARS */
	private Color color;
	private SocketWrapper socket;
	
	private ChatWidget chat;
	private BoardDisplay display;
	private ScoreManager scoreManager;
	private SoundManager soundManager;
	
	private boolean finished;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a BoardDisplayManager that 
	 * 
	 * @param display
	 */
	public ClientCommunicator(Color c, SocketWrapper sw, BoardDisplay bd, ChatWidget cw, ScoreManager sm)
	{
		color = c;
		
		socket = sw;
		socket.addMessageListener(this);
		socket.setName(Util.colorName(c) + " client");
		
		display = bd;
		display.setDelegate(this);
		
		chat = cw;
		chat.setDelegate(this);
		
		scoreManager = sm;
		soundManager = SoundManager.sharedManager();
	}
	
	
	/**
	 * 
	 * @throws InterruptedException
	 */
	public void process() throws InterruptedException
	{
		socket.sendMessage(Protocol.BOARD_READY);
		
		finished = false;
		while (!finished)
			Thread.sleep(1);
	}
	
	/**
	 * Cleanup any necessary data
	 */
	public void cleanup()
	{
		socket.removeMessageListener(this);
		
		try
		{
			socket.getSocket().close();
		}
		catch (IOException e)
		{
			System.out.println("[Error ClientCommunicator.cleanup()] could not close socket");
		}
	}
	
	
	/* ======== DELEGATES ======== */
	/**
	 * Called when user clicks on the Card butotn on BoardDisplay
	 */
	@Override
	public void cardClicked()
	{
		socket.sendMessage(Protocol.CARD_CLICK);
	}

	/**
	 * Called when user hovers from the given BoardLocation on BoardDisplay.
	 */
	@Override
	public void hover(BoardLocation loc)
	{
		socket.sendMessage(Protocol.HOVER, loc);
	}

	/**
	 * Called when user unhovers from the given BoardLocation on BoardDisplay.
	 */
	@Override
	public void unhover(BoardLocation loc)
	{
		socket.sendMessage(Protocol.UNHOVER, loc);
	}

	/**
	 * Called when user left clicks on the given BoardLocation on BoardDisplay.
	 */
	@Override
	public void leftClick(BoardLocation loc)
	{
		socket.sendMessage(Protocol.CLICK_LEFT, loc);
	}

	/**
	 * Called when user right clicks on the given BoardDisplay on BoardDisplay.
	 */
	@Override
	public void rightClick(BoardLocation loc)
	{
		socket.sendMessage(Protocol.CLICK_RIGHT, loc);
	}

	/**
	 * Called when user types the given chat message.
	 * 
	 * @param cw
	 * @param chat
	 */
	@Override
	public void didTypeChat(ChatWidget cw, String chat)
	{
		socket.sendMessage(Protocol.CHAT, new Object[]{color, chat});
	}
	
	/* ======== MESSAGE LISTENER ======== */
	/**
	 * 
	 * @param sw
	 * @param message
	 */
	@SuppressWarnings("unchecked")
	public void didReceiveMessage(SocketWrapper sw, Message message)
	{
		String head = message.head();
		Object body = message.body();
		
		if (head.equals(Protocol.GAME_OVER))
		{
			finished = true;
		}
		else if (head.equals(Protocol.NAME_REQUEST))
		{
			System.out.println("name req received");
			Object[] params = (Object[]) body;
			boolean won = (boolean) params[0];
			String title = (String) params[1];
			String after = (String) params[2];
			
			SoundManager.sharedManager().play(won ? Constants.SOUND_VICTORY : Constants.SOUND_DEFEAT);
			
			String name = display.askName(title, after);
			if (name == null)
				return;
			
			int score = (int) params[3];
			socket.sendMessage(Protocol.NAME_RESPONSE, new Object[]{name, score});
		}
		else if (head.equals(Protocol.NAME_RESPONSE))
		{
			Object[] params = (Object[]) body;
			String name = (String) params[0];
			int score = (int) params[1];
			
			scoreManager.addScore(name, score);
		}
		else if (head.equals(Protocol.CHAT))
		{
			Object[] params = (Object[]) body;
			chat.addChat((Color) params[0], (String) params[1]);
		}
		else if (head.equals(Protocol.DISCONNECT))
		{
			Color color = (Color) body; 
			chat.addNote(color, Util.colorName(color) + " has left the game");
		}
		else if (head.equals(Protocol.BOARD_PAWNS))
		{
			display.update((List<Pawn>) body);
		}
		else if (head.equals(Protocol.NEXT_TURN))
		{
			display.startTimer((int) body);
		}
		else if (head.equals(Protocol.PLAYER_TURN))
		{
			soundManager.play(Constants.SOUND_TURN);
		}
		else if (head.equals(Protocol.CARD_ALERT))
		{
			Object[] params = (Object[]) body;
			soundManager.play(Constants.SOUND_DRAW_CARD);
			display.alertDraw((Color) params[0], Card.createCard((Card.Type) params[1]));
		}
		else if (head.equals(Protocol.HIGHLIGHT_LOC_SEMI))
		{
			display.highlight((BoardLocation) body, Util.changeAlpha(color, 0.3f));
		}
		else if (head.equals(Protocol.HIGHLIGHT_LOC_FULL))
		{
			display.highlight((BoardLocation) body, Util.changeAlpha(color, 0.5f));
		}
		else if (head.equals(Protocol.HIGHLIGHT_PATH_SEMI))
		{
			display.highlight((List<BoardLocation>) body, Util.changeAlpha(color, 0.3f));
		}
		else if (head.equals(Protocol.HIGHLIGHT_PATH_FULL))
		{
			display.highlight((List<BoardLocation>) body, Util.changeAlpha(color, 0.5f));
		}
		else if (head.equals(Protocol.HIGHLIGHT_CLEAR))
		{
			display.unhighlightAll();
		}
		else if (head.equals(Protocol.HIGHLIGHT_CLEAR_LOC))
		{
			display.unhighlight((BoardLocation) body);
		}
		else if (head.equals(Protocol.HIGHLIGHT_CLEAR_PATH))
		{
			display.unhighlight((List<BoardLocation>) body);
		}
	}
}
