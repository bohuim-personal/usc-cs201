package client;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;

/**
 * Constants define useful public static final values.
 * 
 * @author Bohui Moon
 */
public abstract class Constants
{	
	private static Toolkit toolkit = Toolkit.getDefaultToolkit();
	private static final String IMAGES_PATH = "resources/images/";
	private static final String SOUNDS_PATH = "resources/sounds/";
	
	
	public static final String SCORES_FILENAME = "score.txt";
	
	
	//Fonts
	public static final String FONT_NAME 		= "KenVector Future";
	public static final String FONT_THIN_NAME 	= "KenVector Future Thin";
	public static final String FONT_PATH		= "resources/fonts/kenvector_future.ttf";
	public static final String FONT_THIN_PATH	= "resources/fonts/kenvector_future_thin.ttf";
	
	public static final Font MESSAGE_FONT = new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 30);
	public static Font messageFont(int style, int size)
	{
		return new Font(Constants.FONT_THIN_NAME, style, size);
	}
	
	
	//Logo
	public static final String LOGO_PATH = "resources/images/sorry.png";
	public static final Image  LOGO = new ImageIcon(LOGO_PATH).getImage();
	
	//Cursors
	private static final String CURSOR_PATH 			= IMAGES_PATH + "cursors/";
	public  static final String CURSOR_HAND_GRAY_PATH 	= CURSOR_PATH + "cursor_hand_gray.png";
	public  static final String CURSOR_HAND_BEIGE_PATH 	= CURSOR_PATH + "cursor_hand_beige.png";
	public  static final String CURSOR_HAND_WHITE_PATH 	= CURSOR_PATH + "cursor_hand_white.png";
	public  static final Cursor CURSOR_HAND_GRAY 		= Toolkit.getDefaultToolkit().createCustomCursor(toolkit.getImage(CURSOR_HAND_GRAY_PATH), new Point(0,0), "Gray Hand Cursor");
	public  static final Cursor CURSOR_HAND_BEIGE 		= Toolkit.getDefaultToolkit().createCustomCursor(toolkit.getImage(CURSOR_HAND_BEIGE_PATH), new Point(0,0), "Beige Hand Cursor");
	public  static final Cursor CURSOR_HAND_WHITE 		= Toolkit.getDefaultToolkit().createCustomCursor(toolkit.getImage(CURSOR_HAND_WHITE_PATH), new Point(0,0), "White Hand Cursor");
	
	//Buttons
	private static final String BUTTON_PATH 			= IMAGES_PATH + "buttons/";
	public  static final String BUTTON_GRAY_DEFAULT 	= BUTTON_PATH + "button_gray_default.png";
	public  static final String BUTTON_GRAY_PRESSED 	= BUTTON_PATH + "button_gray_pressed.png";
	public  static final String BUTTON_RED_DEFAULT 		= BUTTON_PATH + "button_red_default.png";
	public  static final String BUTTON_RED_PRESSED 		= BUTTON_PATH + "button_red_pressed.png";
	public  static final String BUTTON_BLUE_DEFAULT 	= BUTTON_PATH + "button_blue_default.png";
	public  static final String BUTTON_BLUE_PRESSED 	= BUTTON_PATH + "button_blue_pressed.png";
	public  static final String BUTTON_YELLOW_DEFAULT 	= BUTTON_PATH + "button_yellow_default.png";
	public  static final String BUTTON_YELLOW_PRESSED 	= BUTTON_PATH + "button_yellow_pressed.png";
	public  static final String BUTTON_GREEN_DEFAULT 	= BUTTON_PATH + "button_green_default.png";
	public  static final String BUTTON_GREEN_PRESSED 	= BUTTON_PATH + "button_green_pressed.png";
	public  static final Image[] buttonsForIndex(int index)
	{
		Image[] images = new Image[2];
		String color = "gray";
		if (index == 0) color = "red";
		if (index == 1) color = "blue";
		if (index == 2) color = "yellow";
		if (index == 3) color = "green";
		
		images[0] = new ImageIcon(BUTTON_PATH + "button_" + color + "_default.png").getImage();
		images[1] = new ImageIcon(BUTTON_PATH + "button_" + color + "_pressed.png").getImage();
		return images;
	}
	
	//Cards
	private static final String CARD_PATH 	   = IMAGES_PATH + "cards/";
	public  static final String CARD_BEIGE 		 = CARD_PATH + "card_beige.png";
	public  static final String CARD_BEIGE_LIGHT = CARD_PATH + "card_beige_light.png";
	public  static final String CARD_BROWN 		 = CARD_PATH + "card_brown.png";
	public  static final String CARD_GRAY 		 = CARD_PATH + "card_gray.png";
	public  static final String CARDBACK_RED 	 = CARD_PATH + "cardback_red.png";
	
	//Checkbox
	private static final String CHECKBOX_PATH 		  =   IMAGES_PATH + "checkboxes/";
	public  static final String CHECK_BOX_DEFAULT 	  = CHECKBOX_PATH + "check_box_default.png";
	public  static final String CHECK_CIRCLE_DEFAULT  = CHECKBOX_PATH + "check_circle_default.png";
	public  static final String CHECK_CIRCLE_SELECTED = CHECKBOX_PATH + "check_circle_selected.png";
	
	//Panels
	private static final String PANEL_PATH  = IMAGES_PATH + "panels/";
	public  static final String PANEL_GRAY 	 = PANEL_PATH + "panel_gray.png";
	public  static final String PANEL_RED 	 = PANEL_PATH + "panel_red.png";
	public  static final String PANEL_BLUE 	 = PANEL_PATH + "panel_blue.png";
	public  static final String PANEL_YELLOW = PANEL_PATH + "panel_yellow.png";
	public  static final String PANEL_GREEN  = PANEL_PATH + "panel_green.png";
	public  static String panelPath(String colorName)
	{
		return PANEL_PATH + "panel_" + colorName + ".png";
	}
	
	//Pawns
	private static final String PAWN_PATH = IMAGES_PATH + "pawns/";
	public  static final String PAWN_RED 	= PAWN_PATH + "pawn_red.png";
	public  static final String PAWN_BLUE 	= PAWN_PATH + "pawn_blue.png";
	public  static final String PAWN_YELLOW = PAWN_PATH + "pawn_yellow.png";
	public  static final String PAWN_GREEN 	= PAWN_PATH + "pawn_green.png";
	public  static final Image[] PAWN_IMAGES = {
		new ImageIcon(PAWN_RED).getImage(),
		new ImageIcon(PAWN_BLUE).getImage(),
		new ImageIcon(PAWN_YELLOW).getImage(),
		new ImageIcon(PAWN_GREEN).getImage(),
	};
	
	//Slides
	private static final String SLIDE_PATH  = IMAGES_PATH + "slides/";
	public  static final String SLIDE_RED 	 = SLIDE_PATH + "slide_red.png";
	public  static final String SLIDE_BLUE 	 = SLIDE_PATH + "slide_blue.png";
	public  static final String SLIDE_YELLOW = SLIDE_PATH + "slide_yellow.png";
	public  static final String SLIDE_GREEN  = SLIDE_PATH + "slide_green.png";
	public  static String slidePath(String colorName)
	{
		return SLIDE_PATH + "slide_" + colorName + ".png";
	}
	
	//Tiles
	private static final String TILE_PATH = IMAGES_PATH + "tiles/";
	public  static final String TILE_GRAY 	= TILE_PATH + "tile_gray.png";
	public  static final String TILE_RED 	= TILE_PATH + "tile_red.png";
	public  static final String TILE_BLUE 	= TILE_PATH + "tile_blue.png";
	public  static final String TILE_YELLOW = TILE_PATH + "tile_yellow.png";
	public  static final String TILE_GREEN 	= TILE_PATH + "tile_green.png";
	public  static String tilePath(String colorName)
	{
		return TILE_PATH + "tile_" + colorName + ".png";
	}
	
	//Sounds
	public static final Clip SOUND_DRAW_CARD = makeClip(SOUNDS_PATH + "draw_card.wav");
	public static final Clip SOUND_VICTORY = makeClip(SOUNDS_PATH + "victory.wav");
	public static final Clip SOUND_DEFEAT = makeClip(SOUNDS_PATH + "defeat.wav");
	public static final Clip SOUND_TURN = makeClip(SOUNDS_PATH + "turn.wav");
	
	/**
	 * Create a Clip from the specified filepath name
	 * 
	 * @param filename - String path to sound file
	 * @return sound Clip created
	 */
	public static Clip makeClip(String filename)
	{
		Clip clip = null;
		try
		{
			File f = new File(filename);
			AudioInputStream ais = AudioSystem.getAudioInputStream(f);
			clip = AudioSystem.getClip();
		    clip.open(ais);
		    return clip;
		}
		catch (IOException | UnsupportedAudioFileException| LineUnavailableException e)
		{
			System.out.println("[Error@Constants.makeClip()] " + e.getMessage());
		}
	    return null;
	}
	
	
	/**
	 * Convert the given seconds into a string with leading 0 if less than 10. <br>
	 * Anything not within [0, 60) will be returned as "00"
	 * 
	 * @param seconds
	 * @return
	 */
	public static String secondsToString(int seconds)
	{
		if (seconds < 0 || 60 <= seconds)
			return "00";
		
		if (seconds < 10)
			return "0" + seconds;
		
		return "" + seconds;
	}
	
	/**
	 * Return a pre-formatted ConfirmButton for easier use.
	 * 
	 * @return pre-formatted ConfirmButton
	 */
	public static JButton getConfirmButton()
	{
		JButton confirmButton = new JButton("Confirm");
		
		//looks
		confirmButton.setPreferredSize(new Dimension(120, 40));
		confirmButton.setBorder(BorderFactory.createLineBorder(Color.GRAY));
		confirmButton.setFont(new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 18));
		//icons
		confirmButton.setHorizontalTextPosition(SwingConstants.CENTER);
		confirmButton.setIcon(new ImageIcon(Constants.BUTTON_GRAY_DEFAULT));
		confirmButton.setPressedIcon(new ImageIcon(Constants.BUTTON_GRAY_PRESSED));
		confirmButton.setDisabledIcon(new ImageIcon(Constants.BUTTON_GRAY_DEFAULT));
		
		return confirmButton;
	}
}
