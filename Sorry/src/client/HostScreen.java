package client;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import extension.GrayButton;
import extension.ImagePanel;

/**
 * HostScreen allows the user to start a game by hosting.
 * User etners the port they want to host on through a textfield.
 * 
 * @author Bohui Moon
 */
public class HostScreen extends ImagePanel implements ActionListener
{
	private static final long serialVersionUID = 2382984239842L;
	
	
	/* IVARS */
	private boolean waiting = false;
	private int port = 8888;
	
	private JTextField field;
	

	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a new host screen with the default port number already entered into the field.
	 * Specify negative port to leave the field blank on initialization.
	 * 
	 * @param defaultPort - 
	 */
	public HostScreen(int defaultPort)
	{
		super( new ImageIcon(Constants.PANEL_GRAY).getImage() );
		
		String start = (defaultPort < 0) ? "" : Integer.toString(defaultPort);
		Font font = new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 30);
		
		//host bar
		JPanel hostbar = new JPanel();
		hostbar.setLayout(new BoxLayout(hostbar, BoxLayout.X_AXIS));
		hostbar.setMaximumSize(new Dimension(400, 50));		
			JLabel label = new JLabel("Port:");
			label.setFont(font);
			
			field = new JTextField(start);
			field.setFont(font);
			field.setMaximumSize(new Dimension(150, 50));
			field.setPreferredSize(new Dimension(150, 50));
			field.addActionListener(this);
			
		hostbar.add(Box.createHorizontalGlue());
		hostbar.add(label);
		hostbar.add(field);
		hostbar.add(Box.createHorizontalGlue());
		
		
		//button
		Dimension buttonSize = new Dimension(120, 40);
		JButton button = new GrayButton("Start", 18);
		button.setMaximumSize(buttonSize);
		button.setPreferredSize(buttonSize);
		button.addActionListener(this);
		button.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		add(Box.createVerticalGlue());
		add(hostbar);
		add(Box.createVerticalGlue());
		add(button);
		add(Box.createVerticalGlue());
	}
	
	/**
	 * Returns a valid port number the user entered.
	 * 
	 * @return valid port number the user entered.
	 * @throws InterruptedException
	 */
	public int getPort() throws InterruptedException
	{
		waiting = true;
		while(waiting)
			Thread.sleep(1);
		
		return port;
	}
	
	/**
	 * Called when user enter on port field, or presses the start button.
	 * Check that port is valid, and if so set waiting to false.
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		try
		{
			waiting = false;
			port = Integer.parseInt(field.getText());
		}
		catch (NumberFormatException nfe)
		{
			waiting = true;
		}
	}
}
