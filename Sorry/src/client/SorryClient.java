package client;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.WindowConstants;

import network.SocketWrapper;
import score.ScoreManager;
import server.SorryServer;


/**
 * SorryClient runs Sorry game for one player.
 * After going through title, player, and color selection, starts the game.
 * 
 * @author Bohui Moon
 */
public class SorryClient implements Runnable, SocketWrapper.DisconnectListener
{
	private static final Dimension MIN_SIZE = new Dimension(640, 480);
	private static final Dimension MAX_SIZE = new Dimension(960, 640);
	
	private static final String DATA_PATH = "data/scores.txt";
	private static final String IP = "127.0.0.1";
	private static final int PORT = 8888;
	
	
	/* IVARS */
	private JFrame frame;
	private JMenuBar menubar;
	private HelpFrame helpFrame;
	private ScoreFrame scoreFrame;
	
	private TitleScreen titleScreen;
	
	private HostScreen hostScreen;
	private JoinScreen joinScreen;
	
	private CountScreen countScreen;
	private ColorScreen colorScreen;
	
	private Thread myThread;
	private SocketWrapper connection;
	private ScoreManager scoreManager;
	
	private ChatWidget widget;
	private BoardDisplay display;
	private GameScreen gameScreen;
	
	
	
	/* ======== CONSTRUCTOR & SETUP ======== */
	/**
	 * Initialize frame and needed subpanels.
	 */
	public SorryClient() throws IOException
	{
		frame = new JFrame("Sorry!");
		frame.setSize(MIN_SIZE);
		frame.setMinimumSize(MIN_SIZE);
		frame.setMaximumSize(MAX_SIZE);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		
		setupFonts();
		setupHelpers();
		setupMenuBar();
		frame.setVisible(true);
	}	
	
	/**
	 * Setup global fonts by registering them to the local graphics environment.
	 */
	private void setupFonts()
	{
		try
		{
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File( Constants.FONT_PATH )));
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File( Constants.FONT_THIN_PATH )));
			
			frame.setCursor(Constants.CURSOR_HAND_GRAY);
		}
		catch (IOException|FontFormatException e)
		{
			//error handling
			e.printStackTrace();
		}
	}
	
	/**
	 * Initialize necessary panel screens.
	 */
	private void setupHelpers() throws IOException
	{
		scoreManager = new ScoreManager(DATA_PATH);
		
		helpFrame = new HelpFrame();
		scoreFrame = new ScoreFrame(scoreManager);
		
		titleScreen = new TitleScreen();
		
		hostScreen = new HostScreen(PORT);
		joinScreen = new JoinScreen(IP, PORT);
		
		countScreen = new CountScreen();
		colorScreen = new ColorScreen();
		
		widget = new ChatWidget();
		display = new BoardDisplay();
		gameScreen = new GameScreen(display, widget, 160);
	}
	
	/**
	 * Sets up the menu bar and implements necessary event handlers.
	 */
	private void setupMenuBar()
	{
		menubar = new JMenuBar();
		
		JMenuItem helpItem = new JMenuItem("Help");
		helpItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_H, ActionEvent.CTRL_MASK));
		helpItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				helpFrame.setVisible(true);
				helpFrame.setLocationRelativeTo(frame);
			}
		});
		
		JMenuItem scoreItem = new JMenuItem("Top Scores");
		scoreItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		scoreItem.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				scoreFrame.update();
				scoreFrame.setVisible(true);
				scoreFrame.setLocationRelativeTo(frame);
			}
		});
		
		
		menubar.add(helpItem);
		menubar.add(scoreItem);
		frame.setJMenuBar(menubar);
	}
	
	
	/* ======== HELPERS ======== */
	/**
	 * Remove all previous components and add the given JPanel.
	 * Repaint and revalidate for frame to update
	 * 
	 * @param p
	 */
	private void show(JPanel p)
	{
		Dimension size = frame.getSize();
		if (p == gameScreen)
			frame.setSize(size.width, size.height + 160);
		else if (gameScreen.getParent() == frame.getContentPane())
			frame.setSize(size.width, size.height - 160);
		
		frame.getContentPane().removeAll();
		frame.getContentPane().add(p);
		frame.repaint();
		frame.revalidate();
	}
	
	/**
	 * Called when server shuts down.
	 * 
	 * @param sw
	 */
	@Override
	public void didDisconnect(SocketWrapper sw)
	{
		launch();
	}
	
	/**
	 * Launch the client to get preference settings from user and start game.
	 * Repeat process until user closes game to quit.
	 */
	public void launch()
	{
		if (myThread != null)
			myThread.interrupt();
		
		myThread = new Thread(this);
		myThread.start();
	}
	
	/**
	 * 
	 */
	@Override
	public void run()
	{
		try
		{
			show(titleScreen);
			int type = titleScreen.start();
			
			int port;
			String ip = IP;
			if (type == TitleScreen.SELECTION_HOST)
			{
				show(hostScreen);
				port = hostScreen.getPort();
				
				show(countScreen);
				int count = countScreen.getCount();
				
				if (port == 8080)
					throw new IOException("Cannot host game on ScoreServer port");
				
				new SorryServer(port, count);
			}
			else
			{
				show(joinScreen);
				joinScreen.setConnection();
				
				ip = joinScreen.getIP();
				port = joinScreen.getPort();
			}
			
			//connect to server
			connection = new SocketWrapper( new Socket(ip, port) );
			connection.setDisconnectListener(this);
			
			//color select
			colorScreen.reset();
			show(colorScreen);
			colorScreen.setSocketWrapper(connection);
			colorScreen.process();
			
			//resize and show
			show(gameScreen);
			
			//game
			ClientCommunicator communicator = new ClientCommunicator(colorScreen.getColor(), connection, display, widget, scoreManager);
			communicator.process();
			communicator.cleanup();
			
			scoreManager.save();
		}
		catch (IOException ioe)
		{
			launch();
			System.out.println("[Error SorryClient.run()] socket failure: " + ioe.getMessage());
		}
		catch (InterruptedException ie)
		{
			System.out.println("Client interrupted");
			try { connection.getSocket().close(); }
			catch (IOException e)
			{
				System.out.println("[Error SorryClient.run()] failed to close socket");
			}
		}
	}
	
	
	
	/* ========= MAIN ========= */
	public static void main(String[] args)
	{
		try
		{
			SorryClient client = new SorryClient();
			client.launch();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
