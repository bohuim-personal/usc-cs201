package client;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

/**
 * GameScreen is a combination of BoardDisplay and ChatWidget.
 * The Chat is given a fixed height from the bottom of the panel,
 * and BoardDisplay is filled to the rest of the screen.
 * 
 * @author Bohui Moon
 */
public class GameScreen extends JPanel
{
	private static final long serialVersionUID = -8854196927947667881L;

	
	/* ========= CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public GameScreen(BoardDisplay display, ChatWidget widget, int chatHeight)
	{
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		add(display);
		layout.putConstraint(SpringLayout.WEST, display, 0, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, display, 0, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.NORTH, display, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.SOUTH, display, 0, SpringLayout.NORTH, widget);
		
		add(widget);
		layout.putConstraint(SpringLayout.WEST, widget, 0, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, widget, 0, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, widget, 0, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.NORTH, widget, -chatHeight, SpringLayout.SOUTH, this);
	}
}
