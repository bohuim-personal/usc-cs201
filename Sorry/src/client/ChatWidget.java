package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SpringLayout;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import extension.GrayButton;
import game.Util;


/**
 * 
 * 
 * @author Bohui Moon
 */
public class ChatWidget extends JPanel implements ActionListener
{
	private static final long serialVersionUID = -7205404799146507082L;

	
	/**
	 * ChatWidget.Delegate should be implemented by any class that
	 * wants to respond to chats typed by the user.
	 */
	public interface Delegate
	{
		public void didTypeChat(ChatWidget cw, String chat);
	}
	
	
	/* IVARS */
	private JTextPane textPane;
	private JScrollPane scrollPane;
	
	private JTextField field;
	private JButton sendButton;
	
	private Delegate delegate;
	
	
	/* ========= CONSTRUCTOR ========= */
	/**
	 * 
	 */
	public ChatWidget()
	{
		//text pane
		textPane = new JTextPane();
			textPane.setFont(new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 20));
			textPane.setAlignmentY(BOTTOM_ALIGNMENT);
			textPane.setBackground(Color.BLACK);
		
		scrollPane = new JScrollPane(textPane);
		scrollPane.setVerticalScrollBar(scrollPane.createVerticalScrollBar());
		
		
		//field
		field = new JTextField();
		field.addActionListener(this);
			field.setFont(new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 20));
			field.setBackground(Color.BLACK);
			field.setForeground(Color.WHITE);
			field.setCaretColor(Color.WHITE);
		
		sendButton = new GrayButton("Send", 18);
		sendButton.addActionListener(this);
		
		
		//layout
		SpringLayout layout = new SpringLayout();
		setLayout(layout);
		
		add(scrollPane);
		layout.putConstraint(SpringLayout.WEST, scrollPane, 0, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, scrollPane, 0, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.NORTH, scrollPane, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.SOUTH, scrollPane, 0, SpringLayout.NORTH, field);
		
		add(field);
		layout.putConstraint(SpringLayout.WEST, field, 0, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.SOUTH, field, 0, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.NORTH, field, -40, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.EAST, field, 0, SpringLayout.WEST, sendButton);
		
		add(sendButton);
		layout.putConstraint(SpringLayout.EAST, sendButton, 0, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, sendButton, 0, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.WEST, sendButton, -120, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.NORTH, sendButton, -40, SpringLayout.SOUTH, this);
	}
	
	
	/**
	 * Set the Delegate to respond to chats as the given one.
	 * 
	 * @param delegate - Delegate to start receiving chats
	 */
	public void setDelegate(Delegate delegate)
	{
		this.delegate = delegate;
	}
	
	/**
	 * Add the given chat message to the chat box, with the specified color as the sender
	 * 
	 * @param color - Color of sender
	 * @param chat  - String message of chat
	 */
	public void addChat(Color color, String chat)
	{
		String name = Util.colorName(color).toUpperCase();
		appendToPane(name + ": ", color);
		appendToPane(chat + "\n", Color.WHITE);
	}
	
	/**
	 * Add the given note to the chat box with the specified color.
	 * 
	 * @param color - Color of note message
	 * @param note  - String note to display
	 */
	public void addNote(Color color, String note)
	{
		appendToPane(note + "\n", color);
	}
	
	/**
	 * Clear the current chats
	 */
	public void clear()
	{
		textPane.setText("");
	}


	/**
	 * 
	 * 
	 * @param e
	 */
	@Override
	public void actionPerformed(ActionEvent e)
	{
		String text = field.getText();
		field.setText("");
		
		if (!text.isEmpty() && delegate != null)
			delegate.didTypeChat(this, text);
	}
	
	
	/* ======== PRIVATE HELPERS ========= */
	/**
	 * Append the given msg to the text pane with the given color.
	 * 
	 * @note http://stackoverflow.com/questions/9650992/how-to-change-text-color-in-the-jtextarea
	 * 
	 * @param tp
	 * @param msg
	 * @param c
	 */
	private void appendToPane(String msg, Color c)
    {
        StyleContext sc = StyleContext.getDefaultStyleContext();
        AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, c);

//        aset = sc.addAttribute(aset, StyleConstants.FontFamily, Constants.FONT_THIN_NAME);
        aset = sc.addAttribute(aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED);

        int len = textPane.getDocument().getLength();
        textPane.setCaretPosition(len);
        textPane.setCharacterAttributes(aset, false);
        textPane.replaceSelection(msg);
    }
}
