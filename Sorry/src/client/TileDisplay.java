package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

import extension.ImagePanel;
import game.BoardLocation;
import game.Util;


/**
 * TileDisplay shows the GUI for a single tile object.
 * 
 * @author Bohui Moon
 */
public class TileDisplay extends ImagePanel implements MouseListener
{
	private static final long serialVersionUID = 19273948128102312L;
	
	
	/* IVARS */
	private boolean mutable;
	
	private BoardDisplay display;
	private BoardLocation location;
	
	private Font defaultFont;
	private String defaultText;
	
	private Image defaultImage;
	private Image currentImage;
	
	private Color highlight;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * Create a new TileDisplay under the given BoardDisplay, at the given BoardLocation.
	 * 
	 * @param display - parent display container
	 * @param loc - abstract board location of this tile
	 */
	public TileDisplay(BoardDisplay display, BoardLocation loc)
	{
		this.display = display;
		this.location = loc;
		
		Color color = loc.getColor();
		String colorName = Util.colorName(color);
		
		mutable = !( loc.isStart() || loc.isHome() );
		
		//background image
		String bgImage = Constants.TILE_GRAY;
		if (loc.isSlide() || loc.isSafety())
			bgImage = Constants.tilePath(colorName);
		else if (loc.isStart() || loc.isHome())
			bgImage = Constants.panelPath(colorName);
		this.setImage(new ImageIcon(bgImage).getImage());
		
		
		//icon or label
		defaultImage = !loc.isSlide() ? null : new ImageIcon( Constants.slidePath(colorName) ).getImage();
		
		defaultFont = new Font(Constants.FONT_THIN_NAME, Font.PLAIN, 12);
		defaultText = null;
		if (loc.isHome()) defaultText = "Home";
		if (loc.isStart()) defaultText = "Start";
		
		
		//dummy button
		JButton button = new JButton();
		button.setOpaque(false);
		button.setBorderPainted(false);
		button.setContentAreaFilled(false);
		button.addMouseListener(this);
		this.add(button);
	}

	
	/* ======== ACCESSORS ======== */
	/**
	 * Sets the current image to the given one. <br>
	 * If current image is set, paint prioritizes it over the default image,
	 * meaning passing in an null image will cause the default image to be drawn.
	 * @note some TileDisplay are immutable (home & start), and will not be changed.
	 * 
	 * @param image - Image to draw
	 */
	public void setIcon(Image image)
	{
		if (!mutable)
			return;
		
		currentImage = image;
		repaint();
	}
	
	/**
	 * Set the highlight color to the given color, or null to un-highlight.
	 * 
	 * @param color - Color to highlight this TileDisplay with
	 */
	public void setHighlight(Color color)
	{
		highlight = color;
		repaint();
	}
	
	

	/* ======== EVENTS ======== */
	/**
	 * Called when user clicks a mouse button on this TileDisplay. <br>
	 * Relay message to BoardDisplay depending on type of click.
	 */
	@Override
	public void mouseClicked(MouseEvent e)
	{
		if (e.getButton() == MouseEvent.BUTTON1)
			display.leftClick(location);
		else if (e.getButton() == MouseEvent.BUTTON3)
			display.rightClickAt(location);
	}
	
	/**
	 * 
	 */
	@Override
	public void mousePressed(MouseEvent e)
	{
		
	}


	@Override
	public void mouseReleased(MouseEvent e)
	{
		
	}
	
	@Override
	public void mouseEntered(MouseEvent e)
	{
		display.hover(location);
	}


	@Override
	public void mouseExited(MouseEvent e)
	{
		display.unhover(location);
	}
	
	
	
	/* ======== OVERRIDE ======== */
	/**
	 * Draw whatever image or text that is current set on this TileDisplay.
	 * 
	 * @param g - Graphics to draw on
	 */
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		int w = this.getWidth();
		int h = this.getHeight();
		
		//icon
		int zone = Util.indexFromColor(location.getColor());
		Image img = (currentImage != null) ? currentImage : defaultImage;
		
		if (img == defaultImage)
		{
			if (zone == 0 || zone == 2)
				g.drawImage(img, w/2 - w/6 - 1, h/2 - h/6 - 1, w/3, h/3, null);
			else
				g.drawImage(img, w/2 - h/6 - 1, h/2 - w/6 - 1, h/3, w/3, null);
		}
		else
		{
			if (zone == 0 || zone == 2)
				g.drawImage(img, w/2 - w/10 - 1, h/2 - h/4 - 1, w/5, h/2, null);
			else
				g.drawImage(img, w/2 - h/6 - 1, h/2 - w/6 - 1, h/3, w/3, null);
		}
		
		
		//text
		if (defaultText != null)
		{
			g.setFont(defaultFont);
			FontMetrics fm = g.getFontMetrics();
			
			int strw = fm.stringWidth(defaultText);
			int strh = fm.getHeight();
			g.drawString(defaultText, w/2 - strw/2 + 1, h/2 + strh/3);
		}
		
		
		//highlight
		if (highlight != null)
		{
			g.setColor(highlight);
			g.fillRect(0, 0, w, h);
		}
	}
}
