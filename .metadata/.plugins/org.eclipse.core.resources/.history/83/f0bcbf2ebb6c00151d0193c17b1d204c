package client;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.WindowConstants;


/**
 * SorryClient runs Sorry game for one player.
 * After going through title, player, and color selection, starts the game.
 * 
 * @author Bohui Moon
 */
public class SorryClient
{
	private final Dimension MIN_SIZE = new Dimension(640, 480);
	private final Dimension MAX_SIZE = new Dimension(960, 640);
	
	
	/* IVARS */
	private JFrame frame;
	private JPanel titleScreen;
//	private JPanel countSelect;
//	private JPanel colorSelect;
	
	private boolean waiting;
	
	
	/* ======== CONSTRUCTOR & SETUP ======== */
	/**
	 * Initialize frame and needed subpanels.
	 */
	public SorryClient()
	{
		frame = new JFrame("Sorry!");
		frame.setSize(MIN_SIZE);
		frame.setMinimumSize(MIN_SIZE);
		frame.setMaximumSize(MAX_SIZE);
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		
		setupFonts();
		setupScreens();
	}	
	
	/**
	 * Setup global fonts by registering them to the local graphics environment.
	 */
	private void setupFonts()
	{
		try
		{
			GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
			 ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File( Constants.FONT_PATH )));
			 ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File( Constants.FONT_THIN_PATH )));
		}
		catch (IOException|FontFormatException e)
		{
			//error handling
			e.printStackTrace();
		}
	}
	
	/**
	 * Initialize necessary panel screens.
	 */
	private void setupScreens()
	{
		titleScreen = new TitleScreen(this);
	}
	
	
	/* ======== PUBLIC METHODS ========= */
	/**
	 * Launch the client to get preference settings from usre and start game.
	 * Repeat process until user closes game to quit.
	 */
	public void launch()
	{
		while(true)
		{
			show(titleScreen);
			waiting();
			
			System.out.println("Hi");
		}
	}
	
	/**
	 * Called when user presses Start on the title screen.
	 */
	public void start()
	{
		waiting = false;
	}
	
	
	/* ======== HELPERS ======== */
	/**
	 * Remove all previous components and add the given JPanel.
	 * Repaint and revalidate for frame to update
	 * 
	 * @param p
	 */
	private void show(JPanel p)
	{
		frame.getContentPane().removeAll();
		frame.getContentPane().add(p);
		frame.repaint();
		frame.revalidate();
	}
	
	/**
	 * Set waiting to true and sleep(1) until some event fires.
	 */
	private void waiting()
	{
		waiting = true;
		try
		{
			while(waiting)
				Thread.sleep(1);
		}
		catch (InterruptedException e)
		{
			//error handling
			e.printStackTrace();
		}
	}
	
	
	/* ========= MAIN ========= */
	public static void main(String[] args)
	{
		SorryClient client = new SorryClient();
		client.launch();
	}
}
