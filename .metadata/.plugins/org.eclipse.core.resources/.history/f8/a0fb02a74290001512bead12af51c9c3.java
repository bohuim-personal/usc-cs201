import java.awt.Image;

public class GameEntity {
	private int x; 
	private int y; 
	private int ID; 
	private boolean damaged;
	private boolean isPlayer;
	private Image setImage;
	
	//if monster
	private Image sprite; 
	private Image damagedSprite;
	
	//if player
	private boolean attacking; 
	private String direction;
	private Image left;
	private Image right;
	private Image leftAttack;
	private Image rightAttack;
	private Image leftDamaged;
	private Image rightDamaged;
	private Image leftAttackDamaged;
	private Image rightAttackDamaged;
	
	GameEntity (String name, int id, int X, int Y) {
		if(name.equals("mage"))
		{
			isPlayer = true;
			damaged = false;
			ID = id;
			x = X;
			y = Y;
			
			attacking = false;
			this.direction = "right";
			left = DungeonImageConstants.mageLeft;
			right = DungeonImageConstants.mageRight;
			leftAttack = DungeonImageConstants.mageLeftAttack;
			rightAttack = DungeonImageConstants.mageRightAttack;
			leftDamaged = DungeonImageConstants.mageLeftDamaged;
			rightDamaged = DungeonImageConstants.mageRightDamaged;
			leftAttackDamaged = DungeonImageConstants.mageLeftAttackDamaged;
			rightAttackDamaged = DungeonImageConstants.mageRightAttackDamaged;
			
			setImage = right;
		}
		else if(name.equals("archer"))
		{
			isPlayer = true;
			damaged = false;
			ID = id;
			x = X;
			y = Y;
			
			attacking = false;
			this.direction = "right";
			left = DungeonImageConstants.archerLeft;
			right = DungeonImageConstants.archerRight;
			leftAttack = DungeonImageConstants.archerLeftAttack;
			rightAttack = DungeonImageConstants.archerRightAttack;
			leftDamaged = DungeonImageConstants.archerLeftDamaged;
			rightDamaged = DungeonImageConstants.archerRightDamaged;
			leftAttackDamaged = DungeonImageConstants.archerLeftAttackDamaged;
			rightAttackDamaged = DungeonImageConstants.archerRightAttackDamaged;
			
			setImage = right;
		}
		else if(name.equals("paladin"))
		{
			isPlayer = true;
			damaged = false;
			ID = id;
			x = X;
			y = Y;
			
			attacking = false;
			this.direction = "right";
			left = DungeonImageConstants.paladinLeft;
			right = DungeonImageConstants.paladinRight;
			leftAttack = DungeonImageConstants.paladinLeftAttack;
			rightAttack = DungeonImageConstants.paladinRightAttack;
			leftDamaged = DungeonImageConstants.paladinLeftDamaged;
			rightDamaged = DungeonImageConstants.paladinRightDamaged;
			leftAttackDamaged = DungeonImageConstants.paladinLeftAttackDamaged;
			rightAttackDamaged = DungeonImageConstants.paladinRightAttackDamaged;
			
			setImage = right;
		}
		else if(name.equals("bat"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.bat;
			damagedSprite = DungeonImageConstants.batDamaged;
			
			setImage = sprite;
		}
		else if(name.equals("dragon"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.dragon;
			damagedSprite = DungeonImageConstants.dragonDamaged;
			
			setImage = sprite;
		}
		else if(name.equals("ghost"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.ghost;
			damagedSprite = DungeonImageConstants.ghostDamaged;
			
			setImage = sprite;
		}
		else if(name.equals("ogre"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.ogre;
			damagedSprite = DungeonImageConstants.ogreDamaged;
			setImage = sprite;
		}
		else if(name.equals("skeleton"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.skeleton;
			damagedSprite = DungeonImageConstants.skeletonDamaged;
			
			setImage = sprite;
			
		}else if(name.equals("slime"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.slime;
			damagedSprite = DungeonImageConstants.slimeDamaged;
			
			setImage = sprite;
			
		}else if(name.equals("treant"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.treant;
			damagedSprite = DungeonImageConstants.treantDamaged;
			
			setImage = sprite;
			
		}else if(name.equals("veigar"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.veigar;
			damagedSprite = DungeonImageConstants.veigarDamaged;
			
			setImage = sprite;
			
		}else if(name.equals("wraith"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.wraith;
			damagedSprite = DungeonImageConstants.wraithDamaged;
			
			setImage = sprite;
			
		}else if(name.equals("zombie"))
		{
			isPlayer = false;
			damaged = false;
			ID = id;
			x =X;
			y=Y;
			
			sprite = DungeonImageConstants.zombie;
			damagedSprite = DungeonImageConstants.zombieDamaged;
			
			setImage = sprite;
		}
	}
	
	
	
	public void didFinishAttacking()
	{
		if(isPlayer)
		{
			attacking = false;
			if(direction.equals("up") || direction.equals("right"))
			{
				if(damaged) this.changeImage(this.rightDamaged);
				else this.changeImage(right);
			}
			else
			{
				if(damaged) this.changeImage(this.leftDamaged);
				else this.changeImage(left);
			}
		}
	}
	public void didStartAttacking()
	{
		if(isPlayer)
		{
			attacking = true;
			if(direction.equals("up") || direction.equals("right"))
			{
				if(damaged) this.changeImage(this.rightAttackDamaged);
				else this.changeImage(rightAttack);
			}
			else
			{
				if(damaged) this.changeImage(this.leftAttackDamaged);
				else this.changeImage(leftAttack);
			}
		}
	}
	public void didFinishTakingDamage()
	{
		damaged = false;
		if(isPlayer)
		{
			if(direction.equals("up") || direction.equals("right"))
			{
				if(this.attacking) changeImage(rightAttack);
				else changeImage(right);
			}
			else
			{
				if(this.attacking) changeImage(leftAttack);
				else changeImage(left);
			}
		}
		else if(!isPlayer)
		{
			changeImage(sprite);
		}
	}
	public void didStartTakingDamage()
	{
		damaged = true;
		if(isPlayer)
		{
			if(direction.equals("up") || direction.equals("right"))
			{
				if(this.attacking) changeImage(rightAttackDamaged);
				else changeImage(rightDamaged);
			}
			else
			{
				if(this.attacking) changeImage(leftAttackDamaged);
				else changeImage(leftDamaged);
			}
		}
		else if(!isPlayer)
		{
			changeImage(damagedSprite);
		}
	}
	public Image getImage()
	{
		return setImage;
	}
	private void changeImage(Image i)
	{
		setImage = i;
	}
	public int getX()
	{
		return x;
	}
	public int getY()
	{
		return y;
	}
	public int getID()
	{
		return ID;
	}
	public String getDirection()
	{
		return direction;
	}
	public Boolean isAttacking()
	{
		return attacking;
	}
	public Boolean isPlayer()
	{
		return isPlayer;
	}
	public Boolean isDamaged()
	{
		return damaged;
	}
	public void updateLocation (int x, int y) {
		if(isPlayer)
		{
			
			if((x>this.x || y>this.y))
			{
				if(x>this.x) direction = "up";
				if(y>this.y) direction = "right";
				if(damaged) this.changeImage(rightDamaged);
				else this.changeImage(right);
			}
			else
			{
				if(x<this.x) direction = "down";
				if(y<this.y) direction = "left";
				if(damaged) this.changeImage(leftDamaged);
				else this.changeImage(left);
			}
		}
		
		this.x = x; 
		this.y = y; 
	}
}
