package client;

import java.awt.Image;
import java.awt.LayoutManager;

import javax.swing.JPanel;


/**
 * BackgroundImagePanel is a subclass of JPanel that is capable of rendering a single background image.
 * 
 * @author Bohui Moon
 */
public class ImagePanel extends JPanel
{
	/* IVARS */
	private Image image;
	
	
	/* ======== CONSTRUCTOR & SETUP ======== */
	/**
	 * Create a new BackgroundImagePanel with double buffer, FlowLayout and no background image.
	 */
	public ImagePanel()
	{
		super();
	}
	
	/**
	 * Create a new BackgroundImagePanel with FLowLayout and given background image.
	 */
	public ImagePanel(Image bgImage)
	{
		super();
		image = bgImage;
	}
	
	/**
	 * Create a new BackgroundImagePanel with given layout manager, and no background image.
	 * 
	 * @param layout - LayoutManager to use
	 */
	public ImagePanel(LayoutManager layout)
	{
		super(layout);
	}
	
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * 
	 * @return
	 */
	public Image getImage()
	{
		return image;
	}
}
