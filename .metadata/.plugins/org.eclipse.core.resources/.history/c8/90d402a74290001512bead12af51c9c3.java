import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.Timer;

public class DungeonPanel extends JPanel implements KeyListener{
	private static final long serialVersionUID = 1L;
	//back layer
	private JPanel chatPanel, inputPanel;
	private DDGamePanel gamePanel;
	private JTextField chatTextField;
	private JTextArea chatTextArea;
	private JButton sendChatButton;
	
	//front layer
	private DungeonTile[][] gameSquares;
	private JLabel healthLabel;
	//private HashMap<Integer, String> playerIconMap;
	//private HashMap<Integer, String> playerDirectionMap;
	private HashMap<Integer,GameEntity> idToEntityMap;
	
	//logic
	DDClientCommunicator ddcc;
	Vector<Integer> attackedx;
	Vector<Integer> attackedy;
	private int totalHealth;
	private int currentHealth;
	
	public DungeonPanel()
	{
		super();
		
		initializeComponents();
		createGUI();
		addEvents();
		testCode();//DELETE FOR THE REAL THING
		setVisible(true);
	}
	
	public void initializeComponents()
	{
		setLayout(new BorderLayout());
		ddcc = new DDClientCommunicator();
		ddcc.dPanel = this; 
		
		this.setFocusable(true);
		this.addKeyListener(this);
		
		//playerDirectionMap = new HashMap<Integer,String>();
		//playerIconMap = new HashMap<Integer,String>();
		idToEntityMap = new HashMap<Integer,GameEntity>();
		gameSquares = new DungeonTile[16][];
		chatPanel = new JPanel();
		gamePanel = new DDGamePanel();
		
		inputPanel = new JPanel();
		chatTextField = new JTextField(75);
		
		chatTextArea = new JTextArea(10,10);
		chatTextArea.setEditable(false);
		
		sendChatButton = new JButton("send");
		healthLabel = new JLabel("1000/1000");//temporary
		
		chatPanel.setLayout(new BorderLayout());
		gamePanel.setLayout(new GridLayout(16,16));
		
		
		inputPanel.setLayout(new BorderLayout());
		
		//initialize game panels
		for(int i=0; i<16; i++)
		{
			gameSquares[i] = new DungeonTile[16];
		}
		for(int i= 0; i<16; i++)
		{
			for(int j=0; j<16; j++)
			{
				DungeonTile temp = new DungeonTile();
				temp.setLayout(new BorderLayout());
				gamePanel.add(temp);
				gameSquares[i][j] = temp;
				
			}
		}
	}
	
	public void createGUI()
	{
		//initial layout
		inputPanel.add(chatTextField,BorderLayout.WEST);
		inputPanel.add(sendChatButton, BorderLayout.EAST);
		chatPanel.add(inputPanel,BorderLayout.SOUTH);
		chatPanel.add(chatTextArea, BorderLayout.NORTH);
		this.add(chatPanel,BorderLayout.SOUTH);
		this.add(gamePanel, BorderLayout.CENTER);
		
		//game Panel
		gameSquares[0][0].add(healthLabel,BorderLayout.CENTER);
	}
	
	public void addEvents()
	{
		sendChatButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae)
			{
				String input = chatTextField.getText();
			}
		});
	}
	
	public void testCode()
	{
		new Thread(ddcc).start();
		//playerIconMap.put(1, "mage");
		//this.playerDirectionMap.put(1, "right");
		GameEntity ge = new GameEntity("mage",1,1,0);
		this.idToEntityMap.put(1, ge);
		GameEntity monster = new GameEntity("skeleton",2,10,10);
		this.idToEntityMap.put(2, monster);
		totalHealth = 1000;
		currentHealth = 1000;
		
		this.gameSquares[1][1].setEntity(ge);
		this.gameSquares[10][10].setEntity(monster);//hard coded for now, fix later
	}
	
	
	public void moveTo(int ID, int xInitial, int yInitial, int xFinal, int yFinal)
	{
		if(!this.idToEntityMap.containsKey(ID)) return;
		
		GameEntity toMove = this.idToEntityMap.get(ID);
		
		this.gameSquares[xInitial][yInitial].clearEntity();
		toMove.updateLocation(xFinal, yFinal);
		this.gameSquares[xFinal][yFinal].setEntity(toMove);
		
		this.repaint();
		this.revalidate();
	}
	
	public void attack(final int ID, int x, int y)
	{
		if(!this.idToEntityMap.containsKey(ID)) return;
		
		final GameEntity attackingEntity = this.idToEntityMap.get(ID);
		attackingEntity.didStartAttacking();
		
		
		Timer t  = new Timer(150, new ActionListener(){
			public void actionPerformed(ActionEvent ae)
			{
				if(gameSquares[x][y].getEntity()!=null) attackingEntity.didFinishAttacking();
				
				((Timer)ae.getSource()).stop();
			}
		});
		t.start();
	}
	
	public void setFloor(int floor)
	{
		if (floor == 1) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground12);
		} else if (floor == 2) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground12);
		} else if (floor == 3) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground34);
		} else if (floor == 4) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground34);
		} else if (floor == 5) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground56);
		} else if (floor == 6) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground56);
		} else if (floor == 7) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground78);
		} else if (floor == 8) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground78);
		} else if (floor == 9) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackground9);
		} else if (floor == 10) {
			gamePanel.setDungeonBackgroundFile(DungeonImageConstants.dungeonBackgroundBoss);
		}
	}
	
	
	public void renderMonster(int ID, int xInitial, int yInitial, int xFinal, int yFinal)
	{
		if(!this.idToEntityMap.containsKey(ID)) return;
		
		GameEntity monsterEntity = this.idToEntityMap.get(ID);
		if(monsterEntity.isPlayer()) return;
		
		this.gameSquares[xFinal][yFinal].setEntity(monsterEntity);
		this.gameSquares[xInitial][yInitial].clearEntity();
		this.repaint();
		this.revalidate();
	}
	
	public void takeDamage(int amount,int ID)
	{
		if(!this.idToEntityMap.containsKey(ID)) return;
		
		final GameEntity damagedEntity = this.idToEntityMap.get(ID);
		damagedEntity.didStartTakingDamage();
		
		Timer t  = new Timer(1000, new ActionListener(){
			public void actionPerformed(ActionEvent ae)
			{
				if(damagedEntity!=null) damagedEntity.didFinishTakingDamage();
				
				((Timer)ae.getSource()).stop();
			}
		});
		t.start();
		
		currentHealth -= amount;
		healthLabel.setText(currentHealth + "/" + totalHealth);
		
		this.repaint();
		this.revalidate();
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int key = e.getKeyCode();
		
		if(key == KeyEvent.VK_W)//up
		{
			ddcc.playerDidPressMoveButton("W");
		}
		else if(key == KeyEvent.VK_A)//left
		{
			ddcc.playerDidPressMoveButton("A");
		}
		else if(key == KeyEvent.VK_S)//down
		{
			ddcc.playerDidPressMoveButton("S");
		}
		else if(key == KeyEvent.VK_D)//right
		{
			ddcc.playerDidPressMoveButton("D");
		}
		else if(key == KeyEvent.VK_P)
		{
			ddcc.playerDidPressAttackButton();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

}

class Tuple {
	int x;
	int y;
	public Tuple(int X, int Y)
	{
		x=X;
		y=Y;
	}
}
