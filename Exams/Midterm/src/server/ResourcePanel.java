package server;

import java.awt.Color;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SpringLayout;

import resource.Resource;

public class ResourcePanel extends JPanel
{
	private static final long serialVersionUID = 3942398823L;
	
	
	public ResourcePanel(Vector<Resource> resources)
	{
		this.setLayout(new GridLayout(resources.size(), 1));
		
		for (Resource r : resources)
		{
			JPanel outPanel = new JPanel(new GridLayout(1,1));
			outPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), r.getName()));
			
			String imageName = "resources/img/" + r.getName() + ".png";
			ImagePanel panel = new ImagePanel(new ImageIcon(imageName).getImage()); //from Assignment 3
			
			SpringLayout layout = new SpringLayout();
			panel.setLayout(layout);
			
			JLabel qtyLabel = new JLabel("Amout: " + r.getQuantity());
			JLabel locLabel = new JLabel("Location: (" + r.getX() + "," + r.getY() + ")");
			
			panel.add(qtyLabel);
			layout.putConstraint(SpringLayout.SOUTH, qtyLabel, -10, SpringLayout.VERTICAL_CENTER, panel);
			layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, qtyLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel);
			
			panel.add(locLabel);
			layout.putConstraint(SpringLayout.NORTH, locLabel, 5, SpringLayout.VERTICAL_CENTER, panel);
			layout.putConstraint(SpringLayout.HORIZONTAL_CENTER, locLabel, 0, SpringLayout.HORIZONTAL_CENTER, panel);
			
			outPanel.add(panel);
			this.add(outPanel);
		}
	}
}
