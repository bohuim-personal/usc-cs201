package server;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

import resource.Factory;

public class FactoryInfo extends JFrame
{
	private static final long serialVersionUID = 39248932849238L;

	private Factory factory;
	private SpringLayout layout;
	
	private JPanel topRow;
	private JLabel resourceLabel;
	private JLabel productLabel;
	private ResourcePanel leftCol;
	private ProductPanel rightCol;
	

	public FactoryInfo(Factory factory)
	{
		super(factory.getName() + " information");
		this.factory = factory;
		
		layout = new SpringLayout();
		this.setLayout(layout);
		
		addMenubar();
		addTopbar();
		addLabelRow();
		addResourceCol();
		addProductCol();
		
		this.setSize(500,500);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	private void addMenubar()
	{
		JMenuBar menubar = new JMenuBar();
		JMenuItem about = new JMenuItem("About");
		about.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e)
			{
				JOptionPane.showMessageDialog(FactoryInfo.this, "Bohui Moon", "Spring 2015 - Midterm", JOptionPane.INFORMATION_MESSAGE);
			}
		});
		about.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		menubar.add(about);
		
		this.setJMenuBar(menubar);
	}
	
	private void addTopbar()
	{
		topRow = new JPanel();
		topRow.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		JLabel whLabel = new JLabel("Width: " + factory.getWidth() + " Height: " + factory.getHeight());
		
		Point loc = factory.getTaskBoardLocation();
		JLabel tbLabel = new JLabel("TaskBoard Location: (" + loc.x + "," + loc.y + ")");
		
		topRow.setLayout(new BoxLayout(topRow, BoxLayout.X_AXIS));
		topRow.add(Box.createHorizontalGlue());
		topRow.add(whLabel);
//		panel.add(Box.createRigidArea(new Dimension(30, 30)));
		topRow.add(Box.createHorizontalGlue());
		topRow.add(tbLabel);
		topRow.add(Box.createHorizontalGlue());
		
		
		this.add(topRow);
		layout.putConstraint(SpringLayout.NORTH, topRow, 0, SpringLayout.NORTH, this.getContentPane());
		layout.putConstraint(SpringLayout.EAST, topRow, 0, SpringLayout.EAST, this.getContentPane());
		layout.putConstraint(SpringLayout.WEST, topRow, 0, SpringLayout.WEST, this.getContentPane());
		layout.putConstraint(SpringLayout.SOUTH, topRow, 20, SpringLayout.NORTH, this.getContentPane());
	}
	
	private void addLabelRow()
	{
		resourceLabel = new JLabel("Available Resources");
		this.add(resourceLabel);
		layout.putConstraint(SpringLayout.WEST, resourceLabel, 3, SpringLayout.WEST, this.getContentPane());
		layout.putConstraint(SpringLayout.NORTH, resourceLabel, 0, SpringLayout.SOUTH, topRow);
		
		
		productLabel = new JLabel("Available Products");
		this.add(productLabel);
		layout.putConstraint(SpringLayout.WEST, productLabel, 0, SpringLayout.HORIZONTAL_CENTER, this.getContentPane());
		layout.putConstraint(SpringLayout.NORTH, productLabel, 0, SpringLayout.SOUTH, topRow);
	}
	
	private void addResourceCol()
	{
		leftCol = new ResourcePanel(factory.getResources());
		
//		leftCol.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		this.add(leftCol);
		layout.putConstraint(SpringLayout.NORTH, leftCol, 0, SpringLayout.SOUTH, resourceLabel);
		layout.putConstraint(SpringLayout.WEST, leftCol, 0, SpringLayout.WEST, this.getContentPane());
		layout.putConstraint(SpringLayout.EAST, leftCol, 0, SpringLayout.HORIZONTAL_CENTER, this.getContentPane());
		layout.putConstraint(SpringLayout.SOUTH, leftCol, 0, SpringLayout.SOUTH, this.getContentPane());
	}
	
	private void addProductCol()
	{
		rightCol = new ProductPanel(factory.getProducts());
		
//		rightCol.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		
		this.add(rightCol);
		layout.putConstraint(SpringLayout.NORTH, rightCol, 0, SpringLayout.SOUTH, resourceLabel);
		layout.putConstraint(SpringLayout.WEST, rightCol, 0, SpringLayout.HORIZONTAL_CENTER, this.getContentPane());
		layout.putConstraint(SpringLayout.EAST, rightCol, 0, SpringLayout.EAST, this.getContentPane());
		layout.putConstraint(SpringLayout.SOUTH, rightCol, 0, SpringLayout.SOUTH, this.getContentPane());
	}
}
