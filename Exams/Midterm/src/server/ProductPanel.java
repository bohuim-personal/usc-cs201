package server;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SpringLayout;

import resource.Product;
import resource.Resource;

public class ProductPanel extends JPanel
{
	private static final long serialVersionUID = 23938983L;

	
	public ProductPanel(Vector<Product> products)
	{
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		
		for (Product p : products)
		{
			JPanel panel = new JPanel();
			panel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(Color.BLACK), p.getName()));
			
			panel.setLayout(new GridBagLayout());
			GridBagConstraints gbc = new GridBagConstraints();
			gbc.gridx = 0;
			
			gbc.gridy = 0;
			for (Resource r : p.getResourcesNeeded())
			{
				JLabel label = new JLabel(r.getQuantity() + " " + r.getName());
				panel.add(label, gbc);
				gbc.gridy++;
			}
			mainPanel.add(panel);
		}
		
		JScrollPane scroll = new JScrollPane(mainPanel);
		scroll.setVerticalScrollBar(scroll.createVerticalScrollBar());
		
		
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		this.add(scroll);
		layout.putConstraint(SpringLayout.NORTH, scroll, 0, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.SOUTH, scroll, 0, SpringLayout.SOUTH, this);
		layout.putConstraint(SpringLayout.WEST, scroll, 0, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.EAST, scroll, 0, SpringLayout.EAST, this);
	}
}
