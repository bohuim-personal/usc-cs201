package server;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.LayoutManager;

import javax.swing.JPanel;


/**
 * BackgroundImagePanel is a subclass of JPanel that is capable of rendering a single background image.
 * 
 * @author Bohui Moon
 */
public class ImagePanel extends JPanel
{
	private static final long serialVersionUID = 349583094830498L;
	
	
	/* IVARS */
	private Image image;
	
	
	/* ======== CONSTRUCTOR & SETUP ======== */
	/**
	 * Create a new BackgroundImagePanel with double buffer, FlowLayout and no background image.
	 */
	public ImagePanel()
	{
		this.image = null;
	}
	
	/**
	 * Create a new BackgroundImagePanel with FLowLayout and given background image.
	 */
	public ImagePanel(Image image)
	{
		super();
		this.image = image;
	}
	
	/**
	 * Create a new BackgroundImagePanel with given layout manager, and no background image.
	 * 
	 * @param layout - LayoutManager to use
	 */
	public ImagePanel(LayoutManager layout)
	{
		this(layout, null);
	}
	
	public ImagePanel(LayoutManager layout, Image image)
	{
		super(layout);
		this.image = image;
	}
	
	
	
	/* ======== ACCESSORS ======== */
	/**
	 * Return the current background image
	 * @return current background image
	 */
	public Image getImage()
	{
		return image;
	}
	
	/**
	 * Set the background image to the given image
	 * @param image - to set and show
	 */
	public void setImage(Image image)
	{
		this.image = image;
		repaint();
	}
	
	
	
	/* ======== OVERRIDE ======== */
	/**
	 * Draw the current background image.
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		g.drawImage(image, 0, 0, this.getWidth(), this.getHeight(), null);
	}
}
