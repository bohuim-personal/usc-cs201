package client;

import java.awt.Rectangle;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

import libraries.ImageLibrary;
import resource.Product;
import resource.Resource;

public class FactoryMailbox extends FactoryObject
{

	private Vector<Resource> available;
	private FactoryClientListener listener;
	
	private Queue<Resource> mail;
	
	protected FactoryMailbox(Vector<Resource> deliveries, FactoryClientListener listener)
	{
		super(new Rectangle(0,0,1,1));
		available = deliveries;
		this.listener = listener;
		
		mImage = ImageLibrary.getImage(Constants.resourceFolder + "mailbox" + Constants.png);
		mLabel = "Mailbox";
		mail = new LinkedList<Resource>();
	}
	
	public Resource getStock() throws InterruptedException
	{
		while(mail.isEmpty()) {
			Thread.sleep(200);
		}
		return mail.remove();
	}

	public void insert(Resource resource)
	{
		for(Resource r : available)
		{
			if(r.getName().equals(resource.getName()))
			{
				mail.add(resource);
				break;
			}
		}
	}
	
	public void ship(Product product)
	{
		listener.sendMessage(product.getName());
	}
}
