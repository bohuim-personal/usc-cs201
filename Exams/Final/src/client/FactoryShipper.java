package client;

import java.awt.Image;

import libraries.ImageLibrary;
import resource.Product;

/**
 * 
 * 
 * @author Bohui Moon
 */
public class FactoryShipper extends FactoryWorker
{
	/* IVARS */
	private Image itemImage;
	private Image noItemImage;
	
	private FactoryProductBin bin;
	private Product toShip;
	
	
	/* ======== CONSTRUCTOR ========*/
	/**
	 * 
	 * @param inNumber
	 * @param startNode
	 * @param inFactorySimulation
	 */
	FactoryShipper(int inNumber, FactoryNode startNode, FactorySimulation sim)
	{
		super(inNumber, startNode, sim);
		
		mLabel = "Shipper";
		itemImage = ImageLibrary.getImage(Constants.resourceFolder + "stockperson_box" + Constants.png);
		noItemImage = ImageLibrary.getImage(Constants.resourceFolder + "stockperson_empty" + Constants.png);
		mImage = noItemImage;
		
		bin = sim.getProductBin();
		toShip = null;
	}
	
	
	@Override
	public void run()
	{
		mLock.lock();
		try
		{
			while(true)
			{
				mImage = noItemImage;
				//get product
				if (toShip == null)
				{
					//go to product bin
					mDestinationNode = mFactorySimulation.getNode("ProductBin");
					mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
					mNextNode = mShortestPath.pop();
					atLocation.await();
					
					//wait til product is available
					do
					{
						toShip = bin.getProduct();
					}
					while(toShip == null);
					mImage = itemImage;
				}
				//goto mailbox
				{
					mDestinationNode = mFactorySimulation.getNode("MailBox");
					mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
					mNextNode = mShortestPath.pop();
					atLocation.await();
					
					mFactorySimulation.getMailBox().ship(toShip);
					mImage = noItemImage;
				}
				//report to taskboard
				{
					//get an assignment from the table
					mDestinationNode = mFactorySimulation.getNode("Task Board");
					mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
					mNextNode = mShortestPath.pop();
					atLocation.await();
					
					//mark product as shipped
					mFactorySimulation.getTaskBoard().markShipped(toShip);
				}
				toShip = null;
			}
		}
		catch (InterruptedException e)
		{
			//e.printStackTrace();
			System.out.println("[Error@FactoryShipper.run()]: " + e.getMessage());
		}
		mLock.unlock();
	}
}
