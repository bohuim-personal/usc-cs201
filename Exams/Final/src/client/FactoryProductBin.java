package client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.LinkedList;
import java.util.Queue;

import libraries.ImageLibrary;
import resource.Product;

/**
 * 
 * @author Bohui Moon
 */
public class FactoryProductBin extends FactoryObject
{

	/* IVARS */
	private Queue<Product> finished;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 * @param inDimensions
	 */
	public FactoryProductBin(Rectangle inDimensions)
	{
		super(inDimensions);
		
		mLabel = "ProudctBin";
		mImage = ImageLibrary.getImage(Constants.resourceFolder + "Box" + Constants.png);
		
		finished = new LinkedList<Product>();
	}
	
	
	/**
	 * Add the product to the finished vector only if it doesn't exist
	 * 
	 * @param product
	 */
	public synchronized void addProduct(Product product)
	{
		finished.add(product);
	}
	
	/**
	 * Wait until a product is available and return it by removing.
	 * 
	 * @return first in finished bin
	 */
	public synchronized Product getProduct()
	{
		if (finished.isEmpty())
			return null;
		
		return finished.remove();
	}
	
	/**
	 * Draw the current count of this ProductBin
	 */
	@Override
	public void draw(Graphics g, Point mouseLocation)
	{
		super.draw(g, mouseLocation);
		g.setColor(Color.BLACK);
		
		String str = "" + finished.size();
		g.drawString(str, centerTextX(g, str), centerTextY(g));
	}
}
