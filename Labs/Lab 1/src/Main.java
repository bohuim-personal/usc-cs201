
public class Main
{
	
	public static void main(String[] args)
	{
		System.out.println("Maximum int: " + Integer.MAX_VALUE);
		System.out.println("Minimum int: " + Integer.MIN_VALUE);
		System.out.println("Number of bits of int: " + Integer.SIZE);
	}
}
