package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;

import resource.Factory;
import resource.Resource;
import utilities.Util;

public class ServerClientCommunicator extends Thread {

	private Socket socket;
	private ObjectOutputStream oos;
	private BufferedReader br;
	private ServerListener serverListener;
	
	private MySQLDriver sql;
	
	public ServerClientCommunicator(Socket socket, ServerListener serverListener) throws IOException
	{
		this.socket = socket;
		this.serverListener = serverListener;
		this.oos = new ObjectOutputStream(socket.getOutputStream());
		this.br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		
		sql = new MySQLDriver();
		sql.connect();
	}
	
	public void sendFactory(Factory factory)
	{
		try
		{
			oos.writeObject(factory);
			oos.flush();
		}
		catch (IOException ioe)
		{
			Util.printExceptionToCommand(ioe);
		}
	}
	
	public void sendResource(Resource resource)
	{
		if (socket.isClosed())
			return;
		
		try
		{
			oos.writeObject(resource);
			oos.flush();
		}
		catch (IOException ioe)
		{
			serverListener.removeServerClientCommunicator(this);
			Util.printExceptionToCommand(ioe);
			ioe.printStackTrace();
		}
	}
	
	public void run()
	{
		try
		{
			String line;
			while (true)
			{
				line = br.readLine();
				if (line != null && !line.isEmpty())
				{
					System.out.println(line);
					
					String[] commands = line.split(":");
					if (commands[0].equals("need"))
					{
						serverListener.addResourceRequest( commands[1] );
					}
					else if (commands[0].equals("finished"))
					{
						sql.increment(commands[1]);
					}
				}
			}
		}
		catch (IOException ioe)
		{
			serverListener.removeServerClientCommunicator(this);
			FactoryServerGUI.addMessage(socket.getInetAddress() + ":" + socket.getPort() + " - " + Constants.clientDisconnected);
			
			// this means that the socket is closed since no more lines are being received
			try
			{
				socket.close();
			}
			catch (IOException ioe1)
			{
				Util.printExceptionToCommand(ioe1);
			}
		}
//		catch (InterruptedException e)
//		{
//			e.printStackTrace();
//		}
	}
}
