package server;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Vector;

import resource.Factory;
import resource.Resource;

/**
 * 
 * @author Bohui Moon
 */
public class FactoryWarehouse implements Runnable
{
	private Vector<ServerClientCommunicator> communicators;
	private volatile Vector<Resource> resources;
	
	private Queue<String> requests;
	
	public FactoryWarehouse(Vector<ServerClientCommunicator> sccVector)
	{
		communicators = sccVector;
		requests = new LinkedList<String>();
	}
	
	
	public void setFactory(Factory factory)
	{
		resources = factory.getResources();
	}
	
	public void addResourceRequest(String name)
	{
		requests.add(name);
	}
	
	
	/**
	 * 
	 */
	@Override
	public void run()
	{
		//Random rand = new Random();
		try
		{
			while(resources == null)
				Thread.sleep(1);
			
			while (true)
			{
				Thread.sleep(5000);
				
				if (!requests.isEmpty())
				{
					String name = requests.remove();
					int number = 20;
					
					for (ServerClientCommunicator communicator : communicators)
					{
						communicator.sendResource(new Resource(name, number));
					}
				}
			}
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}

}
