package server;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import resource.Factory;

public class FactoryServerGUI extends JFrame {

	public static final long serialVersionUID = 1;
	private static JTextArea textArea;
	private JScrollPane textAreaScrollPane;
	private JButton selectFactoryButton;
	private JComboBox<String> selectFactoryComboBox;
	private JComboBox<Integer> numberComoboBox;

	public FactoryServerGUI() {
		super(Constants.factoryGUITitleString);
		initializeVariables();
		createGUI();
		addActionAdapters();
		setVisible(true);
	}
	
	private void initializeVariables() {
		textArea = new JTextArea();
		textArea.setEditable(false);
		textAreaScrollPane = new JScrollPane(textArea);

		Vector<String> filenamesVector = new Vector<String>();
		File directory = new File(Constants.defaultResourcesDirectory);
		File[] filesInDirectory = directory.listFiles();
		for (File file : filesInDirectory) {
		    if (file.isFile()) {
		    	filenamesVector.add(file.getName());
		    }
		}
		selectFactoryButton = new JButton(Constants.selectFactoryButtonString);
		selectFactoryComboBox = new JComboBox<String>(filenamesVector);
		
		numberComoboBox = new JComboBox<Integer>(new Integer[]{1, 2, 3, 4, 5});
	}
	
	private void createGUI() {
		setSize(Constants.factoryGUIwidth, Constants.factoryGUIheight);
		JPanel northPanel = new JPanel();
		northPanel.add(numberComoboBox);
		northPanel.add(selectFactoryComboBox);
		northPanel.add(selectFactoryButton);
		add(northPanel, BorderLayout.NORTH);
		add(textAreaScrollPane, BorderLayout.CENTER);
	}
	
	private void addActionAdapters() {
		addWindowListener(new WindowAdapter() {
			  public void windowClosing(WindowEvent we) {
				  System.exit(0);
			  }
		});
		
		ConfigureFactoryListener cfl = new ConfigureFactoryListener(this, selectFactoryComboBox);
		selectFactoryButton.addActionListener(cfl);
	}
	
	public void sendFactory(Factory factory)
	{
		FactoryServer.sendFactory(factory, numberComoboBox.getSelectedIndex() + 1);
	}
	
	public static void addMessage(String msg) {
		if (textArea.getText() != null && textArea.getText().trim().length() > 0) {
			textArea.append("\n" + msg);
		}
		else {
			textArea.setText(msg);
		}
	}
}
