package server;

import java.net.ServerSocket;

import resource.Factory;
import resource.Product;
import resource.Resource;

public class FactoryServer
{

	private ServerSocket ss;
	private static ServerListener serverListener;
	
	public FactoryServer()
	{
		PortGUI pf = new PortGUI();
		ss = pf.getServerSocket();
		new FactoryServerGUI();
		listenForConnections();
	}
	
	private void listenForConnections()
	{
		serverListener = new ServerListener(ss);
		serverListener.start();
	}
	
	public static void sendFactory(Factory factory, int n)
	{
		MySQLDriver mysql = new MySQLDriver();
		mysql.connect();
		
		for (Product p : factory.getProducts())
		{
			if (!mysql.doesExist(p.getName()))
				mysql.add(p.getName());
		}
		mysql.stop();
		
		Factory splitFactory = new Factory();
		splitFactory.setName(factory.getName());
		splitFactory.setNumberOfWorkers(factory.getNumberOfWorkers());
		splitFactory.setDimensions(factory.getWidth(), factory.getHeight());
		splitFactory.setTaskBoardLocation(factory.getTaskBoardLocation());
		
		for (Product p : factory.getProducts())
			splitFactory.addProduct( new Product(p.getName(), p.getQuantity()/n, p.getResourcesNeeded()) );
		
		for (Resource r : factory.getResources())
			splitFactory.addResource( new Resource(r.getName(), r.getQuantity()/n, r.getX(), r.getY()) );
		
		if (serverListener != null)
			serverListener.sendFactory(splitFactory);
	}
		
	public static void main(String [] args)
	{
		new FactoryServer();
	}
}
