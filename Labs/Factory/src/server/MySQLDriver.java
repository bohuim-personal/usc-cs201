package server;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Connection;
import java.sql.DriverManager;

import com.mysql.jdbc.Driver;

/**
 * 
 * 
 * @author Bohui Moon
 */
public class MySQLDriver
{
	private final static String SELECT_NAME = "SELECT * FROM FACTORYORDERS WHERE NAME=?";
	private final static String ADD_PRODUCT = "INSERT INTO FACTORYORDERS(NAME, CREATED) VALUES(?,?)";
	
	
	/* IVARS */
	private Connection connection;
	
	
	/* ======== CONSTRUCTOR ======== */
	/**
	 * 
	 */
	public MySQLDriver()
	{
		try
		{
			new Driver();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	
	/* ======== OPEN/CLOSE ========= */
	public void connect()
	{
		try
		{
			connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/factory", "root", "root");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void stop()
	{
		try
		{
			connection.close();
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	
	public boolean doesExist(String productName)
	{
		try
		{
			PreparedStatement ps = connection.prepareStatement(SELECT_NAME);
			ps.setString(1, productName);
			
			ResultSet result = ps.executeQuery();
			while (result.next())
			{
				FactoryServerGUI.addMessage(result.getString(1) + " exists with count: " + result.getInt(2));
				return true;
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
	public void add(String productName)
	{
		try
		{
			PreparedStatement ps = connection.prepareStatement(ADD_PRODUCT);
			ps.setString(1, productName);
			ps.setInt(2, 0);
			ps.executeUpdate();
			
			FactoryServerGUI.addMessage("Adding product: " + productName + " to table with count 0");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
	
	public void increment(String productName)
	{
		try
		{
			PreparedStatement ps = connection.prepareStatement("UPDATE FACTORYORDERS SET created = created + 1 WHERE name=?");
			ps.setString(1, productName);
			
			ps.executeUpdate();
			
			FactoryServerGUI.addMessage("Product " + productName + " incremented by 1");
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		
	}
}








