package client;

import java.awt.Rectangle;
import java.util.concurrent.Semaphore;

import libraries.ImageLibrary;

/**
 * 
 * @author Dennis
 */
public class FactoryResourceDoor extends FactoryObject
{
	
	/* IVARS */
	private Semaphore permit;
	
	
	/**
	 * 
	 * @param inDimensions
	 */
	protected FactoryResourceDoor(Rectangle inDimensions)
	{
		super(inDimensions);
		
		mImage = ImageLibrary.getImage(Constants.resourceFolder + "door" + Constants.png);
		mLabel = "Workroom Door";
		
		permit = new Semaphore(1);
	}

	
	public void getPermit() throws InterruptedException
	{
		permit.acquire();
	}
	
	public void returnPermit()
	{
		permit.release();
	}
}
