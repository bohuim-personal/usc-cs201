package client;

import java.io.FileWriter;
import java.io.IOException;

/**
 * FactoryReport ensures implementing class has {@code report()}
 * 
 * @author Bohui Moon
 */
public interface FactoryReporter
{
	/**
	 * 
	 */
	public void report(FileWriter fw) throws IOException;
}
