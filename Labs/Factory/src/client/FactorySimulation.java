package client;

import java.awt.Point;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.JTable;

import resource.Factory;
import resource.Resource;


public class FactorySimulation {
	
	Factory mFactory;
	
	private ArrayList<FactoryObject> mFObjects;
	private ArrayList<FactoryWorker> mFWorkers;
	private FactoryNode mFNodes[][];
	private Map<String, FactoryNode> mFNodeMap;
	private FactoryTaskBoard mTaskBoard;
	private FactoryMailbox mMailbox;
	
	private FactoryManager manager;
	private Vector<FactoryWorkbench> workbenches;
	
	private boolean done = false;
	private double totalTime = 0.0;
	
	//instance constructor
	{
		mFObjects = new ArrayList<FactoryObject>();
		mFWorkers = new ArrayList<FactoryWorker>();
		mFNodeMap = new HashMap<String, FactoryNode>();
	}
	
	FactorySimulation(Factory inFactory, JTable inTable, FactoryManager manager)
	{
		this.manager = manager;
		
		mFactory = inFactory;
		mFNodes = new FactoryNode[mFactory.getWidth()][mFactory.getHeight()];
		
		//Create the nodes of the factory
		for(int height = 0; height < mFactory.getHeight(); height++)
		{
			for(int width = 0; width < mFactory.getWidth(); width++)
			{
				mFNodes[width][height] = new FactoryNode(width,height);
				mFObjects.add(mFNodes[width][height]);
			}
		}
		
		//Link all of the nodes together
		for(FactoryNode[] nodes: mFNodes)
		{
			for(FactoryNode node : nodes)
			{
				int x = node.getX();
				int y = node.getY();
				if(x != 0) node.addNeighbor(mFNodes[x-1][y]);
				if(x != mFactory.getWidth()-1) node.addNeighbor(mFNodes[x+1][y]);
				if(y != 0) node.addNeighbor(mFNodes[x][y-1]);
				if(y != mFactory.getHeight()-1) node.addNeighbor(mFNodes[x][y+1]);
			}
		}
		
		//Create the resources
		for(Resource resource : mFactory.getResources()) {
			FactoryResource fr = new FactoryResource(resource);
			mFObjects.add(fr);
			mFNodes[fr.getX()][fr.getY()].setObject(fr);
			mFNodeMap.put(fr.getName(), mFNodes[fr.getX()][fr.getY()]);
		}
		
		//Create the task board
		Point taskBoardLocation = mFactory.getTaskBoardLocation();
		mTaskBoard = new FactoryTaskBoard(inTable,inFactory.getProducts(),taskBoardLocation.x,taskBoardLocation.y);
		mFObjects.add(mTaskBoard);
		mFNodes[taskBoardLocation.x][taskBoardLocation.y].setObject(mTaskBoard);
		mFNodeMap.put("Task Board", mFNodes[taskBoardLocation.x][taskBoardLocation.y]);
		
		//Create the workers, set their first task to look at the task board
		for(int i = 0; i < mFactory.getNumberOfWorkers(); i++) {
			//Start each worker at the first node (upper left corner)
			//Note, may change this, but all factories have an upper left corner(0,0) so it makes sense
			FactoryWorker fw = new FactoryWorker(i, mFNodes[0][0], this);
			mFObjects.add(fw);
			mFWorkers.add(fw);
		}
		
		//walls.txt
		Scanner scanner = null;
		try
		{
			scanner = new Scanner(new File("resources/walls.txt"));
			while (scanner.hasNext())
			{
				int x = scanner.nextInt();
				int y = scanner.nextInt();
				String imgFile = scanner.next();
				
				FactoryWall wall = new FactoryWall(new Rectangle(x,y,1,1), imgFile);
				mFObjects.add(wall);
				mFNodes[x][y].setObject(wall);
			}
		}
		catch (FileNotFoundException e)
		{
			System.out.println(e.getMessage());
		}
		finally
		{
			if (scanner != null)
				scanner.close();
		}
		
		//mailbox
		mMailbox = new FactoryMailbox(mFactory.getResources());
		mFObjects.add(mMailbox);
		mFNodes[0][0].setObject(mMailbox);
		mFNodeMap.put("Mailbox", mFNodes[0][0]);
		
		//stockpersons
		for (int i=0; i < 3; i++)
		{
			FactoryStockPerson sp = new FactoryStockPerson(i, mFNodes[0][0], this);
			mFObjects.add(sp);
			mFWorkers.add(sp);
		}
		
		//workbench
		workbenches = new Vector<FactoryWorkbench>();
		for (int i=10; i < 14; i++)
		{
			FactoryWorkbench wb = new FactoryWorkbench(new Rectangle(13,i,1,1));
			mFNodes[13][i].setObject(wb);
			mFObjects.add(wb);
			workbenches.add(wb);
		}
		
		//doors
//		FactoryResourceDoor rDoor = new FactoryResourceDoor(new Rectangle(1, 11, 1, 1));
//		mFNodes[1][11].setObject(rDoor);
//		mFNodeMap.put("Resource", mFNodes[1][11]);
//		mFObjects.add(rDoor);
		
		FactoryWorkroomDoor wDoor = new FactoryWorkroomDoor(new Rectangle(13, 8, 1, 1), workbenches);
		mFNodes[13][8].setObject(wDoor);
		mFNodeMap.put("Workroom", mFNodes[13][8]);
		mFObjects.add(wDoor);
		
		//robots
		FactoryRobotBin robotBin = new FactoryRobotBin(new Rectangle(10,0,1,1));
		mFNodes[10][0].setObject(robotBin);
		mFNodeMap.put("RobotBin", mFNodes[10][0]);
		mFObjects.add(robotBin);
		
		for (int i=0; i < 20; i++)
		{
			FactoryRobot fr = new FactoryRobot(i, this.getNode("RobotBin"), this);
			mFObjects.add(fr);
			mFWorkers.add(fr);
		}
		
		for (FactoryWorker fw : mFWorkers)
			fw.getThread().start();
	}
	
	public FactoryMailbox getMailbox()
	{
		return mMailbox;
	}
	
	public void update(double deltaTime) 
	{
		totalTime += deltaTime;
		
		if (done)
			return;
			
		//Update all the objects in the factor that need updating each tick
		for(FactoryObject object : mFWorkers) 
			object.update(deltaTime);
		
		if (mTaskBoard.isDone())
		{
			done = true;
			
			//time report dialog
			JOptionPane.showMessageDialog(null, "Total time: " + (new DecimalFormat(".###")).format(totalTime) + "s", "Simulation Over!", JOptionPane.INFORMATION_MESSAGE);
			
			Timestamp time = new Timestamp(System.currentTimeMillis());
			String ofilename = "reports/" + time;
			ofilename = ofilename.replaceAll("[-:. ]", "_");
			
			File file = null;
			try
			{
				file = new File(ofilename);
				file.createNewFile();
				
				FileWriter fw = new FileWriter(file, true);
				for (FactoryObject obj : mFObjects)
				{
					if (obj instanceof FactoryReporter)
						((FactoryReporter) obj).report(fw);
				}
				fw.close();
			}
			catch(IOException e)
			{
				System.out.println("Error occured during file write: " + ofilename);
				if (file != null)
					file.delete();
			}
		}
	}
	
	public ArrayList<FactoryObject> getObjects() {
		return mFObjects;
	}
	
	public ArrayList<FactoryWorker> getWorkers() {
		return mFWorkers;
	}
	
	public FactoryNode[][] getNodes() {
		return mFNodes;
	}

	public String getName() {
		return mFactory.getName();
	}

	public double getWidth() {
		return mFactory.getWidth();
	}
	
	public double getHeight() {
		return mFactory.getHeight();
	}

	public FactoryNode getNode(String key) {
		return mFNodeMap.get(key);
	}

	public FactoryTaskBoard getTaskBoard() {
		return mTaskBoard;
	}
	
	public void reportFinishedProduct(String productName)
	{
		manager.sendMessage("finished:" + productName);
	}
}
