package client;

import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Stack;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import libraries.ImageLibrary;
import resource.Product;
import resource.Resource;

public class FactoryWorker extends FactoryObject implements Runnable, FactoryReporter
{
	
	protected int mNumber;
	protected Timestamp finished;
	
	protected FactorySimulation mFactorySimulation;
	protected Product mProductToMake;
	
	protected Lock mLock;
	protected Condition atLocation;
	
	//Nodes each worker keeps track of for path finding
	protected FactoryNode mPrevNode;
	protected FactoryNode mCurrentNode;
	protected FactoryNode mNextNode;
	protected FactoryNode mDestinationNode;
	protected Stack<FactoryNode> mShortestPath;
	
	protected Thread mThread;
	
	//instance constructor
	{
		mImage = ImageLibrary.getImage(Constants.resourceFolder + "worker" + Constants.png);
		mLock = new ReentrantLock();
		atLocation = mLock.newCondition();
	}
	
	FactoryWorker(int inNumber, FactoryNode startNode, FactorySimulation inFactorySimulation)
	{
		super(new Rectangle(startNode.getX(), startNode.getY(), 1, 1));
		mNumber = inNumber;
		mCurrentNode = startNode;
		mFactorySimulation = inFactorySimulation;
		mLabel = Constants.workerString + String.valueOf(mNumber);
		
		mThread = new Thread(this);
	}
	
	public Thread getThread()
	{
		return mThread;
	}
	
	public FactoryNode getPrevNode()
	{
		return mPrevNode;
	}
	
	
	@Override
	public void draw(Graphics g, Point mouseLocation)
	{
		super.draw(g, mouseLocation);
	}
	
	@Override
	public void update(double deltaTime)
	{
		if(!mLock.tryLock()) 
			return;
		
		//if we have somewhere to go, go there
		if(mDestinationNode != null)
		{
			if(moveTowards(mNextNode,deltaTime * Constants.workerSpeed))
			{
				//if we arrived, save our current node
				mPrevNode = mCurrentNode;
				mCurrentNode = mNextNode;
				if(!mShortestPath.isEmpty())
				{
					//if we have somewhere else to go, save that location
					mNextNode = mShortestPath.pop();
					mCurrentNode.unMark();
				}
				
				//if we arrived at the location, signal the worker thread so they can do more actions
				if(mCurrentNode == mDestinationNode)
				{
					mDestinationNode.unMark();
					atLocation.signal();
				}
			}
		}
		mLock.unlock();
	}
	
	/**
	 * 
	 */
	@Override
	public void report(FileWriter fw) throws IOException
	{
		fw.write(mNumber + " finished at " + finished + "\n");
	}
	
	//Use a separate thread for expensive operations
	//Path finding
	//Making objects
	//Waiting
	@Override
	public void run() 
	{
		mLock.lock();
		try 
		{
			//Thread.sleep(1000*mNumber); //used to space out the factory workers
			while(true) 
			{
				if(mProductToMake == null) 
				{
					FactoryNode tbNode = mFactorySimulation.getNode("Task Board");
					FactoryTaskBoard tb = mFactorySimulation.getTaskBoard();
					
					if (!tb.isLine()) //no line
						mDestinationNode = tbNode;
					else //there is a line
					{
						int index = tb.indexInLine(this);
						if (index == 0) //first in line
						{
							mDestinationNode = tbNode;
						}
						else if (index > 0) //in line & not first
						{
							mDestinationNode = tb.workerAt(index - 1).getPrevNode();
						}
						else if (index < 0) //not in line
						{
							mDestinationNode = tb.lastInLine().getPrevNode();
						}
					}
					
					
					//get an assignment from the table
					mDestinationNode = mFactorySimulation.getNode("Task Board");
					mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
					mNextNode = mShortestPath.pop();
					atLocation.await();
					
					tb.lock();
					tb.getInLine(this);
					if (tb.workerAt(0) == this)
					{
						tb.outOfLine();
						while(!mDestinationNode.acquireNode())
							Thread.sleep(1);
						
						Thread.sleep(500);
						mProductToMake = tb.getTask();
						mDestinationNode.releaseNode();
						if(mProductToMake == null)
						{
							tb.unlock();
							break; //No more tasks, end here
						}
					}
					tb.unlock();
				}
				else //build product
				{
					ArrayList<FactoryRobot> robots = new ArrayList<FactoryRobot>();
					ArrayList<Thread> robotThreads = new ArrayList<Thread>();
					FactoryRobotBin robotBin;
					//go to the robot bin
					{
						mDestinationNode = mFactorySimulation.getNode("RobotBin");
						mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
						mNextNode = mShortestPath.pop();
						atLocation.await();
						robotBin = (FactoryRobotBin) mDestinationNode.getObject();
					}
					//resource room
					{
//						mDestinationNode = mFactorySimulation.getNode("Resource");
//						mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
//						mNextNode = mShortestPath.pop();
//						atLocation.await();
//						
//						FactoryResourceDoor door = (FactoryResourceDoor) mDestinationNode.getObject();
//						door.getPermit();
						
						for(Resource resource : mProductToMake.getResourcesNeeded())
						{
							FactoryRobot robot = robotBin.getRobot();
							if (robot != null)
							{
								robots.add(robot);
								robot.getResource(resource, mFactorySimulation.getNode("Workroom"));
								robotThreads.add(robot.getThread());
							}
							else
							{
								mDestinationNode = mFactorySimulation.getNode(resource.getName());
								mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
								mNextNode = mShortestPath.pop();
								atLocation.await();
								if (!mProductToMake.getResourcesNeeded().lastElement().equals(resource))
								{
									mDestinationNode = mFactorySimulation.getNode("RobotBin");
									mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
									mNextNode = mShortestPath.pop();
									atLocation.await();
								}
							}
						}
						
//						mDestinationNode = mFactorySimulation.getNode("Resource");
//						mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
//						mNextNode = mShortestPath.pop();
//						atLocation.await();
						
//						door.returnPermit();
					}
					//workroom
					{
						mDestinationNode = mFactorySimulation.getNode("Workroom");
						mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
						mNextNode = mShortestPath.pop();
						atLocation.await();
						
						for (FactoryRobot r : robots)
							r.sendBack();
						
						for (Thread t : robotThreads)
							t.join();
						
						FactoryWorkroomDoor door = (FactoryWorkroomDoor) mDestinationNode.getObject();
						FactoryWorkbench workbench = door.getWorkbench();
						mDestinationNode = mFactorySimulation.getNodes()[workbench.getX()][workbench.getY()];
						mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
						mNextNode = mShortestPath.pop();
						atLocation.await();
						
						workbench.assemble(mProductToMake);
						
						mDestinationNode = mFactorySimulation.getNode("Workroom");
						mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
						mNextNode = mShortestPath.pop();
						atLocation.await();
						
						door.returnWorkbench(workbench);
					}
					//update table
					{
						mDestinationNode = mFactorySimulation.getNode("Task Board");
						mShortestPath = mCurrentNode.findShortestPath(mDestinationNode);
						mNextNode = mShortestPath.pop();
						atLocation.await();
						
						//product finished
						mFactorySimulation.reportFinishedProduct(mProductToMake.getName());
						mFactorySimulation.getTaskBoard().endTask(mProductToMake);
						mProductToMake = null;
						
						finished = new Timestamp(System.currentTimeMillis());
					}
				}
			}
		} 
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
		mLock.unlock();
	}

}
