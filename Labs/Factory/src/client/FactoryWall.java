package client;

//imports
import java.awt.Rectangle;

import libraries.ImageLibrary;


/**
 * 
 * 
 * @author Bohui Moon
 */
public class FactoryWall extends FactoryObject
{
	
	/* Constructor */
	/**
	 * Call super constructor with given dimension
	 */
	public FactoryWall(Rectangle dim, String file)
	{
		super(dim);
		mImage = ImageLibrary.getImage(file);
	}
}
