package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.TableModel;

/**
 * 
 * @author Bohui Moon
 */
public class FactoryProgressPanel extends JPanel implements Runnable
{
	private static final long serialVersionUID = -87338292792723L;
	
	private JTable mTable;
	private TableModel mModel;
	
	private final String TITLE = "Factory Progress";
	private final int BORDER = 20;
	
	/**
	 * 
	 * @param inTable
	 */
	FactoryProgressPanel(JTable inTable)
	{
		mTable = inTable;
		mModel = mTable.getModel();
		new Thread(this).start();
	}
	
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		//title
		int w = this.getWidth();
		int h = this.getHeight();
		Font f = new Font("Times New Roman", Font.BOLD | Font.ITALIC, w/24);
		g.setFont(f);
		
		int strW = g.getFontMetrics(f).stringWidth(TITLE);
		int strH = g.getFontMetrics(f).getHeight(); 
		g.drawString(TITLE, (w - strW)/2, strH);
		
		
		//
		double total = 0;
		double started = 0;
		double completed = 0;
		for (int i=0; i < mModel.getRowCount(); ++i)
		{
			total += (int)mModel.getValueAt(i, Constants.totalNameIndex);
			started += (int)mModel.getValueAt(i, Constants.startedIndex);
			completed += (int)mModel.getValueAt(i, Constants.completedIndex);
		}
		
		int frameX = BORDER;
		int frameY = BORDER + strH;
		int frameW = w - (2 * BORDER);
		int frameH = h - (2 * BORDER) - strH;
		g.drawRect(frameX - 1, frameY - 1, frameW + 1, frameH + 1);
		
		
		int startedWidth = (int)((started/total) * frameW);
		int completedWidth = (int)((completed/total) * frameW);
		int nostatusWidth = frameW - (startedWidth + completedWidth);
		
		g.setColor(Color.GREEN);
		g.fillRect(frameX, frameY, completedWidth, frameH);
		g.setColor(Color.YELLOW);
		g.fillRect(frameX + completedWidth, frameY, startedWidth, frameH);
		g.setColor(Color.RED);
		g.fillRect(frameX + startedWidth + completedWidth, frameY, nostatusWidth, frameH);
	}

	@Override
	public void run()
	{
		while(true)
		{
			try
			{
				Thread.sleep(33);
				repaint();
			}
			catch (InterruptedException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	
}
