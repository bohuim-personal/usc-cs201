# USC CSCI 201: Principles of Software Design

Taken during Fall of 2015  
[Course Website](http://www-scf.usc.edu/~csci201/) (since updated, can't find archive)


**Layout**

- Assignments under `/Assignments`
  - Except for the first one, assignments were additions to the Sorry game
  - included zip files are archived versions
- Group project game under `/Descent into Darkness`
- The programming midterm & final under `/Exams`
- Labs under `/Labs`
  - Labs were also done as iterations & additions to the `Factory` project
- Final Sorry game under `/Sorry`
